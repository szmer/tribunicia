import json
import os.path
import pathlib
import sqlite3

import pytest
import yaml

from tribunicia.dbsetup import setup_db
from tribunicia.pl_morphoparsing import MorfeuszAnalyzer, add_paragraphs_interps

# Set up the MorfeuszAnalyzer object that will be used by all the tests.
@pytest.fixture(scope='module')
def morfeusz_analyzer():
    with open(os.path.join(pathlib.Path(__file__).parent.resolve(), 'resources',
        'pl_test_models.yaml')) as f:
        morpho_yaml = yaml.safe_load(f)
    return MorfeuszAnalyzer(
            morpho_yaml['morfeusz_dict_dir'],
            morpho_yaml['morfeusz_dict_name'],
            morpho_yaml['concraft_model_path'],
            # artificially limit the chunk size to actually test chunking
            morfeusz_chunk_size=24)

#
# The tests.
#
# An example of a two-sentence text for use in tests, parsed with Morfeusz.
example_parse = [
        (0, 1, ('Dinozaur ', 'dinozaur', 'subst:sg:nom:m', ['nazwa pospolita'], ['paleont.'])),
        (1, 2, ('się ', 'się', 'qub', [], [])),
        (2, 3, ('zatrzymał', 'zatrzymać', 'praet:sg:m:perf', [], [])),
        (3, 4, ('. ', '.', 'interp', [], [])),
        (4, 5, ('Poszedł ', 'pójść', 'praet:sg:m:perf', [], [])),
        (5, 6, ('do ', 'do', 'prep:gen', [], [])),
        (5, 6, ('do ', 'do', 'xxx', [], [])),
        (6, 7, ('domu', 'dom', 'subst:sg:gen:m', [], [])),
        (6, 7, ('domu', 'dom', 'subst:sg:gen:m', ['nazwa pospolita'], [])),
        (6, 7, ('domu', 'dom', 'subst:sg:loc:m', [], [])),
        (6, 7, ('domu', 'dom', 'subst:sg:loc:m', ['nazwa pospolita'], [])),
        (6, 7, ('domu', 'dom', 'subst:sg:voc:m', ['nazwa pospolita'], [])),
        (7, 8, ('.', '.', 'interp', [], []))
        ]

@pytest.mark.pl_morpho
def test_create_interp(morfeusz_analyzer):
    assert morfeusz_analyzer.create_interp('subst:sg:nom', 'abażur', [1.0]) == {
            'base': 'abażur', 'ctag': 'subst', 'msd': 'sg:nom', 'disamb': [1.0]
            }

@pytest.mark.pl_morpho
def test_make_pos_entry(morfeusz_analyzer):
    source_text = 'Ucho królika'
    by_start = {
            0: [{'start': 0, 'end': 4, 'test_stuff': [0, 1, 'Ucho']}],
            5: [{'start': 5, 'end': 12, 'test_stuff': [1, 3, 'królika', 'subst']}, 
                {'start': 5, 'end': 10, 'test_stuff': [1, 2, 'króli', 'subst']}],
            10: [{'start': 10, 'end': 12, 'test_stuff': [2, 3, 'ka', 'qub']}],
            }
    entry, pos = morfeusz_analyzer.make_pos_entry(by_start, 0, source_text)
    assert entry == {'start': 0, 'end': 4, 'test_stuff': [0, 1, 'Ucho']}
    assert pos == 4
    entry, pos = morfeusz_analyzer.make_pos_entry(by_start, 5, source_text)
    # (get the possible paths through the second token)
    assert entry == [
            [{'start': 5, 'end': 12, 'test_stuff': [1, 3, 'królika', 'subst']}],
            [{'start': 5, 'end': 10, 'test_stuff': [1, 2, 'króli', 'subst']},
                {'start': 10, 'end': 12, 'test_stuff': [2, 3, 'ka', 'qub']}]]
    assert pos == 12

@pytest.mark.pl_morpho
def test_merge_morfeusz_variants(morfeusz_analyzer):
    variant_list = morfeusz_analyzer.merge_morfeusz_variants(example_parse)
    assert len(variant_list) == 8
    # Expect a list with one token list inside.
    assert variant_list[0] == [(0, 1,
        ('Dinozaur ', 'dinozaur', 'subst:sg:nom:m', ['nazwa pospolita'], ['paleont.']))]
    assert len(variant_list[6]) == 5
    assert variant_list[6][0] == (6, 7, ('domu', 'dom', 'subst:sg:gen:m', [], []))

@pytest.mark.pl_morpho
def test_split_morfeusz_sents(morfeusz_analyzer):
    variant_list = morfeusz_analyzer.merge_morfeusz_variants(example_parse)
    sents, sents_txt = morfeusz_analyzer.split_morfeusz_sents(variant_list)
    assert len(sents) == 2
    assert len(sents_txt) == 2
    assert [s.strip() for s in sents_txt] == ['Dinozaur się zatrzymał.', 'Poszedł do domu.']
    assert len(sents[0]) == 4, 'wrong number of positions in the first sentence'
    assert len(sents[1]) == 4, 'wrong number of positions in the second sentence'
    # (first token in first position of the first sentence)
    assert sents[0][0][0][:2] == (0, 1), 'no positions at the start of the token'
    assert sents[0][0][0][2][:2] == ('Dinozaur ', 'dinozaur'), ('no form and base at the start of'
            ' the interp tuple')
    assert 'subst' in sents[0][0][0][2][2], 'possibly bad morphosyntactic part of the interp tuple'

@pytest.mark.pl_morpho
def test_unique_everseen(morfeusz_analyzer):
    assert list(morfeusz_analyzer.unique_everseen('AAAABBBCCDAABBB')) == ['A', 'B', 'C', 'D']
    assert list(morfeusz_analyzer.unique_everseen('ABBCcAD', str.lower)) == ['A', 'B', 'C', 'D']

@pytest.mark.pl_morpho
def test_analyze_positions(morfeusz_analyzer):
    analyzed = morfeusz_analyzer.analyze_positions(example_parse[:4],
            'Dinozaur się zatrzymał. Poszedł do domu.')
    assert len(analyzed) == 4
    assert analyzed[0] == {'start': 0, 'end': 1, 'offset': 0,
        'orth': 'Dinozaur ',
        'interps': [{'base': 'dinozaur', 'ctag': 'subst', 'msd': 'sg:nom:m', 'disamb': ()}]}
    analyzed = morfeusz_analyzer.analyze_positions(example_parse[4:],
            'Dinozaur się zatrzymał. Poszedł do domu.', start_offset=24)
    assert len(analyzed) == 4
    assert analyzed[2] == {'start': 6, 'end': 7, 'offset': 11,
        'orth': 'domu',
        'interps': [
            {'base': 'dom', 'ctag': 'subst', 'msd': 'sg:gen:m', 'disamb': ()},
            {'base': 'dom', 'ctag': 'subst', 'msd': 'sg:gen:m', 'disamb': ()},
            {'base': 'dom', 'ctag': 'subst', 'msd': 'sg:loc:m', 'disamb': ()},
            {'base': 'dom', 'ctag': 'subst', 'msd': 'sg:loc:m', 'disamb': ()},
            {'base': 'dom', 'ctag': 'subst', 'msd': 'sg:voc:m', 'disamb': ()},
            ],
        'nps': True}

@pytest.mark.pl_morpho
def test_parse_with_morfeusz(morfeusz_analyzer):
    sents, sents_txt = morfeusz_analyzer.parse_with_morfeusz(
            'Dinozaur się zatrzymał. Poszedł do domu.')
    assert len(sents) == 2
    assert len(sents_txt) == 2
    # (the returned sents_txt have the additional space markers for finding the positions)
    assert [s.strip() for s in sents_txt] == ['Dinozaur się zatrzymał.', 'Poszedł do domu.']
    assert len(sents[0]) == 4, 'wrong number of positions in the first sentence'
    assert len(sents[1]) == 4, 'wrong number of positions in the second sentence'
    # (first token in first position of the first sentence)
    assert sents[0][0][0][:2] == [0, 1], 'no positions at the start of the token'
    assert sents[0][0][0][2][:2] == ('Dinozaur ', 'dinozaur'), ('no form and base at the start of'
            ' the interp tuple')
    assert 'subst' in sents[0][0][0][2][2], 'possibly bad morphosyntactic part of the interp tuple'
    # The Concraft disambiguation.
    assert sents[1][0][0][3] == 1.0, 'bad Concraft disambiguation data for the "Poszedł" token'
    # (at least one of these should have likelihood of less than 1.0)
    assert sents[1][2][0][3] < 1.0 or sents[1][2][1][3] < 1.0, ('bad Concraft disambiguation data'
            ' for the "domu" token')
    # Change the verb endings so that there are more interpretations and the raw text reconstruction
    # might fail on the parse_with_morfeusz level.
    sents, sents_txt = morfeusz_analyzer.parse_with_morfeusz('Dziś się zatrzymałem. Poszedłeś.')
    assert len(sents_txt) == 2
    assert sents_txt[0].strip() == 'Dziś się zatrzymałem.'
    assert sents_txt[1].strip() == 'Poszedłeś.'

@pytest.mark.pl_morpho
def test_as_token_positions(morfeusz_analyzer):
    sents, sents_txt = morfeusz_analyzer.parse_with_morfeusz(
            'Dinozaur się zatrzymał. Poszedł do domu.'
            ' Potem skoczył. Kupił dużo kości.')
    pathed_sents = morfeusz_analyzer.as_token_positions(sents, sents_txt)
    # All the sentences parsed must be collected back after pathing.
    assert len(pathed_sents) == len(sents)
    assert len(pathed_sents[1]) == 4, 'wrong number of positions in the second sentence'
    assert pathed_sents[0][0]['orth'] == 'Dinozaur '
    assert pathed_sents[0][0]['interps'][0]['base'] == 'dinozaur'
    assert len(pathed_sents[1][2]['interps']) == 3, ('bad number of unique interps for the "domu"'
            ' token')
    assert (pathed_sents[1][2]['interps'][0]['disamb'][0] < 1.0
            or pathed_sents[1][2]['interps'][1]['disamb'][0] < 1.0), ('bad Concraft disambiguation'
                    ' data for the "domu" token')

@pytest.mark.pl_morpho
def test_add_paragraph_interps(db_path):
    connection = sqlite3.connect(db_path)
    setup_db(connection)
    with open(os.path.join(pathlib.Path(__file__).parent.resolve(), 'resources',
        'pl_test_models.yaml')) as f:
        morpho_yaml = yaml.safe_load(f)
    # This "edition", with rowid 1, will be assigned to the first "paragraph".
    connection.execute('INSERT INTO edition (title) VALUES (?)',
            ('Księga dinozaurów',))
    connection.execute('INSERT INTO paragraph (text, doc, edition) VALUES (?, ?, ?)',
            ('Dinozaur się zatrzymał.', 1, 1))
    connection.execute('INSERT INTO paragraph (text, edition) VALUES (?, ?)',
            ('<<meta paragraph>>', 2))
    connection.execute('INSERT INTO paragraph (text, doc, edition) VALUES (?, ?, ?)',
            ('Poszedł do domu.', 2, 2))
    assert len(connection.execute('SELECT text FROM paragraph').fetchall()) == 3
    # The first, edition-limited run should not touch the second paragraph.
    add_paragraphs_interps(connection, morpho_yaml, 'Księga dinozaurów')
    # (order by rowid so it's predictable)
    par_interps = connection.execute('SELECT text, text_interp FROM paragraph ORDER BY rowid'
            ).fetchall()
    assert par_interps[0][1], 'the first paragraphs gets no interpretation'
    assert json.loads(par_interps[0][1]), f'cannot load contents of {par_interps[0][0]}'
    assert len(json.loads(par_interps[0][1])) == 1, 'wrong number of sents in the 1st paragraph'
    assert len(json.loads(par_interps[0][1])[0]) == 4, ('wrong number of interps in the 1st'
            ' paragraph')
    assert json.loads(par_interps[0][1])[0][0]['orth'] == 'Dinozaur '
    assert not par_interps[1][1]
    assert not par_interps[2][1]
    # Run on everything.
    add_paragraphs_interps(connection, morpho_yaml)
    par_interps = connection.execute('SELECT text, text_interp FROM paragraph').fetchall()
    assert par_interps[0][1], '(full run) the first doc paragraph gets no interpretation'
    assert not par_interps[1][1], 'the meta paragraph should no receive an interpretation'
    assert par_interps[2][1], '(full run) the second doc paragraph gets no interpretation'
    assert json.loads(par_interps[0][1]), f'(full run) cannot load contents of {par_interps[0][0]}'
    assert json.loads(par_interps[2][1]), f'(full run) cannot load contents of {par_interps[0][0]}'
