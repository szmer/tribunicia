import sqlite3

from tribunicia.dbsetup import setup_db
from tribunicia.pagegen import pdf_as_pages, txt_dir_as_pages
from tribunicia.read_edition import _find_sections, read_edition
from tribunicia.structure_finder import StructureFinder


###-def test_find_sections(base_edition_settings, edition_path):
###-    struct_find = StructureFinder(settings=base_edition_settings)
###-    pagegen = txt_dir_as_pages(edition_path, pattern=r'tekst-\d+.txt$')
###-    sections = _find_sections(pagegen, struct_find)
###-    doc_n = 0
###-    for sec in sections:
###-        if len([par for pg, par in sec[1] if 'redakcyi' in par]) == 1:
###-            assert sec[0] == 'm', 'the redakcyi fragment should be meta because of line length'
###-        if len([par for pg, par in sec[1] if 'źyński' in par]) == 1:
###-            assert sec[0] == 'm', 'the źyński fragment should be meta because of fragment length'
###-        if sec[0] == 'd':
###-            doc_n += 1
###-            if doc_n == 2:
###-                assert len([par for pg, par in sec[1] if 'My rady' in par]) == 1, ('My rady'
###-                        f' lacking in {sec[1]}')
###-            if doc_n == 3:
###-                assert len([par for pg, par in sec[1] if '124. [Laudum conventus particularis'
###-                    in par]) == 1, (f'The heading 124. [Laudum... lacking in {sec[1]}')
###-    assert doc_n == 3, 'expected 3 documents'

def test_find_sections_special(base_date_settings, pdf_edition_path):
    struct_settings = base_date_settings
    struct_settings['heading_signs'] = ['Chapter']
    struct_find = StructureFinder(settings=struct_settings)
    pagegen = pdf_as_pages(pdf_edition_path)
    sections = _find_sections(pagegen, struct_find)
    assert len([s for s in sections if s[0] == 'd']) == 6
    pagegen = pdf_as_pages(pdf_edition_path)
    sections = _find_sections(pagegen, struct_find, strip_chapter_titles=True)
    assert len([s for s in sections if s[0] == 'd']) == 4
    pagegen = pdf_as_pages(pdf_edition_path)
    sections = _find_sections(pagegen, struct_find, strip_chapter_titles=True, merge_short_docs=1)
    sections = [s for s in sections if s[0] == 'd']
    assert len(sections) == 3
    assert sections[2][1][0][1].startswith('Chapter 943')

def test_read_edition(base_edition_settings, db_path, edition_path, edition_meta):
    connection = sqlite3.connect(db_path)
    setup_db(connection)
    # Read the edition to the prepared database.
    struct_find = StructureFinder(settings=base_edition_settings)
    pagegen = txt_dir_as_pages(edition_path, pattern=r'tekst-\d+.txt$')
    read_edition(pagegen, edition_meta, connection, struct_find)
    # Checks.
    eds = list(connection.execute('SELECT title, publisher, publ_date FROM edition'))
    assert len(eds) == 1
    assert eds[0][0] == 'Rządy sejmikowe w Polsce'
    assert eds[0][1] == 'Adolf Pawiński'
    assert eds[0][2] == '1888-01-01'
    docs = list(connection.execute('SELECT edition, title, source_entity FROM document'
        ' ORDER BY rowid'))
    assert len(docs) == 3
    for doc_n, doc in enumerate(docs):
        if doc_n > 0: # first doc has no good title
            assert doc[1].strip(), f'no title for doc {doc_n}'
        assert doc[0] == 1, 'bad edition indication'
        assert doc[2] == 'sejmik kujawski', 'bad source entity'
    pars = list(connection.execute('SELECT * FROM paragraph ORDER BY rowid'))
    redakcyi_encountered = False
    radziejewa_encountered = False
    laudumconv_encountered = False
    for par_n, par in enumerate(pars):
        if par[0]:
            # (take account for one-based sqlite numbering)
            assert par[0] == par_n + 2, f'invalid next paragraph indication for par. {par_n}'
        assert par[1] == 1, 'bad edition indication'
        if 'redakcyi' in par[4]:
            redakcyi_encountered = True
            assert not par[2], 'the redakcyi fragment should have no document linkage'
            assert par[3] == 131, 'bad page number for the redakcyi fragment'
        if '124. [Laudum conventus particularis' in par[4]:
            laudumconv_encountered = True
            assert par[2] == 3, ('the Laudum conventus fragment should have proper'
                    ' document linkage')
            assert par[3] == 132, 'bad page number for the Laudum conventus fragment'
        if 'Radziejewa' in par[4]:
            radziejewa_encountered = True
            assert par[2] == 3, 'the Radziejewa fragment should have proper document linkage'
            assert par[3] == 132, 'bad page number for the Radziejewa fragment'
    assert redakcyi_encountered
    assert radziejewa_encountered
    assert laudumconv_encountered, 'Missing the title of the (last) document'
