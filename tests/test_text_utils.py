# pylint: disable=missing-docstring
from tribunicia.text_utils import (
        join_linebreaks, get_constraint_pattern, find_constrained_subsequences
        )

def test_join_linebreaks():
    assert join_linebreaks('aaa bbbb-\nbb ccc') == 'aaa bbbbbb ccc'
    assert join_linebreaks('\n\naaa bbbb-\nbb ccc\n') == 'aaa bbbbbb ccc'
    assert join_linebreaks('aaa bbbb- x\nbb ccc') == 'aaa bbbbbb ccc'
    assert join_linebreaks('aaa bbbb- x\nbb ccc', clean_end_shades=False) == 'aaa bbbb- x bb ccc'

def test_get_constraint_pattern():
    patt = get_constraint_pattern(['a', 'b', 'c'])
    assert patt.pattern == '(@a@|@b@|@c@)+'
    assert patt.search('@a@@b@')
    assert not patt.search('@abc')

def test_find_constrained_subsequences():
    term_list = ['z', 'a', 'b', 'c', 'x', 'o', 'b', 'a', 'y']
    patt = get_constraint_pattern(['a', 'b', 'c'])
    result = find_constrained_subsequences(term_list, patt)
    assert result == [(1, 3), (6, 7)]
