from datetime import date

from tribunicia.structure_finder import StructureFinder

def test_doc_start_score():
    sf = StructureFinder(settings={
        'doc_start_signs': ['Title', '[Ww]ork'],
        'doc_start_signs2': ['[Ww]elcome'],
        'doc_start_sign2_bonus': 0.25,
        'nochron_penalty': 0
        })
    assert sf.doc_start_score('Title: A tale') < 0
    assert sf.doc_start_score('Welcome to the story') < 0
    assert sf.doc_start_score('Title: A tale. Welcome to our story') > 0
    assert sf.doc_start_score('An important work. Welcome to our story') > 0
    assert sf.doc_start_score('A Cat and Mouse. Welcome to our story') > 0

def test_extract_dates(base_date_settings):
    sf = StructureFinder(settings=base_date_settings)
    dt = sf.extract_dates('4. U niwe-rsal zjazdu do starostów o dawanie pomocy posłom, wysłanym do'
            ' k-ro\'lewnej Anny do Płocka, z Osieka- 4 października 1572 -r. eis huiuscemodi'
            ' litteris. Uw. Po akcie pomieszczone ~w rękopisie. mz k. 72—72')
    assert len(dt) == 1 and dt[0] == date(1572, 10, 4)
    dt = sf.extract_dates('9 V 1955')
    assert not dt # the late date should be rejected
    # (Check some additional examples from populobot)
    assert sf.extract_dates(
            '64. Wisnia, 24. maja 1683. Laudum sejmiku wiszeńskiego (fragment). ')[0] == date(
                    1683, 5, 24)
    assert sf.extract_dates('Działo się wRadziejowie die 22 Junii,anno 1705.')[0] == date(
            1705, 6, 22)
    assert sf.extract_dates('Februarius, 16. 1703')[0] == date(1703, 2, 16)
    assert sf.extract_dates('1650 Februarius, 3. 173')[0] == date(1650, 2, 3)
    # The anno domini indications.
    assert sf.extract_dates('(Junius, 28), anno Domini 1706.')[0] == date(1706, 6, 28)
    assert sf.extract_dates('Decemb. 20), anno Domini 1672.')[0] == date(1672, 12, 20)
    assert sf.extract_dates('Magdalenae (Jul. 21), anno Domini 1672. ')[0] == date(1672, 7, 21)
    assert sf.extract_dates('proxóna (Decemb. 23), auno. Domini 1671.')[0] == date(1671, 12, 23)
    # (Check after changing the max year)
    base_date_settings['max_year'] = 2000
    sf = StructureFinder(settings=base_date_settings)
    dt = sf.extract_dates('9 V 1955')
    assert len(dt) == 1 and dt[0] == date(1955, 5, 9)

def test_extract_paragraphs():
    text = """The geological distribution has a very important bearing on this matter of nomenclature.
    Since Hayden's original description (1857) the position of the Judith River Beds has been
    confirmed by Hatcher and Stanton as belonging to a lower horizon than the true Laramie Series,
    namely to the Ft. Pierre, and since all the Ceratopsia from the Judith River Beds belong to
    older and simpler forms than the Ceratopsia of the Laramie and Montana beds, it is highly
    probable that the reference by Cope and Lambe of the Edmonton Carnivore to a New Jersey
    Cretaceous genus, Dryptosaurus, and to a Judith River species (D. incrassatus), is incorrect.

    It appears certain that the Edmonton and Laramie carnivores are generically distinct from those
    of the Judith River Beds.

    The specimens upon which the latter genus [Deinodon] is based, consist of fragments of about a
    dozen teeth, of which three-fourths [types of Deinodon] are nearly identical in form with those
    of Megalosaurus, while the others [types of Aublysodon] are more or less peculiar."""
    sf = StructureFinder()
    sf.max_doc_line_len = 100
    pars = sf.extract_paragraphs(text)
    assert len(pars) == 3
    assert len(pars[0].split('\n')) == 7
    assert pars[1].startswith('It appears')
    assert pars[1].endswith('Judith River Beds.')

def test_guess_date(base_date_settings):
    text = """I735. September, lg. Radzie]. grodzka Relation. 18 f. 260.

    342. [Instructio pro comitiis ordinariis Varsaviæ celebrandis.]

    Actum in castro Radziäjovienyi feria quinta in crastino festi sancti Matht-æi
    apostoli et evangelistae (September, 22), anno Domini 1735. Obtulit nobilis Antomus
    Bieliński."""
    sf = StructureFinder(settings=base_date_settings)
    text = sf.extract_paragraphs(text)
    # The first one should extract the date from the title, the second one from the text.
    dt = sf.guess_date('46 September, 13. 1735', text)
    assert dt and (dt.day, dt.month, dt.year) == (13, 9, 1735)
    dt = sf.guess_date('Laudum Radziejoviense', text)
    assert dt and (dt.day, dt.month, dt.year) == (22, 9, 1735)

def test_heading_score():
    sf = StructureFinder(settings={
        'heading_signs': ['Laudum','Konstytucj[ae]'],
        'heading_signs2': ['Lublin', 'starost[ay]'],
        'nochron_penalty': 0
        })
    assert sf.heading_score('Abrakadabra hokus-pokus') < 0
    assert sf.heading_score('Konstytucje sejmiku w Lublinie') > 0
    assert sf.heading_score('Laudum starosty w Środzie') > 0
    assert sf.heading_score('Konstytucja sejmiku w Lublinie') > sf.heading_score(
            'Konstytucje sejmiku w Środzie')
    # Penalize lowercase and long paragraphs.
    assert sf.heading_score('Laudum starosty w Środzie') > sf.heading_score(
            'laudum starosty w środzie')
    assert sf.heading_score('Laudum sejmiku w Lublinie. Pojedziemy na łów towarzyszu mój'
            'na łów na łów na łowy do zielonej dąbrowy towarzyszu mój') < 0
    # Try with the nochron penalty.
    sf = StructureFinder(settings={
        'heading_signs': ['Laudum','Konstytucj[ae]'],
        'heading_signs2': ['Lublin', 'starost[ay]']
        })
    assert sf.heading_score('Konstytucje sejmiku w Lublinie') < 0, 'is nochron_penalty applied?'

def test_is_meta_fragment():
    sf = StructureFinder()
    assert not sf.is_meta_fragment('Jeże obłażą żelazne żerdzie wśród żałosnej pożogi')
    assert not sf.is_meta_fragment('Jeże obłażą żelazne żerdzie wśród żałosnej pożogi  '
            ), 'possibly a problem with internal tokenization'
    sf = StructureFinder(settings={'meta_signs': ['żelazne'],
        'heading_signs': ['Akt', 'Uroczysty'],
        'max_doc_line_len': 100})
    assert sf.is_meta_fragment('Jeże obłażą żelazne żerdzie wśród żałosnej pożogi')
    assert not sf.is_meta_fragment('Dinozaury wywędrowały za pożywieniem')
    # (A long line)
    assert sf.is_meta_fragment('Dinozaury wywędrowały za pożywieniem Dinozaury wywędrowały za'
            ' pożywieniemDinozaury wywędrowały za pożywieniemDinozaury wywędrowały za'
            ' pożywieniemDinozaury wywędrowały za pożywieniemDinozaury wywędrowały za pożywieniem')
    assert sf.is_meta_fragment('Dino 234973 5750309 5750309 3508 pożyw')
    # (Very short tokens)
    assert sf.is_meta_fragment('Dino. Za u Ry wy wę d ro wa ły za poży wie nie m')
    assert sf.is_meta_fragment('DINOZAURY WYWĘDROWAŁY ZA POŻYWIENIEM')
    assert not sf.is_meta_fragment('Uroczysty Akt o Dinozaurach 54 423 653')
    assert sf.is_meta_fragment('Oto Opowieść o Dinozaurach 54 423 653')
    # (Footnote detection)
    assert sf.is_meta_fragment('1. Dinozaury, książka o pożywieniu 3 ) jedzenie')
    # (Few char types)
    assert sf.is_meta_fragment('Dddd ww pppppppp')
    # (Abbreviations)
    assert sf.is_meta_fragment('Dinoz. wyw. za pożyw.')

def test_ocr_corrected():
    sf = StructureFinder(settings={'ocr_corrections': {'zelazne': 'żelazne', 'mal': 'mał'}})
    assert sf.ocr_corrected('zelazne malpiszony') == 'żelazne małpiszony'

def test_select_title():
    sf = StructureFinder(settings={
        'heading_signs': ['Laudum','Konstytucj[ae]'],
        'heading_signs2': ['Lublin', 'starost[ay]'],
        'nochron_penalty': 0
        })
    assert sf.select_title(['Abrakadabra hokus-pokus',
        'Konstytucje sejmiku w Lublinie']) == 'Konstytucje sejmiku w Lublinie'
    # The title here is too buried to be found.
    assert sf.select_title(['Abrakadabra hokus-pokus', '', '', '',
        'Konstytucje sejmiku w Lublinie']) == ''
    # The paragraph too long to be a title.
    assert sf.select_title(['Dinozaury wywędrowały za pożywieniem Dinozaury wywędrowały za'
            ' pożywieniemDinozaury wywędrowały za pożywieniemDinozaury wywędrowały za'
            ' pożywieniemDinozaury wywędrowały za pożywieniemDinozaury wywędrowały za ']) == ''
    
