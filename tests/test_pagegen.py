import os.path
import pathlib

import pytest

from tribunicia import pagegen

def test_pdf_as_pages(pdf_edition_path):
    gen = pagegen.pdf_as_pages(pdf_edition_path)
    pages = list(gen)
    assert len(pages) == 3
    assert [1, 2, 3] == [i[0] for i in pages], 'bad page numbers'
    assert pages[1][1].strip().startswith('Chapter 7. Random Moon adventures')
    assert pages[2][1].strip().endswith('landscape of the moon.')

def test_txt_dir_as_pages():
    test_path = os.path.join(pathlib.Path(__file__).parent.resolve(), 'resources/test_edition')
    gen = pagegen.txt_dir_as_pages(test_path)
    with pytest.raises(ValueError): # should capture readme.txt that has no number
        list(gen)
    gen = pagegen.txt_dir_as_pages(test_path, pattern=r'txt\d+\.txt$')
    assert list(gen) == [(1, '1\nhalo\n'), (2, '2\n'), (3, '3\n')]
    gen2 = pagegen.txt_dir_as_pages(test_path, pattern=r'txt\d+\.txt$', number_shift=10,
            exclude_ranges=[(0, 2)])
    assert list(gen2) == [(12, '2\n'), (13, '3\n')]
