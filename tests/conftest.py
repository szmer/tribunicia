from datetime import date
import os
import pathlib

import pytest

from tribunicia.doc_metadata import DocMetadata

def pytest_addoption(parser):
    parser.addoption('--pl_morpho', action='store_true', default=False,
            help='whether to run pl_morphoparsing tests (you must provide pl_test_models.yaml)')
    parser.addoption('--ner', action='store_true', default=False,
            help='whether to run NER-related tests (you need optional transformers and torch pkgs')

def pytest_configure(config):
        config.addinivalue_line('markers',
                'pl_morpho: uses and needs Morfeusz and Concraft models described by the user'
                ' in pl_test_models.yaml')
        config.addinivalue_line('markers',
                'ner: uses and needs transformers model defined in tribunicia.enrich_with_names')

def pytest_collection_modifyitems(config, items):
    if not config.getoption('--pl_morpho'):
        skip_pl_morpho = pytest.mark.skip(reason='need --pl_morpho option to run')
        for item in items:
            if 'pl_morpho' in item.keywords:
                item.add_marker(skip_pl_morpho)
    if not config.getoption('--ner'):
        skip_ner = pytest.mark.skip(reason='need --ner option to run')
        for item in items:
            if 'ner' in item.keywords:
                item.add_marker(skip_ner)

#
# Fixtures.
#
@pytest.fixture(scope='module')
def edition_path():
    return os.path.join(pathlib.Path(__file__).parent.resolve(), 'resources',
            'test_edition_with_texts')

@pytest.fixture(scope='module')
def pdf_edition_path():
    return os.path.join(pathlib.Path(__file__).parent.resolve(), 'resources',
            'testdoc.pdf')

@pytest.fixture(scope='module')
def edition_meta():
    return DocMetadata(source_entity='sejmik kujawski',
            edition_title='Rządy sejmikowe w Polsce',
            publisher='Adolf Pawiński',
            publ_date=date(1888, 1, 1))

@pytest.fixture(scope='function')
def db_path():
    test_db_path = 'pytest_read_edition_db.db'
    if os.path.exists(test_db_path):
        raise RuntimeError(f'{test_db_path} isn\'t supposed to exist in this directory, please'
                ' delete or rename it.')
    yield test_db_path
    os.remove(test_db_path)

@pytest.fixture(scope='function')
def base_edition_settings(base_date_settings):
    base_date_settings['heading_signs'] = ['Laudum', 'Limitatio']
    base_date_settings['doc_start_signs'] = ['[Aa]ctum in', 'My rady']
    base_date_settings['max_doc_line_len'] = 100
    return base_date_settings

@pytest.fixture(scope='function')
def base_date_settings():
    return {
            'nochron_penalty': 0,
            'months_pattern': '(stycze?[nń])|(luty?)|(marz?e?c)|(kwie[tc]i?e?[nń])|(maj)|(czerwi?e?c)'
                '|(lipi?e?c)|(sierpi?e?[nń])|(wrze[sś]i?e?[nń])|(październik)|(listopad)'
                '|(grudz?i?e?[nń])|([ji]anuar)|(februar)|(mart)|(april)|(mai)|([ji]u[nl])|(august)'
                '|(septemb)|(octob)|(decemb)',
            'month_names_to_numbers': [
                ("stycz", 1),
                ("janua", 1),
                ("ianua", 1),
                ("lut", 2),
                ("febr", 2),
                ("mar", 3),
                ("kwiet", 4),
                ("apr", 4),
                ("maj", 5),
                ("mai", 5),
                ("czerw", 6),
                ("jun", 6),
                ("iun", 6),
                ("lip", 7),
                ("jul", 7),
                ("iul", 7),
                ("sierp", 8),
                ("aug", 8),
                ("wrze", 9),
                ("sept", 9),
                ("paźdz", 10),
                ("octob", 10),
                ("list", 11),
                ("nov", 11),
                ("grud", 12),
                ("dece", 12),
                ]
            }
