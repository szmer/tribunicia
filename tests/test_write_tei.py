from collections import namedtuple
import json
import os
import pathlib
import re
import sqlite3

from lxml.etree import tostring
import pytest

from tribunicia.dbsetup import setup_db
from tribunicia.write_tei import write_tei_corpus_nkjp, TeiWriterNkjp

@pytest.fixture(scope='module')
def text_morpho_data():
    data_path = os.path.join(pathlib.Path(__file__).parent.resolve(), 'resources',
            'tei_test_data.json')
    with open(data_path) as inp:
        return json.load(inp)

@pytest.fixture(scope='function')
def filled_db_connection(db_path, text_morpho_data):
    connection = sqlite3.connect(db_path)
    setup_db(connection)
    connection.execute('INSERT INTO edition (title, editor, place) VALUES (?, ?, ?)',
            ('Akta sejmikowe', 'Jacyś ludzie', 'Kraków'))
    connection.execute('INSERT INTO document (edition, title, source_entity) VALUES (?, ?, ?)',
            (1, 'Dokument 1', 'Sejmik szlachecki'))
    par_query = ('INSERT INTO paragraph (doc, edition, pagenum, text, text_interp)'
            ' VALUES (?, ?, ?, ?, ?)')
    # (this one has an additional column)
    connection.execute(par_query.replace('interp)', 'interp, nextpar)')[:-1] + ', ?)',
            (1, 1, 1, text_morpho_data['text1'], json.dumps(text_morpho_data['repr1']), 2))
    connection.execute(par_query,
            (1, 1, 2, text_morpho_data['text2'], json.dumps(text_morpho_data['repr2'])))
    return connection

def test_segment_id():
    tw = TeiWriterNkjp()
    token_counter = { 'corp_name': 'abc', 'tok_num': 0 }
    assert tw.segment_id(token_counter) == 'segm_1.1-seg'
    assert tw.segment_id(token_counter) == 'segm_1.2-seg'

def test_segment_elem():
    tw = TeiWriterNkjp()
    # (make it "after pause", which is usually for punctuation and suffixes
    seg = tw.segment_elem('token-segm-1', 'garnek', 3, False, 'token-raw-1')
    seg_str = tostring(seg, encoding='unicode')
    assert '<w>garnek</w>' in seg_str
    assert 'corresp="text.xml#string-range(token-raw-1,3,6)"' in seg_str
    assert 'nps="true"' in seg_str

def test_morphos_segment_elem(text_morpho_data):
    tw = TeiWriterNkjp()
    token_counter = { 'corp_name': 'abc', 'tok_num': 2 }
    # (ignore the choice_found flag, the second value)
    seg, _ = tw.morphos_segment_elem(token_counter, text_morpho_data['repr1'][1][0], False)
    seg_str = tostring(seg, encoding='unicode')
    assert '<f name="orth"><string>Zakrzewskiego</string></f>' in seg_str
    assert '<f name="base"><string>zakrzewski</string></f>' in seg_str
    assert '<f name="ctag"><symbol xml:id="morph_1.2.1.ctag" value="adj"/></f>' in seg_str

def test_parenthesis_choice_elem(text_morpho_data):
    tw = TeiWriterNkjp()
    token_counter = { 'corp_name': 'abc', 'tok_num': 3 }
    choice = tw.parenthesis_choice_elem(text_morpho_data['repr1'][1][1], False,
            token_counter, 'token-raw-4')
    choice_str = tostring(choice, encoding='unicode')
    assert choice_str.startswith('<choice ')
    paren_found = False
    for chi, child in enumerate(choice.getchildren()):
        child_str = tostring(child, encoding='unicode')
        if child_str.startswith('<nkjp:paren '):
            paren_found = True
            for pchi, pchild in enumerate(child.getchildren()):
                assert pchild.tag == 'seg', f'element {pchi} inside the paren {chi} is not a <seg>'
                assert 'corresp="text.xml#string-range(' in tostring(pchild, encoding='unicode')
        else:
            assert 'corresp="text.xml#string-range(' in child_str
    assert paren_found, 'there is no <paren> element in the <choice>'

def test_sentence_elem():
    tw = TeiWriterNkjp()
    assert re.match('^<s.*/>$', tostring(tw.sentence_elem('elem-id'), encoding='unicode'))

def test_raw_paragraph_elem(text_morpho_data):
    tw = TeiWriterNkjp()
    par = tw.raw_paragraph_elem('par-1', text_morpho_data['text1'], 3, False)
    par_str = tostring(par, encoding='unicode')
    assert par_str.startswith('<p xml:id="par-1"><pb n="3"/>')
    assert par_str.endswith('prześwietny trybunał koronny</p>')

def test_paragraph_elem():
    tw = TeiWriterNkjp()
    assert (tostring(tw.paragraph_elem('par-id', 13), encoding='unicode')
            == '<p xml:id="par-id"><pb n="13"/></p>')

def test_raw_document(text_morpho_data):
    tw = TeiWriterNkjp()
    par1 = tw.raw_paragraph_elem('par-1', text_morpho_data['text1'], 1, False)
    par2 = tw.raw_paragraph_elem('par-2', text_morpho_data['text2'], False, False)
    doc = tw.raw_document_elem('doc-1', [par1, par2], 'assembly resolution')
    assert tostring(doc, encoding='unicode').startswith('<div ')
    assert len(doc.getchildren()) == 2
    assert text_morpho_data['text1'] in tostring(doc.getchildren()[0], encoding='unicode')
    assert text_morpho_data['text2'] in tostring(doc.getchildren()[1], encoding='unicode')

def test_tei_raw_subcorpus():
    tw = TeiWriterNkjp()
    corp, body = tw.tei_empty_subcorpus(name='Cave Guide', lang='en')
    corp_str = tostring(corp, encoding='unicode')
    assert 'href="header.xml"' in corp_str
    assert 'xml:lang="en"' in corp_str
    assert 'xml:id="Cave Guide_text"' in corp_str
    assert corp.xpath('.//body')[0] == body

def test_tei_raw_with_header(text_morpho_data, filled_db_connection):
    tw = TeiWriterNkjp()
    filled_db_connection.row_factory = sqlite3.Row
    edition = filled_db_connection.execute(
            'SELECT rowid, * FROM edition WHERE rowid = 1').fetchone()
    doc = filled_db_connection.execute(
            'SELECT rowid, * FROM document WHERE rowid = 1').fetchone()
    pars = filled_db_connection.execute(
            'SELECT * FROM paragraph WHERE doc = 1 ORDER BY nextpar').fetchall()
    # (extend the named tuple with the par_rows field)
    Doc = namedtuple('Doc', list(doc.keys()) + ['par_rows'])
    doc = Doc(par_rows=pars, **{key: doc[key] for key in doc.keys()})
    text_xml, header_xml = tw.tei_raw_with_header('Sejmiki', edition, 1, doc)
    text_xml_str = tostring(text_xml, encoding='unicode')
    assert text_xml_str.startswith('<teiCorpus ')
    assert '</body></text></TEI>' in text_xml_str 
    assert text_morpho_data['text1'] in text_xml_str
    assert text_morpho_data['text2'] in text_xml_str
    header_xml_str = tostring(header_xml, encoding='unicode')
    assert '<editor>Jacyś ludzie</editor>' in header_xml_str
    assert '<title level="s">Akta sejmikowe</title>' in header_xml_str
    assert '<title level="a">Dokument 1</title>' in header_xml_str
    assert '<bibl type="edition">' in header_xml_str
    assert '<pubPlace role="place">Kraków</pubPlace>' in header_xml_str

def test_tei_segmentation(filled_db_connection):
    tw = TeiWriterNkjp()
    filled_db_connection.row_factory = sqlite3.Row
    doc = filled_db_connection.execute(
            'SELECT rowid, * FROM document WHERE rowid = 1').fetchone()
    pars = filled_db_connection.execute(
            'SELECT * FROM paragraph WHERE doc = 1 ORDER BY nextpar').fetchall()
    # (extend the named tuple with the par_rows field)
    Doc = namedtuple('Doc', list(doc.keys()) + ['par_rows'])
    doc = Doc(par_rows=pars, **{key: doc[key] for key in doc.keys()})
    ann_segmentation_xml = tw.tei_segmentation('Sejmiki', doc)
    ann_segmentation_xml_str = tostring(ann_segmentation_xml, encoding='unicode')
    assert 'corresp="text.xml#string-range(txt_1.0-ab' in ann_segmentation_xml_str
    assert '<choice><nkjp:paren>' in ann_segmentation_xml_str

def test_tei_morphosyntax(filled_db_connection):
    tw = TeiWriterNkjp()
    filled_db_connection.row_factory = sqlite3.Row
    doc = filled_db_connection.execute(
            'SELECT rowid, * FROM document WHERE rowid = 1').fetchone()
    pars = filled_db_connection.execute(
            'SELECT * FROM paragraph WHERE doc = 1 ORDER BY nextpar').fetchall()
    # (extend the named tuple with the par_rows field)
    Doc = namedtuple('Doc', list(doc.keys()) + ['par_rows'])
    doc = Doc(par_rows=pars, **{key: doc[key] for key in doc.keys()})
    ann_morphosyntax_xml = tw.tei_morphosyntax('Sejmiki', doc)
    ann_morphosyntax_xml_str = tostring(ann_morphosyntax_xml, encoding='unicode')
    assert 'id="morph_0.0-s"' in ann_morphosyntax_xml_str
    assert '<f name="orth"><string>Zakrzewskiego</string></f>' in ann_morphosyntax_xml_str
    assert '<f name="base"><string>zakrzewski</string></f>' in ann_morphosyntax_xml_str
    assert re.search('<f name="ctag"><symbol xml:id=".*" value="adj"/></f>',
            ann_morphosyntax_xml_str)

def test_write_tei_corpus_nkjp(tmp_path, text_morpho_data, filled_db_connection):
    write_tei_corpus_nkjp(tmp_path, 'Sejmiki', filled_db_connection, 'Akta sejmikowe')
    assert os.path.isfile(tmp_path / 'Sejmiki' / 'Sejmiki-00000' / 'text.xml')
    with open(tmp_path / 'Sejmiki' / 'Sejmiki-00000' / 'text.xml') as inp:
        text = re.sub(r'\s\s+', '', inp.read())
        assert '</body></text></TEI>' in text
        assert re.sub(r'\s\s+', '', text_morpho_data['text1']) in text
        assert re.sub(r'\s\s+', '', text_morpho_data['text2']) in text
    assert os.path.isfile(tmp_path / 'Sejmiki' / 'Sejmiki-00000' / 'header.xml')
    with open(tmp_path / 'Sejmiki' / 'Sejmiki-00000' / 'header.xml') as inp:
        text = re.sub(r'\s\s+', '', inp.read())
        assert '<editor>Jacyś ludzie</editor>' in text
        assert '<title level="s">Akta sejmikowe</title>' in text
        assert '<title level="a">Dokument 1</title>' in text
        assert '<bibl type="edition">' in text
        assert '<pubPlace role="place">Kraków</pubPlace>' in text
    assert os.path.isfile(tmp_path / 'Sejmiki' / 'Sejmiki-00000' / 'ann_segmentation.xml')
    with open(tmp_path / 'Sejmiki' / 'Sejmiki-00000' / 'ann_segmentation.xml') as inp:
        text = re.sub(r'\s\s+', '', inp.read())
        assert 'corresp="text.xml#string-range(txt_1.0-ab' in text
        assert '<choice><nkjp:paren>' in text
    assert os.path.isfile(tmp_path / 'Sejmiki' / 'Sejmiki-00000' / 'ann_morphosyntax.xml')
    with open(tmp_path / 'Sejmiki' / 'Sejmiki-00000' / 'ann_morphosyntax.xml') as inp:
        text = re.sub(r'\s\s+', '', inp.read())
        assert 'id="morph_0.0-s"' in text
        assert '<f name="orth"><string>Zakrzewskiego</string></f>' in text
        assert '<f name="base"><string>zakrzewski</string></f>' in text
        assert re.search('<f name="ctag"><symbol xml:id=".*" value="adj"/></f>', text)
