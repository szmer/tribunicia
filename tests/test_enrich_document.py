import sqlite3

from tribunicia.dbsetup import setup_db
from tribunicia.enrich_document import add_is_sejmik_resolution
from tribunicia.pagegen import txt_dir_as_pages
from tribunicia.read_edition import read_edition
from tribunicia.structure_finder import StructureFinder

def test_add_is_sejmik_resolution(db_path, edition_path, base_edition_settings, edition_meta):
    connection = sqlite3.connect(db_path)
    setup_db(connection)
    # Read the edition to the prepared database.
    struct_find = StructureFinder(settings=base_edition_settings)
    pagegen = txt_dir_as_pages(edition_path, pattern=r'tekst-\d+.txt$')
    read_edition(pagegen, edition_meta, connection, struct_find)
    add_is_sejmik_resolution(connection, edition_meta.edition_title)
    sejmik_annotations = connection.execute('SELECT type FROM document').fetchall()
    assert sejmik_annotations[0][0] != 'sejmik resolution'
    assert sejmik_annotations[1][0] == 'sejmik resolution'
    assert sejmik_annotations[2][0] == 'sejmik resolution'
