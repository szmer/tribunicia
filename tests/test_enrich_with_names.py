import json
import os
import pathlib
import sqlite3

import pytest
import yaml

from tribunicia.dbsetup import setup_db
from tribunicia.enrich_with_names import (
        add_potential_surname_entities_ann,
        find_references_in_text,
        find_notes_in_foot_text,
        extract_main_and_foot_from_page,
        extract_refs_from_pars,
        extract_notes_from_pars,
        process_page_with_footnotes
        )
from tribunicia.pagegen import txt_dir_as_pages
from tribunicia.pl_morphoparsing import add_paragraphs_interps
from tribunicia.read_edition import read_edition
from tribunicia.structure_finder import StructureFinder

def test_find_references_in_text():
    text = 'Naprzód tedy zgodnie postanowieliśmy wszyscy na dzień 29 decembra zjachać się pod Śrzodą dla namówienia spólnego i postanowienia dalszego w tych rzeczach niżej opisanych|. A każdy ma przyjachać jako nalepi może do potrzeby, rozumiejąc, że żaden rycerskiego narodu% widząc takowy ogień i niebezpieczeństwo, z chęci swej przyjachać nie zamieszka, ale powinności swej, którą każdy nosi na sobie, przyjazno ojczyźnie dosyć uczyni. Jako poczynając od starszego, co by nami rządzieł i do którego byśmy się ściągać mieli, tedy przez p. Strykowskiego ! opowiedziawszy tuteczne sprawy jm. panu poznańskiemu 2, akomodując się we wszytkim prawu pospolitemu, wzywamy jm., aby za tak gwałtownym a nagłym niebezpieczeństwem, w niebytności jm. p. wojewody poznańskiego 3'
    assert find_references_in_text(text) == [(' opisanych|', 159), (' narodu%', 256), ('! ', 543),
                                             (' 2', 597), (' 3', 753)]

def test_find_notes_in_foot_text():
    text = '1 Stanistaw Karnkowski. * Vol. leg. II 1054—68. + Piotr Polulicki. ^ Władysław Stegozaur 2 Andrzej Opaliński, marszałek w. kor.'
    assert find_notes_in_foot_text(text) == [('1 ', 0), (' * ', 23), (' + ', 47),
                                             (' ^ ', 66), (' 2 ', 88)]

test_page_objs_cases = [[
            {
                'rowid': 100,
                'text': '28. Panów też Oporowskich młodych, synów nieboszczyka% p. Erazmego starosty kruświckiego, krzywda jasna a wielka pokazana jest, którzy nigdy nie będąc iuste et legitime pozwani ani wedle prawa sądzeni, i owszem, o żadnym pożwie ani prawie nie wiedząc, ani podlegli żadnym opiekunom albo osobom obcym, albo z kancelaryjej danym, wzięto im de facto *',
                'doc': 120,
            },
            {
                'rowid': 101,
                'text': 'po śmierci * K. J. M. starostwo kruświckie, z którego oni przez brata swego nieboszczyka ami przez nikogo innego skupieni nigdy nie beli, i owszem, im continua possessione od śmierci ojcowskiej w niej beli. A tak ofiarowali się wszyscy ichm. panowie i rycerstwo tu będący, że na tej elekcyjej tak się za nimi zastawić chcą, jakoby im posesyja ich dzierżawy przywrócona beła.',
                'doc': 120
            },
            {
                'rowid': 102,
                'text': '% xa VW tekście dopisane między wierszami.',
                'doc': None
            },
            {
                'rowid': 103,
                'text':  '* Vol. leg. II 807—9: Reces sejmu warszawskiego r. 1570, * Pol. ley. II 796—-5805,',
                'doc': None
            }
        ]]

def test_extract_refs_from_pars():
    main_pars = test_page_objs_cases[0][:2]
    refs = extract_refs_from_pars(main_pars)
    assert refs == [(100, ' nieboszczyka%', 40), (100, ' *', 346), (101, ' *', 10)]

def test_extract_notes_from_pars():
    foot_pars = test_page_objs_cases[0][2:]
    notes = extract_notes_from_pars(foot_pars)
    assert notes == [(102, '% ', 0), (103, '* ', 0), (103, ' * ', 56)]

@pytest.mark.parametrize(
        "input_pars,out_rowids",
        list(zip(test_page_objs_cases, 
                 [[[100, 101], [102, 103]]]))
)
def test_extract_main_and_foot_from_page(input_pars, out_rowids):
    main, foot = extract_main_and_foot_from_page(input_pars)
    assert [p['rowid'] for p in main] == out_rowids[0]
    assert [p['rowid'] for p in foot] == out_rowids[1]

@pytest.mark.parametrize(
        "input_pars,out_objs",
        list(zip(test_page_objs_cases,
                 [[
                     {'par_row_id': 100, 'offset': 40, 'ref': ' nieboszczyka%',
                      'note': '% xa VW tekście dopisane między wierszami.', 'confidence_score': 1.0},
                     {'par_row_id': 100, 'offset': 346,  'ref': ' *',
                      'note': '* Vol. leg. II 807—9: Reces sejmu warszawskiego r. 1570,',
                      'confidence_score': 1.0},
                     {'par_row_id': 101, 'offset': 10, 'ref': ' *',
                      'note': ' * Pol. ley. II 796—-5805,', 'confidence_score': 1.0}
                     ]]))
)
def test_process_page_with_footnotes(input_pars, out_objs):
    result = process_page_with_footnotes(input_pars)
    assert result == out_objs

@pytest.mark.ner
def test_add_is_sejmik_resolution(db_path, edition_path, base_edition_settings, edition_meta):
    connection = sqlite3.connect(db_path)
    setup_db(connection)
    # Read the edition to the prepared database.
    struct_find = StructureFinder(settings=base_edition_settings)
    pagegen = txt_dir_as_pages(edition_path, pattern=r'tekst-\d+.txt$')
    read_edition(pagegen, edition_meta, connection, struct_find)
    add_potential_surname_entities_ann(connection, edition_meta.edition_title)
    surname_annotations = connection.execute('SELECT text_potential_surnames, text '
                                             'FROM paragraph '
                                             'WHERE text LIKE \'%Głębocki%\'').fetchall()
    assert len(surname_annotations) == 1
    ner_annot = json.loads(surname_annotations[0][0])
    assert len(ner_annot) >= 1
    assert ner_annot[0]['kind'] == 'PER'
    assert ner_annot[0]['word'] == 'Głębocki'

    surname_annotations2 = connection.execute('SELECT text_potential_surnames, text '
                                             'FROM paragraph '
                                             'WHERE text LIKE \'%Radziejewa%\'').fetchall()
    assert len(surname_annotations2) >= 1
    ner_annot2 = json.loads(surname_annotations2[0][0])
    assert len(ner_annot2) >= 1
    idx2 = [item['word'] for item in ner_annot2].index('Radziejewa')
    assert ner_annot2[idx2]['kind'] == 'LOC'

    surname_annotations3 = connection.execute('SELECT text_potential_surnames, text '
                                             'FROM paragraph '
                                             'WHERE text LIKE \'%Jarnowskim%\'').fetchall()
    assert len(surname_annotations3) == 1
    ner_annot3 = json.loads(surname_annotations3[0][0])
    assert len(ner_annot3) >= 1
    idx3 = [item['word'] for item in ner_annot3].index('Jarnowskim')
    assert ner_annot3[idx3]['kind'] == 'PER'

#    # Not detected.
#    surname_annotations3 = connection.execute('SELECT text_potential_surnames, text '
#                                             'FROM paragraph '
#                                             'WHERE text LIKE \'%Bajerski%\'').fetchall()
#    assert len(surname_annotations3) == 1
#    ner_annot3 = json.loads(surname_annotations3[0][0])
#    assert len(ner_annot3) >= 1
#    idx3 = [item['word'] for item in ner_annot3].index('Bajerskiego')
#    assert ner_annot[idx3]['kind'] == 'PER'

@pytest.mark.ner
@pytest.mark.pl_morpho
def test_add_is_sejmik_resolution_morpho(db_path, edition_path, base_edition_settings, edition_meta):
    connection = sqlite3.connect(db_path)
    setup_db(connection)
    with open(os.path.join(pathlib.Path(__file__).parent.resolve(), 'resources',
        'pl_test_models.yaml')) as f:
        morpho_yaml = yaml.safe_load(f)
    # Read the edition to the prepared database.
    struct_find = StructureFinder(settings=base_edition_settings)
    pagegen = txt_dir_as_pages(edition_path, pattern=r'tekst-\d+.txt$')
    read_edition(pagegen, edition_meta, connection, struct_find)
    add_paragraphs_interps(connection, morpho_yaml, edition_meta.edition_title)
    add_potential_surname_entities_ann(connection, edition_meta.edition_title)
    surname_annotations = connection.execute('SELECT text_potential_surnames, text '
                                             'FROM paragraph '
                                             'WHERE text LIKE \'%Głębocki%\'').fetchall()
    ner_annot = json.loads(surname_annotations[0][0])
    try:
        ent_idx = ['Głębocki' in item['word'] for item in ner_annot].index(True)
    except ValueError:
        pytest.fail(f'No mention of Głębocki in surname annotations: {ner_annot}')
    assert 'morpho_interp' in ner_annot[ent_idx]
    assert ner_annot[ent_idx]['morpho_interp']['base'] == 'Głębocki'

    surname_annotations2 = connection.execute('SELECT text_potential_surnames, text '
                                             'FROM paragraph '
                                             'WHERE text LIKE \'%Radziejewa%\'').fetchall()
    ner_annot2 = json.loads(surname_annotations2[0][0])
    try:
        ent_idx2 = ['Radziejew' in item['word'] for item in ner_annot2].index(True)
    except ValueError:
        pytest.fail(f'No mention of Radziejew in surname annotations: {ner_annot2}')
    assert 'morpho_interp' in ner_annot2[ent_idx2]
    assert ner_annot2[ent_idx2]['morpho_interp']['base'] == 'Radziejewa' # unrecognized lemma

    surname_annotations3 = connection.execute('SELECT text_potential_surnames, text '
                                             'FROM paragraph '
                                             'WHERE text LIKE \'%Jarnowskim%\'').fetchall()
    ner_annot3 = json.loads(surname_annotations3[0][0])
    try:
        ent_idx3 = ['Jarnowskim' in item['word'] for item in ner_annot3].index(True)
    except ValueError:
        pytest.fail(f'No mention of Jarnowskim in surname annotations: {ner_annot3}')
    assert 'morpho_interp' in ner_annot3[ent_idx3]
    assert ner_annot3[ent_idx3]['morpho_interp']['base'] == 'Jarnowskim' # unrecognized lemma
