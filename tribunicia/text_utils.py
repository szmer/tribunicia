"""
Additional utilities for processing raw text that are not dependent on parameters that would be
passed to a StuctureFinder object.
"""
import re

# Regular expressions for typical document elements to detect sejmik resolutions.
myrady = re.compile('[^\\w]{0,4}my.? r[au]d', flags=re.IGNORECASE)
resolution_titles = [re.compile(s) for s in [
    'Artyk', 'Articuli', 'Postanowien', 'Uchwał[ay]', 'Deklarac', 'Laudu?m?a?', 'Konfedera',
    'Instru[kc]', 'Kwit\\s', 'Pokwitowan', 'Punkt[ay]']]

def is_sejmik_resolution(title, paragraphs):
    """
    Assess whether the Section class object is a resolution of a sejmik assembly. Similar functions
    could be used to assess research pertinence of a section in different scenarios.
    """
    # Consider first actual paragraphs.
    if len(paragraphs) <= 1:
        return False # no-content sections are nonpertinent
    if 'sędziowie' in paragraphs[1]: # judicial lauda from Ruthenia
        return False
    for par in paragraphs:
        if myrady.match(par) is not None:
            return True
    # Consider the title.
    signs_pert_titles = [s.search(title) for s in resolution_titles]
    if len([s for s in signs_pert_titles if s is not None]) > 0:
        return True
    return False

def join_linebreaks(text, clean_end_shades=True):
    """
    Join a text split into lines, optionally removing junk characters occuring at the end due
    to OCR being confused by shades.
    """
    lines = text.split('\n')
    joined_text = ''
    for l_i, line in enumerate([l for l in lines if l]):
        if clean_end_shades:
            if re.search(' [^aeikouwyz]$', line, flags=re.I) is not None:
                line = line[:-2]
        if len(joined_text) > 0:
            if joined_text[-1] == '-' and line[0].islower():
                joined_text = joined_text[:-1] + line
                continue
        joined_text += (' ' if l_i > 0 else '') + line
    return joined_text

def get_constraint_pattern(get_constraint_set):
    "Produce a regex pattern from terms for use with find_constrained_subsequences."
    return re.compile('(' + '|'.join([f'@{a}@' for a in get_constraint_set]) + ')+')

def find_constrained_subsequences(term_list, constraint_pattern):
    "Return a list of index pairs where terms from constraints can be found."
    term_search_str = '@' + '@@'.join(term_list) + '@'
    found = constraint_pattern.search(term_search_str)
    if not found:
        return []

    length_tracker = 0
    search_str_idx_to_term_list = dict()
    for tn, term in enumerate(term_list):
        search_str_idx_to_term_list[length_tracker] = tn
        length_tracker += 2 + len(term)
    index_pairs = []
    for match in constraint_pattern.finditer(term_search_str):
        index_pairs.append((search_str_idx_to_term_list[match.start()],
                            search_str_idx_to_term_list[match.start()
                                                        + match.group().rfind('@@')+1]))
    return index_pairs
