import itertools
import json
import sqlite3

from tribunicia.concraft_pl2 import Concraft
from morfeusz2 import Morfeusz
import pexpect

def add_paragraphs_interps(sqlite: sqlite3.Connection, models_settings: dict, edition=None):
    """
    Add Polish morphosyntactic interpretations to paragraphs existing in the database. Optionally
    you can limit the operation to only one `edition`. The information format is JSON-ified output
    of `MorfeuszAnalyzer.as_token_positions` method ran on the text after Morfeusz and Concraft
    parsing. It is written to the `text_interp` column for the paragraphs, only for the ones that
    are part of a document (i.e., not the "meta" paragraphs).

    Args:
    - `sqlite` - connection to a database where you have the paragraphs already written.
    - `models_settings` - a dictionary, where keys are the same as the arguments to the
        MorfeuszAnalyzer constructor. From these, the required arguments to that method are also
        required here.
    - `edition` - if you provide the title of the edition, as string, only the paragraphs from that
        edition will be parsed. Otherwise the function works on all the paragraphs that exist in the
        database.
    """
    # Collect the optional keyword arguments to the MorfeuszAnalyzer from model_settings.
    models_kwargs = dict()
    for key in ['morfeusz_chunk_size', 'concraft_port', 'concraft_mem_allocation']:
        if key in models_settings:
            models_kwargs[key] = models_settings[key]
    analyzer = MorfeuszAnalyzer(
            models_settings['morfeusz_dict_dir'],
            models_settings['morfeusz_dict_name'],
            models_settings['concraft_model_path'],
            **models_kwargs
            )
    query = 'SELECT rowid, text FROM paragraph WHERE doc IS NOT NULL'
    if edition is not None:
        edition_num_row = sqlite.execute('SELECT rowid FROM edition WHERE title = ?',
                (edition,)).fetchone()
        if edition_num_row is None:
            raise RuntimeError(f'cannot find edition "{edition}" in the database')
        query += ' AND edition = ?'
        paragraph_rows = sqlite.execute(query, (edition_num_row[0],))
    else:
        paragraph_rows = sqlite.execute(query)
    row = paragraph_rows.fetchone()
    while row is not None:
        # (row[0] is the rowid, row[1] is the text)
        parsed_sents, sents_txt = analyzer.parse_with_morfeusz(row[1])
        pathed_sents = analyzer.as_token_positions(parsed_sents, sents_txt)
        sqlite.execute('UPDATE paragraph SET text_interp = ? WHERE rowid = ?', (
            json.dumps(pathed_sents),
            row[0]))
        row = paragraph_rows.fetchone()
    sqlite.commit()

class MorfeuszAnalyzer():
    """
    The class of object dealing with obtaining the morphosyntactic information about Polish text
    with Morfeusz and Concraft tools.

    Adapted from Ameba by Dorota Komosińska (GNU GPL 2+ licensed). The methods taken from there,
    apart from __init__, are marked by (A) in the docstring.
    """
    def __init__(self, morfeusz_dict_dir, morfeusz_dict_name, concraft_model_path,
            morfeusz_chunk_size=2500, concraft_port=3000, concraft_mem_allocation=256):
        """
        Create a MorfeuszAnalyzer object.

        Args:
        - `morfeusz_dict_dir` - the path to the directory where the Morfeusz dictionary resides.
        - `morfeusz_dict_name` - the name of the dictionary (in `morfsz_dict_dir`). Its real file
            name is expected to be `<ARG>-a.dict`, so e.g. give "korbeusz" here if the real name is
            `korbeusz-a.dict`.
        - `concraft-model-path` - the path to the Concraft model `.gz` file.
        - `morfeusz_chunk_size` - the size, in characters, of a chunk that should be sent to Morfeusz
            for analysis. If the text to parse is longer, it will be split into multiple chunks.
        - `concraft_port` - the port where the Concraft server should run. You can change it if you
            run something else on the default port.
        - `concraft_mem_allocation` - the memory, in MB, which should be allocated by the Concraft
            server/parser. Theoretically the more, the better, to a point at least.
        """
        self.morfsz_chunk_size = morfeusz_chunk_size
        self.morfeusz = Morfeusz(dict_path=morfeusz_dict_dir, dict_name=morfeusz_dict_name,
                generate=False, expand_tags=True,
                whitespace=302) # append whitespaces to preceding tokens
        self.concraft_port = concraft_port
        self.concraft_serv = pexpect.spawn(
                'concraft-pl server --port={} -i {} +RTS -N -A{}M'.format(
                    concraft_port, concraft_model_path, concraft_mem_allocation))
        self.concraft_serv.expect('ctrl-c to quit')
        self.concraft = Concraft()

    def create_interp(self, interp, base, disamb):
        """
        (A) Make a dictionary for the morphosyntactic intepretation. Inside, `base` should be the
        lemma, `ctag` the part-of-speech tag, `msd` contain the additional morphosyntactic data,
        and `disamb` the Concraft data.
        """
        struc_data = interp.split(':', 1)
        if len(struc_data) == 1:
            struc_data.append('')
        ctag, rest_tag = struc_data
        return { 'base': base, 'ctag': ctag, 'msd': rest_tag, 'disamb': disamb }

    def make_pos_entry(self, by_start, pos, source_text):
        """
        (A) Prepare the position entries for the `pos`(ition) with a dictionary of token dictionary
        lists, sorted `by_start` (position). The entries may represent "nesting" inside the position
        if there are possible paths? (the TEI "choice" lists). This method is used by
        `analize_positions`. The second value is the ending position.
        """
        try:
            by_start[pos]
        except:
            raise RuntimeError(f'Missing an entry for text: {source_text}, pos: {pos}')
        if len(by_start[pos]) == 1:
            ret = by_start[pos][0]
            retpos = ret['end']
        elif len(by_start[pos]) == 0:
            raise RuntimeError("missing segment?")
        else:
            choices = [{'s' : x["start"], 'e' : x["end"], "content" : [x]} for x in by_start[pos]]
            p = pos
            while max([x["e"] for x in choices]) != min([x["e"] for x in choices]):
                # musi być min(), bo inaczej nie uwzględnia gałęzi "wstecz" i zaczyna czytać poza
                # ostatnim indeksem
                p = min([x["e"] for x in choices])
                for c in choices:
                    if c["e"] > p:
                        continue
                    nested_c, pp = self.make_pos_entry(by_start, p, source_text)
                    c["content"].append(nested_c)
                    c["e"] = pp
            ret = [c["content"] for c in choices]
            retpos = choices[0]["e"]
        return ret, retpos

    def merge_morfeusz_variants(self, morfeusz_output):
        """
        With output from the Morfeusz Python binding, transform the tuples of
        `(start_node, end_node, (interp...))`
        into lists of nodes of
        `[(start_node, end_node, interp...), ...]`,
        merging variants of the same start-end position into one list each.
        """
        positions_lists = dict() # start_end -> ready lists
        for node_n, node in enumerate(morfeusz_output):
            key = '{}_{}'.format(node[0], node[1])
            if not key in positions_lists:
                positions_lists[key] = []
            positions_lists[key].append(node)

        def position_sorter(key):
            start, end = tuple(key.split('_'))
            return int(start)*1000 + int(end)
        return [item[1] for item in sorted(positions_lists.items(),
            key=lambda item: position_sorter(item[0]))]

    def split_morfeusz_sents(self, morfeusz_nodes):
        """
        Given a Morfeusz representation produces by `merge_morfeusz_variants`, return it as a list
        of sentences (detected with punctuation). The second returned value is the list of texts of
        the sentences.
        """
        sent_boundaries = [0]
        sents_txt = ['']
        texted_start_positions = set() # get one text version for each Morfeusz DAG position
        texted_end_positions = set()
        previous_brev = False
        previous_spacer = False # if the previous token was followed by a space
        for (node_n, node) in enumerate(morfeusz_nodes):
            current_brev = False
            for vi, variant in enumerate(node):
                interp = variant[2]
                # If we haven't added text for this path in the sentence graph, add the form to text.
                if (not variant[0] in texted_start_positions
                        and not variant[1] in texted_end_positions):
###                    if previous_spacer:
###                        sents_txt[-1] += ' '
                    sents_txt[-1] += interp[0]
                    texted_start_positions.add(variant[0])
                    texted_end_positions.add(variant[1])
                    if interp[0][-1] == ' ':
                        previous_spacer = True
                    else:
                        previous_spacer = False
                if 'brev' in interp[2]:
                    previous_brev = True
                    current_brev = True
                if interp[0].strip() in ['.', '!', '?'] and not previous_brev:
                    sent_boundaries.append(node_n+1)
                    sents_txt.append('')
            if not current_brev:
                previous_brev = False
        sent_boundaries.append(len(morfeusz_nodes))
        sents = []
        for (bnd_n, bnd) in enumerate(sent_boundaries[1:]):
            # bnd_n is effectively bnd_n-1, because of skipping first element
            sents.append(morfeusz_nodes[sent_boundaries[bnd_n]:bnd])
        sents = [s for s in sents if len(s) > 0]
        sents_txt = [s for s in sents_txt if len(s) > 0]
        return sents, sents_txt

    def unique_everseen(self, iterable, key=None):
        """
        (A) List unique elements, preserving order, in a generator. Remember all elements ever seen.
        The `key` function can be provided to be applied on elements for comparing them.

        unique_everseen('AAAABBBCCDAABBB') --> A B C D
        unique_everseen('ABBCcAD', str.lower) --> A B C D
        """
        seen = set()
        seen_add = seen.add
        if key is None:
            for element in itertools.filterfalse(seen.__contains__, iterable):
                seen_add(element)
                yield element
        else:
           for element in iterable:
                k = key(element)
                if k not in seen:
                    seen_add(k)
                    yield element

    def analyze_positions(self, morf_result, source_text, start_offset=0):
        """
        (A) Given a sentence of Morfeusz output (`morf_result`) and the `source_text`, return a list
        of dictionaries representing the token positions along with their possible interpretations
        (instead of potential tokens for every positions lying loose). See the `as_token_positions`
        method for the dict format; that method is intended for use wth a sequence of sentences.
        `Start_offset` should be used when `morf_result` text starts on some character position
        further in the `source_text`.
        """
        segs = {}
        by_start = {}
        # Convert the interpretation row entries to data dictionaries.
        for row in morf_result:
            if len(row) < 3:
                continue
            start_node = int(row[0])
            end_node = int(row[1])
            interp_part = row[2]
            disamb = row[3:]
            word = interp_part[0]
            base = interp_part[1]
            interp = interp_part[2]
            if (start_node, end_node) not in segs:
                seg = {
                        'orth' : word,
                        'interps' : [self.create_interp(interp, base, disamb)],
                        'start' : start_node,
                        'end' : end_node
                        }
                segs[(start_node, end_node)] = seg
                if start_node not in by_start:
                    by_start[start_node] = []
                by_start[start_node].append(seg)
            else:
                segs[(start_node, end_node)]['interps'].append(
                        self.create_interp(interp, base, disamb))
        # Collect the position information to order the tokens by positions.
        first_start = None # start graph node of the first token
        eoffsets = dict() # start offset -> end offset of the word
        boffsets = {} # beginning offsets
        maxpos = -1
        for pos, sgs in sorted(by_start.items()):
            for seg in sgs:
                if not eoffsets:
                     # initialize with the empty first graph edge entry
                    eoffsets[seg['start']] = 0
                    first_start = seg['start']
                # expected beginning and end of the token
                found_b_offset = source_text[eoffsets[seg['start']]+start_offset:].find(seg['orth'])
                if found_b_offset == -1:
                    raise RuntimeError(f'bad offsets, cannot find {seg["orth"]} from index '
                            f'{eoffsets[seg["start"]]} + {start_offset}')
                b = found_b_offset + eoffsets[seg['start']]
                e = b + len(seg['orth'])
                if seg['start'] in boffsets and b != boffsets[seg['start']]:
                    raise RuntimeError('token position problem')
                if seg['end'] in eoffsets and e != eoffsets[seg['end']]:
                    raise RuntimeError('token position problem')
                boffsets[seg['start']] = b
                eoffsets[seg['end']] = e
                seg['offset'] = boffsets[seg['start']]
                if eoffsets[seg['start']] == boffsets[seg['start']] and seg['start'] > 0:
                    seg['nps'] = True
            maxpos = pos
        ret = []
        pos = first_start
        while pos <= maxpos:
            c, pos = self.make_pos_entry(by_start, pos, source_text)
            ret.append(c)
        return ret

    def parse_with_morfeusz(self, text):
        """
        The basic function to get output from the Morfeusz analyzer for a (possibly) long text.
        Returns a list of sentences (may not be accurate because of chunking), as lists of token
        entry lists. The second returned value is a list of sentence texts as strings.

        The format of the return value is <sentences> -> <token position lists> ->
        [<start node>, <end node>, (<form>, <base>, <interp>...), <disambiguation probability>, ...]
        (... indicate additional lexicographical data that may be added by Morfeusz and Concraft).
        """
        morfsz_sentences = []
        morfsz_sentences_txt = []
        parsed_boundary = 0 # track where we left the parsing after the previous chunk
        while len(text) != parsed_boundary:
            prev_parsed_boundary = parsed_boundary
            # Find the word ending to end the next chunk.
            parsed_boundary = text[:parsed_boundary+self.morfsz_chunk_size].rfind(' ')
            if parsed_boundary == -1 or (
                    prev_parsed_boundary+self.morfsz_chunk_size >= len(text)):
                # (at the end of the paragraph text)
                parsed_boundary = len(text)
            text_chunk = text[prev_parsed_boundary:parsed_boundary]
            # Get the analysis with disambiguation, group the intepretations of the same token and
            # split the token sequence into sentence sub-subsequences.
            parsed_nodes = self.concraft.disamb(self.morfeusz.analyse(text_chunk))
            # (remove the empty strings that we can get from Concraft)
            parsed_nodes = [n for n in parsed_nodes if n]
            for node_n in range(len(parsed_nodes)):
                # Convert Concraft disamb likelihoods to floats.
                parsed_nodes[node_n] = (list(parsed_nodes[node_n][:3])
                        + [float(parsed_nodes[node_n][3])]
                        + list(parsed_nodes[node_n][4:]))
            parsed_nodes = self.unique_everseen(parsed_nodes,
                    key=lambda x: f'{x[0]}-{x[1]}-{x[2][:3]}')
            parsed_nodes = self.merge_morfeusz_variants(parsed_nodes)
            sents, sents_txt = self.split_morfeusz_sents(parsed_nodes)
            morfsz_sentences += sents
            morfsz_sentences_txt += sents_txt
        return morfsz_sentences, morfsz_sentences_txt

    def as_token_positions(self, parsed_sents, sents_txt):
        """
        Convert output sentences of `parse_with_morfeusz` to lists of token positions. Each of these
        positions contain a dictionary with the token position (`start`, `end`, `offset` keys),
        the word form (`orth`), a list of possible `interps`. The `disamb`[iguation] data is
        included inside each of the interp dictionaries; the first item there is the actual
        likelihood.
        """
        pathed_sentences = []
        sent_counter = 0
        for sent_n, morf_sent in enumerate(parsed_sents):
            # Get a list of token postions with their interps.
            morf_sent = sum(morf_sent, start=[]) # flatten the list of sentence token interps
            tokens_interps = self.analyze_positions(morf_sent, sents_txt[sent_n])
            # Extract sentences from the tokens_interps.
            sent_start = 0
            for tok_n, token in enumerate(tokens_interps):
                # Extract some real token (a dictionary) from the nested lists of possible
                # interpretation paths. With a 'real' token we can orient ourselves in the sequence
                # of token positions and sentences.
                real_token = token
                while type(real_token) != dict:
                    real_token = real_token[0]
                # Detect the tokens that are at the end of original sentences and split the pathed
                # sentences at that point.
                if tok_n == len(tokens_interps) or real_token['end'] == int(
                        parsed_sents[sent_counter][-1][0][1]):
                    pathed_sentences.append(tokens_interps[sent_start:tok_n+1])
                    sent_start = tok_n+1
                    sent_counter += 1
        return pathed_sentences
