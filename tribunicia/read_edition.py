"Reading the documents and other sections from the editions."
from collections.abc import Iterable
import re
import sqlite3

from tribunicia.doc_metadata import DocMetadata
from tribunicia.structure_finder import StructureFinder
from tribunicia.text_utils import join_linebreaks

def read_edition(pages: Iterable, metadata: DocMetadata, sqlite: sqlite3.Connection,
        struct_find: StructureFinder, dupl_action='REPLACE', strip_chapter_titles=False,
        book_title=None, merge_short_docs=False, double_newlines=False):
    """
    Read the edition of documents from the `pages` and put it to the database through `sqlite`. Use
    `metadata` to inform about the edition; the proper `struct_find` object is needed to detect the
    documents inside.

    Args:
    - `pages`: should provide the pages of the edition as pairs (page number: int, text: string).
    - `metadata`: associated with the whole edition, will be associated with all the documents.
    - `sqlite`: the sqlite3 connection where the extracted documents will be written.
    - `struct_find`: a StructureFinder instance that will be used to recognize the documents'
        structure.
    - `dupl_action`: what to do if an entry for an edition, document, paragraph etc. already
        exists; must be one of the sqlite `INSERT OR...` keywords (`ABORT`, `FAIL`, `IGNORE`,
        `REPLACE`, `ROLLBACK`).
    - `strip_chapter_titles` - if True, it will attempt to remove chapter titles that may appear
        at the top of the page in some books.
    - `book_title` - if `strip_chapter_titles`, this is added to the possible titles to strip.
    - `merge_short_docs` - if a number x, documents of <= x paragraphs shorter than 100x chars are
        merged with the subsequent documents (assumed to be multiple different-level chapter names).
    """
    sections = _find_sections(pages, struct_find, strip_chapter_titles=strip_chapter_titles,
                              book_title=book_title, merge_short_docs=merge_short_docs,
                              double_newlines=double_newlines)
    # Write what we found to the database, starting with the edition metadata.
    sqlite.execute(f'INSERT OR {dupl_action}'
            ' INTO edition (title, editor, publisher, place, publ_date)'
            ' VALUES (?, ?, ?, ?, ?)',
            (metadata.edition_title, metadata.editor, metadata.publisher,
                metadata.publ_place, metadata.publ_date.isoformat()))
    sqlite.commit()
    publ_rowid = sqlite.execute('SELECT last_insert_rowid()').fetchone()[0]
    # Write paragraphs, creating the document rows if necessary.
    previous_rowid = None
    inserted_meta_idxs = set()
    last_doc_page_n = 0
    def insert_meta(sec):
        nonlocal previous_rowid
        sqlite.execute(f'INSERT OR {dupl_action}'
                ' INTO paragraph (edition, pagenum, text)'
                ' VALUES (?, ?, ?)',
                # Use just the first page, paragraph item in the second pair of the section
                # entry.
                (publ_rowid, sec[1][0][0], sec[1][0][1]))
        par_rowid = sqlite.execute('SELECT last_insert_rowid()').fetchone()[0]
        if previous_rowid is not None:
            sqlite.execute('UPDATE paragraph SET nextpar = ? WHERE rowid = ?',
                    (par_rowid, previous_rowid))
        previous_rowid = par_rowid
    for sec_n, sec in enumerate(sections):
        # For meta sections, just enter the paragraph row.
        if sec[0] == 'm':
            page_n = sec[1][0][0]
            if page_n <= last_doc_page_n and sec_n not in inserted_meta_idxs:
                insert_meta(sec)
        # For document sections, enter the document row and the paragraph rows linked to it.
        elif sec[0] == 'd':
            doc_title = struct_find.select_title([i[1] for i in sec[1]])
            source_date = struct_find.guess_date(doc_title, [i[1] for i in sec[1]])
            sqlite.execute(f'INSERT OR {dupl_action}'
                    ' INTO document (edition, title, source_date, source_entity, source_place,'
                    ' source_region)'
                    ' VALUES (?, ?, ?, ?, ?, ?)',
                    (publ_rowid, doc_title, source_date.isoformat() if source_date else '',
                        metadata.source_entity, metadata.source_place, metadata.source_region))
            doc_rowid = sqlite.execute('SELECT last_insert_rowid()').fetchone()[0]
            for (page_num, paragraph) in sec[1]:
                if page_num > last_doc_page_n:
                    # Look ahead for meta sections that would belong here (before the current page).
                    check_sec_n = sec_n + 1
                    while (check_sec_n < len(sections) and sections[check_sec_n][0] != 'd'
                           and sections[check_sec_n][1][0][0] <= last_doc_page_n):
                        if check_sec_n not in inserted_meta_idxs:
                            insert_meta(sections[check_sec_n])
                            inserted_meta_idxs.add(check_sec_n)
                        check_sec_n += 1
                sqlite.execute(f'INSERT OR {dupl_action}'
                        ' INTO paragraph (edition, doc, pagenum, text)'
                        ' VALUES (?, ?, ?, ?)',
                        (publ_rowid, doc_rowid, page_num, paragraph))
                par_rowid = sqlite.execute('SELECT last_insert_rowid()').fetchone()[0]
                if previous_rowid is not None:
                    sqlite.execute('UPDATE paragraph SET nextpar = ? WHERE rowid = ?',
                            (par_rowid, previous_rowid))
                previous_rowid = par_rowid
                last_doc_page_n = page_num
        else:
            raise RuntimeError(f'unknown section type {sec[0]} when trying to insert to db')
    sqlite.commit()

def _safe_compile(pattern, flags=0):
    return re.compile(re.escape(pattern), flags=flags)

def _find_sections(pages: Iterable, struct_find: StructureFinder, strip_chapter_titles=False,
        book_title=None, merge_short_docs=False, double_newlines=False):
    """
    An implementation function for `read_edition`. Returns a a list of pairs (section type: d[oc]
    or m[eta], list of (pages, paragraphs)).

    Optional args:
    - `strip_chapter_titles` - if True, it will attempt to remove chapter titles that may appear
        at the top of the page in some books.
    - `book_title` - if `strip_chapter_titles`, this is added to the possible titles to strip.
    - `merge_short_docs` - if a number x, documents of <= x paragraphs shorter than 100x chars are
        merged with the subsequent documents (assumed to be multiple different-level chapter names).
    """
    pages_paragraphs = dict() # a dictionary of page_number: paragraph texts
    for page_n, page_text in pages:
        # Also normalize the non-breaking spaces.
        page_text = re.sub(' +', ' ', struct_find.ocr_corrected(page_text).replace('\xa0', ' '))
        if not page_n in pages_paragraphs:
            pages_paragraphs[page_n] = []
        for par in struct_find.extract_paragraphs(page_text, double_newlines=double_newlines):
            pages_paragraphs[page_n].append(par)
    sections = []
    current_document_paragraphs = []
    # We keep the score to use it with beginning paragraph detection.
    previous_heading_score = 0
    possible_heading = False
    possible_heading_page = False
    # These are the pages where the chapter title appears first at the top of the page, and was
    # marked as a heading mistakenly (applicable when `strip_chapter_titles`).
    pages_to_merge = set()
    # Keep track of the chapter headings to possibly strip.
    found_headings = []
    if strip_chapter_titles and book_title is not None:
        found_headings.append(book_title)
    # On each loop run we decide if the previous paragraph was a heading, and check the heading
    # score of the current one to use together with the doc start score of the next one.
    for page_n in sorted(pages_paragraphs.keys()):
        top_page_heading = False
        for local_par_n, paragraph in enumerate(pages_paragraphs[page_n]):
            is_meta = False # depends on detection
            if struct_find.is_meta_fragment(paragraph):
                is_meta = True
                # Meta section are always considered one-paragraph in length.
                sections.append(('m', [(page_n, join_linebreaks(paragraph))]))
            if not is_meta:
                # If it's not meta, handle the case where there might have been a heading previosly.
                # Note that all document paragraphs pass through here
                if possible_heading:
                    if (previous_heading_score
                            + max(0, struct_find.doc_start_score(paragraph))) > 0:
                        if (top_page_heading
                                and top_page_heading.search(possible_heading) is not None):
                            pages_to_merge.add(page_n)
                        # (add the whole previous document)
                        if current_document_paragraphs:
                            sections.append(('d', current_document_paragraphs))
                        current_document_paragraphs = [(possible_heading_page,
                            join_linebreaks(possible_heading))]
                        if strip_chapter_titles:
                            found_headings.append(_safe_compile(possible_heading, flags=re.I))
                    else:
                        # If there's no chance it was a heading, add the previous fragment to the
                        # current document section.
                        current_document_paragraphs.append(
                                (possible_heading_page, join_linebreaks(possible_heading)))
                # If we strip chapter titles, don't allow the headings to repeat at the start of a
                # page.
                disallow_heading = False
                if strip_chapter_titles and local_par_n == 0:
                    for chapter_patt in found_headings[-5:]:
                        if chapter_patt.search(paragraph) is not None:
                            previous_heading_score = -10.0
                            disallow_heading = True
                            break
                    # If we find a new heading at the top of the page.
                    if not disallow_heading and struct_find.heading_score(paragraph) > 0:
                        top_page_heading = _safe_compile(paragraph, flags=re.I)
                # When skipping chapter titles, detected disallowed headings are skipped altogether.
                if not disallow_heading:
                    previous_heading_score = struct_find.heading_score(paragraph)
                    possible_heading = paragraph
                    possible_heading_page = page_n
    # Add the last paragraphs/documents if something remains.
    if previous_heading_score <= 0 and possible_heading: # add it to the last document section
        current_document_paragraphs.append((possible_heading_page,
            join_linebreaks(possible_heading)))
    if current_document_paragraphs:
        sections.append(('d', current_document_paragraphs))
    if previous_heading_score > 0:
        # the reverse of the earlier conditional - a lone heading at the end
        sections.append(('d', [(possible_heading_page, possible_heading)]))
        if strip_chapter_titles:
            found_headings.append(_safe_compile(possible_heading, flags=re.I))
    # Merge the pages where the heading was found prematurely (meaningful when
    #  `strip_chapter_titles` is on).
    actual_sections = []
    last_page = -1
    merge_next = False # for merging short documents, if requested
    for section in sections:
        merged_sec = False
        if not section[0] == 'd':
            continue
        if merge_next:
            for as_n in range(len(actual_sections)-1, 0, -1):
                if actual_sections[as_n][0] == 'd':
                    actual_sections[as_n] = (actual_sections[as_n][0],
                            actual_sections[as_n][1] + section[1])
                    break
            merge_next = False
            merged_sec = True
        if (not merged_sec and actual_sections
                # (can be true only if `strip_chapter_titles`:)
                and section[1][0][0] in pages_to_merge
                and section[1][0][0] > last_page):
            # Merge the section with the previous document section.
            merged_sec = True
            for as_n in range(len(actual_sections)-1, 0, -1):
                if actual_sections[as_n][0] == 'd':
                    actual_sections[as_n] = (actual_sections[as_n][0],
                            # [1:] because of skipping the chapter name
                            actual_sections[as_n][1] + section[1][1:])
                    break
        elif (isinstance(merge_short_docs, int)
                and len(section[1]) <= merge_short_docs
                and sum(len(p[1]) for p in section[1]) <= merge_short_docs * 100):
            merge_next = True
        last_page = section[1][0][0]
        if not merged_sec:
            actual_sections.append(section)
    # Add meta sections after each page.
    sec_shift = 0
    origin_sec_idx = 0
    last_doc_page_n = 0
    for doc_sec_n in range(len(actual_sections)):
        page_n = actual_sections[doc_sec_n+sec_shift][1][-1][0]
        if page_n > last_doc_page_n:
            # It's possible that we go beyond the intended pages for meta sections, in that case we
            # will need to go back when adding them for the next document.
            local_origin_sec_idx = origin_sec_idx
            while local_origin_sec_idx < len(sections) and (sections[local_origin_sec_idx][0] == 'm'
                   or sections[local_origin_sec_idx][1][0][0] < page_n):
                if(sections[local_origin_sec_idx][0] == 'm'
                   and sections[local_origin_sec_idx][1][0][0] <= page_n):
                    actual_sections.insert(doc_sec_n + sec_shift + 1, sections[local_origin_sec_idx])
                    origin_sec_idx = local_origin_sec_idx
                    sec_shift += 1
                local_origin_sec_idx += 1
            last_doc_page_n = page_n
    return actual_sections
