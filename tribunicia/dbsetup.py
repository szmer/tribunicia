import sqlite3

def setup_db(sqlite: sqlite3.Connection):
    "Setup the tables (if necessary) at the `sqlite` connection."
    sqlite.execute('CREATE TABLE IF NOT EXISTS edition ('
            ' title TEXT UNIQUE,'
            ' editor TEXT,'
            ' publisher TEXT, '
            ' place TEXT,'
            # the dates are written as Python ISO timestamps
            ' publ_date TEXT)')
    sqlite.execute('CREATE TABLE IF NOT EXISTS document ('
            ' edition INTEGER,'
            ' title TEXT,'
            ' type TEXT,'
            ' language TEXT,'
            ' source_date TEXT,'
            ' source_entity TEXT,'
            ' source_place TEXT,'
            ' source_region TEXT)')
    sqlite.execute('CREATE TABLE IF NOT EXISTS paragraph ('
            ' nextpar INTEGER,'
            ' edition INTEGER,'
            ' doc INTEGER,'
            ' pagenum INTEGER,'
            ' text TEXT,'
            ' text_interp TEXT,'
            ' text_potential_surnames TEXT,'
            ' text_footnotes TEXT)')
    sqlite.execute('CREATE TABLE IF NOT EXISTS frequency ('
            ' group_name TEXT, '
            ' name TEXT, '
            ' value REAL, '
            ' value_text TEXT)')
