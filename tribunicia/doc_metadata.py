from datetime import datetime, MAXYEAR

class DocMetadata():
    """
    The collection of metadata for a document or a whole edition of documents that can be written
    to the database.
    """
    def __init__(self,
            source_entity='', editor='', publisher='', edition_title='',
            source_place='', publ_place='',
            source_date=datetime(year=MAXYEAR, month=12, day=31),
            publ_date=datetime(year=MAXYEAR, month=12, day=31),
            source_region=''):
        """
        Create the DocMetadata record object with some or all of the possible metadata.

        Args:
        - `source_entity` - the entity that was initially responsible for the document; it may be
            an institution or an author author if they are responsible for the document
            individually. Note an explicit `author` field is not included, but might be added later
            to indicate the actual author for institutional source document.
        - `editor` - the entity responsible for the document edition (see `edition title`).
        - `publisher` - of the document edition.
        - `edition_title` - of the edition where the document was later published for scholarly use.
        - `source_place` - the place where the document was initially sent or made known.
        - `publ_place` - the place of the edition's publication (see `edition title`).
        - `source_date` - the date when the the document was initially written, sent or made known.
            Use 1st of January if no exact date is known.
        - `publ_date` - the date when the the edition of the document was published (see
            `edition title`). Use 1st of January if no exact date is known.
        - `source_region` - the region related to the `source_place`, such as a province or
            a voivodeship.
        """
        # Just set all the arguments as attributes of the object.
        args = locals()
        for arg, val in args.items():
            setattr(self, arg, val)
