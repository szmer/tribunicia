"General, non strictly text utils."

def exhaust_paragraph_rows(paragraph_rows):
    "A generator that will yield paragraph_rows from the database in order."
    par_row = paragraph_rows.fetchone()
    while par_row is not None:
        par_row = paragraph_rows.fetchone()
        yield par_row
