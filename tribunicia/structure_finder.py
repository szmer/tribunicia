"""
A configurable object (we suggest to keep the settings in YAML or a similar format) with methods
intended for recognizing structure in scanned or electronic editions of many historical documents,
such as sejmik assembly resolutions.
"""
from datetime import date
import logging
import re

import roman

def logged_get(settings_dict, key, default, clean=False):
    """
    Like dict get method, but with the key/value pair logged. If `clear_patterns` is 'str', clean
    newlines as from a string if we do get a value from the dict, if 'list`, then as from a list.
    """
    if key in settings_dict:
        logging.info(f'Setting the structure parameter {key} to {settings_dict[key]}')
        if clean == 'str':
            return settings_dict[key].replace('\n', '')
        if clean == 'list':
            return [elem.replace('\n', '') for elem in settings_dict[key]]
        if clean:
            raise ValueError(f'Unexpected value of clean: {clean}')
        return settings_dict[key]
    return default

class StructureFinder:
    # TODO document the settings.
    def __init__(self, settings={}):
        self.months = logged_get(settings, 'months_pattern', '', clean='str')
        # Lists of patterns.
        self.month_names_to_numbers = logged_get(settings, 'month_names_to_numbers', [])
        self.heading_signs = logged_get(settings, 'heading_signs', [], clean='list')
        self.heading_signs2 = logged_get(settings, 'heading_signs2', [], clean='list')
        self.doc_start_signs = logged_get(settings, 'doc_start_signs', [], clean='list')
        self.doc_start_signs2 = logged_get(settings, 'doc_start_signs2', [], clean='list')
        self.meta_signs = logged_get(settings, 'meta_signs', [], clean='list')
        self.heading_antisigns = logged_get(settings, 'heading_antisigns', [], clean='list')
        self.meta_antisigns = logged_get(settings, 'meta_antisigns', [], clean='list')
        self.hard_doc_strings = logged_get(settings, 'hard_doc_strings', [], clean='list')

        # Numerical values.
        self.min_year = logged_get(settings, 'min_year', 1500)
        self.max_year = logged_get(settings, 'max_year', 1795)
        self.max_doc_line_len = logged_get(settings, 'max_doc_line_len', False)
        self.max_heading_line_len = logged_get(settings, 'max_heading_line_len', 100)
        self.min_intrapar_len = logged_get(settings, 'min_intrapar_len', 60)
        # (The length_discount setting controls per how many characters in the fragment one full
        # point is subtracted from the heading score)
        self.heading_score_length_discount = logged_get(settings,
                'heading_score_length_discount', 70)
        # (Thr bonus to the doc start bonus for any secondary sign occurring)
        self.doc_start_sign2_bonus = logged_get(settings, 'doc_start_sign2_bonus',
                0.8 / len(self.doc_start_signs2) if self.doc_start_signs2 else 0)
        # This subtracts from the heading score if no date is extractable from the potential
        # heading (a feature not present in populobot). Set to 0 to turn off.
        self.nochron_penalty = logged_get(settings, 'nochron_penalty', -2.0)

        # Flags.
        self.uppercase_are_meta = logged_get(settings, 'uppercase_are_meta', True)
        # (compatibility with the buggy old populobot code)
        self.bugcompat = logged_get(settings, 'bugcompat', False)

        # Other
        self.ocr_corrections = logged_get(settings, 'ocr_corrections', dict())

    def doc_start_score(self, paragraph: str):
        """
        Return a score indicating whether the `paragraph` (a string) could be a beginning of a
        document. More than 0.0 can be considered a positive response.

        To get that, it requires some `doc_start_signs` (or word capitalization) to appear along
        with some `doc_start_signs2`.
        """
        signs_count = -0.5
        signs1 = [re.search(s, paragraph) for s in self.doc_start_signs]
        if self.bugcompat:
            if any(signs1) or (len(paragraph) > 0 and paragraph[0].lower() != paragraph[0]):
                signs_count += 0.3
                for sign in self.doc_start_signs2:
                    # Note this is True even on nothing found (as -1 is truthy), so ironically the
                    # bonus will be < 0.3 only when some sign is found at the start.
                    if paragraph.lower().find(sign):
                        signs_count += 0.3 / len(self.doc_start_signs2)
        else:
            capitalization = [ch for ch in paragraph if ch.isupper()]
            if any(signs1) or len(capitalization) > 2:
                signs_count += 0.3
                for sign in self.doc_start_signs2:
                    if re.search(sign, paragraph) is not None:
                        signs_count += self.doc_start_sign2_bonus
        return signs_count

    def extract_dates(self, string: str):
        "Find dates in `string` and return them as a list of `date` objects."
        dates = []
        month_names = re.compile(self.months) # from global
        month_romandigs = re.compile('[xXvViI]{1,3}', flags=re.I)
        for month_find in (list(month_names.finditer(string.lower()))
                + list(month_romandigs.finditer(string))):
            month_number = False
            year_number = False
            day_number = False
            # try to extract the month
            found_month = month_find.group(0)
            logging.debug('{} - month candidate'.format(found_month))
            # For Roman digits, parse them, for the rest take then number from MONTHS.
            if not month_romandigs.match(found_month):
                for (cue, number) in self.month_names_to_numbers:
                    if cue.lower() in found_month.lower():
                        month_number = number
                        break
            else:
                try:
                    month_number = roman.fromRoman(found_month)
                except roman.InvalidRomanNumeralError:
                    pass
            if not month_number:
                continue
            logging.debug('{} - month number'.format(month_number))
            next_space_ind = string[month_find.end():].find(' ')
            next_blank_ind = string[month_find.end():].find('\n')
            if next_space_ind == -1 or (next_blank_ind >= 0 and next_blank_ind < next_space_ind):
                next_space_ind = next_blank_ind
            next_space_ind = month_find.end() + next_space_ind # align it in the whole str context
            prev_space_ind = string[:month_find.start()].rfind(' ')
            prev_blank_ind = string[:month_find.end()].rfind('\n')
            if prev_space_ind == -1 or (prev_blank_ind >= 0 and prev_blank_ind > prev_space_ind):
                prev_space_ind = prev_blank_ind
            if next_space_ind == -1:
                logging.debug('end of the string, aborted')
                continue
            reversed_order = False # year-month-day or month-day-year
            # Try to extract the next number. This may be the year or the day.
            expected_num_str = string[next_space_ind+1:next_space_ind+5]
            logging.debug('{} - expected number (after month)'.format(expected_num_str))
            try:
                first_num_str = re.search('\\d{1,4}', expected_num_str).group(0)
            except AttributeError: # if search() produces None
                continue
            if len(first_num_str) == 3:
                continue
            first_num = int(first_num_str)
            if len(first_num_str) == 4:
                year_number = first_num
                if year_number < self.min_year or year_number > self.max_year:
                    logging.debug('Rejected the year number {}'.format(year_number))
                    year_number = False
            else: # we've eliminated the 3-letter case earlier
                day_number = first_num
                if (day_number > 31
                        or (month_number == 2 and day_number > 29)
                        or (month_number in [4,6,9,11] and day_number > 30)):
                    logging.debug('{} day number rejected for month {}'.format(day_number,
                        month_number))
                    day_number = False
                else:
                    logging.debug('{} - day number'.format(day_number))
                    reversed_order = True
                    try:
                        # If this suceeds, we have a month-day-year date.
                        expected_num2_str = re.search('\\d{4}',
                                string[next_space_ind+1+len(first_num_str)+1
                                    # give allowance for "anno domini" etc.
                                    :next_space_ind+1+len(first_num_str)+20]).group(0)
                        year_number = int(expected_num2_str)
                    except AttributeError:
                        pass
                    if year_number and year_number < self.min_year or year_number > self.max_year:
                        logging.debug('Rejected the year number {}'.format(year_number))
                        year_number = False
                        day_number = False
                    elif year_number:
                        logging.debug('{} - year number'.format(year_number))
                        try:
                            dates.append(date(int(year_number), int(month_number), int(day_number)))
                        except ValueError:
                            logging.debug('Invalid date {}-{}-{}'.format(day_number,
                                month_number,
                                year_number))
                            continue
                        continue
            # If we don't have a date by now, we would need some chars before the month.
            if prev_space_ind == -1:
                logging.debug('beginning of the string, aborted')
                continue
            # Try the alternative order, as in '1670 Januarius, 22'.
            if reversed_order:
                expected_year_str = string[prev_space_ind-4:prev_space_ind]
                logging.debug('{} - expected year string (before month)'.format(expected_year_str))
                if re.match('^\\d+$', expected_year_str):
                    year_number = int(expected_year_str)
                    if year_number < self.min_year or year_number > self.max_year:
                        logging.debug('Rejected the year number {}'.format(year_number))
                        year_number = False
                    else:
                        reversed_order = True
                if not year_number:
                    continue
                logging.debug('{} - year number'.format(year_number))
            # Try to extract the day for the normal order day-month-year
            else:
                expected_day_str = string[max(prev_space_ind-3, 0):prev_space_ind]
                logging.debug('{} - expected day string'.format(expected_day_str))
                try:
                    day_number = int(re.search('\\d+', expected_day_str).group(0))
                except AttributeError:
                    continue
                if (day_number > 31
                        or (month_number == 2 and day_number > 29)
                        or (month_number in [4,6,9,11] and day_number > 30)):
                    logging.debug('{} day number rejected for month {}'.format(day_number,
                        month_number))
                    continue
                logging.debug('{} - day number'.format(day_number))
            if day_number and month_number and year_number:
                logging.debug('Date found: {} {} {}'.format(day_number,
                    month_number,
                    year_number))
                try:
                    dates.append(date(int(year_number), int(month_number), int(day_number)))
                except ValueError:
                    logging.debug('Invalid date {}-{}-{}'.format(day_number,
                        month_number,
                        year_number))
                    continue
        return dates

    def extract_paragraphs(self, page_text: str, double_newlines=False):
        """
        Extract a list of paragraphs as strings from the `page_text`.
        """
        paragraphs = []
        current_par_lines = []
        for line in re.split('\n+' if double_newlines else '\n', page_text):
            if line == '': # end of the current paragraph
                if current_par_lines:
                    paragraphs.append('\n'.join([p.strip() for p in current_par_lines]))
                    current_par_lines = []
            # the paragraph ends on this line:
            elif len(line) < self.min_intrapar_len:
                current_par_lines.append(line)
                paragraphs.append('\n'.join([p.strip() for p in current_par_lines]))
                current_par_lines = []
            else:
                current_par_lines.append(line)
        if current_par_lines: # the possible leftover paragraph
            paragraphs.append('\n'.join([p.strip() for p in current_par_lines]))
        return [par for par in paragraphs if len(par) > 0]

    def guess_date(self, title: str, paragraphs: list):
        """
        Given the `title` and document `paragraphs`, try to guess the date on which the document was
        created. Return the `date` object for the date that was chosen or False, if none was.
        """
        # First, try to return the earliest (full) date from the title.
        title_dates = sorted(self.extract_dates(title))
        if len(title_dates) > 0:
            return title_dates[0]
        # If title yields nothing, try the content - the first date that appears in the document.
        content_dates = sorted(self.extract_dates(' '.join(paragraphs)))
        if len(content_dates) > 0:
            return content_dates[0]
        return False

    def heading_score(self, paragraph: str):
        """
        Compute a score estimating how likely the `paragraph` is to be a heading. Generally
        fragments with heading score above 0 can be considered headings.
        """
        if len(paragraph) < 15 or len(paragraph) > self.max_heading_line_len:
            return -11
        paragraph = paragraph.replace('-', '') # some potential cleanup
        signs_1ord = [re.search(s, paragraph) for s in self.heading_signs]
        signs_1ord_count = len([s for s in signs_1ord if s])
        logging.debug('{}...: +{:.1f} from first-order signs'.format(paragraph[:30],
            signs_1ord_count))
        signs_2ord = [re.search(s, paragraph) for s in self.heading_signs2]
        signs_2ord_count = len([s for s in signs_2ord if s])
        logging.debug('{}...: +{:.1f} from second-order signs'.format(paragraph[:30],
            signs_2ord_count))
        if signs_1ord_count == 0:
            signs_2ord_count -= 1.5
            logging.debug(f'(hdng) {paragraph[:30]}...: -1.5 from no first-order signs')
        elif not len([re.search(s, paragraph) for s in self.heading_signs[:25]]):
            signs_2ord_count -= 1.0
            logging.debug(f'(hdng) {paragraph[:30]}...: -1.0 from no first-order signs in the first'
                    ' 25 characters')
        signs_count = signs_1ord_count + signs_2ord_count
        antisigns = [re.search(s, paragraph) for s in self.heading_antisigns]
        antisigns_count = len([s for s in antisigns if s]) * 0.6
        logging.debug('{}...: -{:.1f} from anti-signs {}'.format(paragraph[:30],
            antisigns_count,
            [s.group(0) for s in antisigns if s is not None]))
        # If the first letter is not uppercase, it's a strong signal against.
        try:
            first_letter = re.search('[^\\W\\d_]', paragraph).group(0)
            if first_letter.lower() == first_letter:
                logging.debug(f'(hdng) {paragraph[:30]}...: -1.0 from lowercase first letter')
                antisigns_count += 1
        # Penalize also no-letter paragraphs if such are found.
        except AttributeError:
            antisigns_count += 1
        signs_count -= antisigns_count
        logging.debug(f'(hdng) {signs_count} signs for {paragraph[:30]}...')
        # To be positive, the signs count must be more than a factor dependent on paragraph length
        logging.debug('{}...: -{:.1f} from paragraph length'.format(paragraph[:30],
            (len(paragraph) / self.heading_score_length_discount)))
        # Possibly add the non-date penalty.
        nochron_penalty = 0.0
        if int(self.nochron_penalty) != 0:
            if not self.extract_dates(paragraph):
                nochron_penalty = self.nochron_penalty
                logging.debug(f'(hdng) Applied nochron penalty {nochron_penalty}'
                        f' for {paragraph[:30]}...')
        return signs_count - (len(paragraph) / self.heading_score_length_discount) + nochron_penalty

    def is_meta_fragment(self, fragment: str):
        """
        Get a boolean with a guess whether the `fragment` is a meta fragment (a footnote, an
        editor's comment etc.).
        """
        for string in self.hard_doc_strings:
            if string in fragment:
                logging.debug(f'(meta) Hard doc string {string} found in {fragment}')
                return False
        if len(fragment) < 9:
            logging.debug(f'(meta) Too few characters in {fragment}')
            return True
        if self.max_doc_line_len:
            for line in fragment.split('\n'):
                if len(line) > self.max_doc_line_len:
                    logging.debug(f'(meta) There is a line that is too long: {line}'
                            f' in fragment {fragment[:30]}...')
                    return True
        if len(fragment) < 800:
            for sign in self.meta_signs:
                if re.search(sign, fragment):
                    logging.debug(f'(meta) Found meta sign {sign} in {fragment[:30]}...')
                    return True
        # If a large part of the fragment of non-alphabetic (re.sub removes alphabs for the check)
        if len(fragment) > 0 and len(re.sub(r'[^\W0-9]', '', fragment)) / len(fragment) >= 0.65:
            logging.debug(f'(meta) Too much non-alphabetic in {fragment[:30]}...')
            return True
        # If almost a majority of the fragment's tokens are very short (happens in footnotes)
        tokens = [t for t in re.split(r'\s+', fragment) if len(t)]
        if len(tokens) >= 4 and len([t for t in tokens if len(t) <= 2]) > 0.48 * len(tokens):
            logging.debug(f'(meta) Very short tokens in {fragment[:30]}...')
            return True
        # If there are fully uppercase words, other than Roman numerals.
        if len(tokens) < 10 and self.uppercase_are_meta:
            for t in tokens:
                if      (
                        len(t) > 3 and re.sub('[IVXCLM]', '', t) != ''
                        and t == t.upper() and t != t.lower()
                        ):
                    logging.debug(f'(meta) Fully capitalized {t} in {fragment[:30]}...')
                    return True
        # If the majority of words are capitalized or numbers, or non-alphanumeric (provided it's
        # not a quotation of titles).
        titles_presence = any(
                re.search(s, fragment)
                # TODO - move these to the heading signs?
                for s in self.heading_signs + ['[kK]ról [pP]ols', '^We? ']
            )
        if not titles_presence and len(tokens) >= 3:
            capit_or_num_count = (
                len([t for t in tokens if (t[0] != t[0].lower()) or (re.search(r'[\W0-9]', t))])
            )
            if capit_or_num_count > 0.85 * len(tokens):
                logging.debug(f'(meta) Almost all capitalized or numbers in {fragment[:30]}...')
                return True
        # If there are many footnote point-like places.
        # (look also for footnotes looking like this: "a )" - it's the second sub-pattern before the
        # ending paren)
        if len(fragment) < 380 and len(list(re.findall(r'(((^| ).)|(. ))\)', fragment))) >= 2:
            logging.debug(f'(meta) Too many footnote point-like places in {fragment[:30]}...')
            return True
        # If there is very few kinds of characters used.
        if len(fragment) in range(2, 17) and len(set(fragment.lower())) <= max(2, len(fragment) / 3):
            logging.debug(f'(meta) Few character types in {fragment[:30]}...')
            return True
        # If large percentage of tokens is abbreviated.
        if len(re.findall(r'\b[A-Za-z]{1,5}\.', fragment)) > (len(fragment.split(' ')) * 0.33):
            logging.debug(f'(meta) Too many abbreviations in {fragment[:30]}...')
            return True
        return False

    def ocr_corrected(self, paragraph: str):
        """
        Return the defined OCR corrections to the `paragraph` and return it.
        """
        for patt, corr in self.ocr_corrections.items():
            paragraph = re.sub(patt, corr, paragraph)
        return paragraph

    def select_title(self, paragraphs: list):
        """
        Select what should be title among the `paragraphs`. This may be an empty string for an empty
        list or if nothing looks suitable.
        """
        if len(paragraphs) == 0:
            return ''
        max_heading_score = 0
        max_heading = ''
        for paragraph in paragraphs[:3]:
            score = self.heading_score(paragraph )
            if score > max_heading_score:
                max_heading_score = score
                max_heading = paragraph 
        return max_heading
