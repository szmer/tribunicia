"""Different transformations for adding information about personal names in document texts."""
from collections import defaultdict
from copy import copy
from itertools import chain
import json
import logging
import re
from typing import Tuple

from tribunicia.lib_morphoparsing import disamb_position, position_offset, position_char_len
from tribunicia.text_utils import find_constrained_subsequences, get_constraint_pattern

MONTH_PATTERN = '(' + '|'.join([
    'stycz', 'jan', 'ian', 'lut', 'feb', 'mar', 'kwiet', 'apr', 'maj', 'mai', 'czerw', 'jun', 'iun',
    'lip', 'jul', 'iul', 'sierp', 'aug', 'wrze', 'sept', 'paźd', 'oct', 'list', 'nov', 'grud',
    'dece'
    ]) + ')'

BASE_PERSON_LEXEMES = {'pan': ['pan', 'pp'],
                'jegomość pan': ['jmp'],
                'mościwy': ['mciwych', 'M', 'mci'],
                'urodzony': ['urodzony', 'urodzić', 'Ura'],
                'jegomość': ['jm', 'JM', 'JMci', 'Jm'],
                'ichm': ['ichm',  'Ichm','ichmciami', 'ichmci', 'ichmciów', 'ichmciom'],
                'wielmożny': ['wielmożny', 'w'],
                'jaśnie wielmożny': ['jw'],
                }
AFFIX_PERSON_LEXEMES = {
                'JKM': ['JKMć', 'J', 'j', 'K', 'k'],
                'nasz': ['nasz', 'n'],
                'brat': ['brat', 'bracia'],
                'obywatel': ['obywatel'],
                'conj': ['i', 'z', 'ze', 'na', ',', r'\.'],
                'jednostka': ['powiat', 'województwo', 'ziemia'],
                'odmiejscowe': ['[^@]+[cs]ki[^@]{0,4}'],
                'nazwy_własne': ['[A-ZŻŹĆŁÓŚ][^@]{2,}']
               }

def reverse_index(idx):
    "Reverse the original index from key -> [vals] to val -> [keys]."
    rev_idx = defaultdict(lambda: [])
    for key, val_list in idx.items():
        for val in val_list:
            if val not in rev_idx:
                rev_idx[val] = []
            rev_idx[val].append(key)
    return rev_idx

def person_phrase_pattern(additional_patterns):
    "Get a big pattern that can be passed to find_constrained_subsequences from utils."
    return get_constraint_pattern(
            set(list(chain.from_iterable(BASE_PERSON_LEXEMES.values()))
                + list(chain.from_iterable(AFFIX_PERSON_LEXEMES.values()))
                + list(additional_patterns)))

def person_subranges(terms, rg, first_names=[]):
    """
    Extract the list of subsequent subranges related to individual persons from the terms. The
    terms are addumed to correspond to rg, i.e. range (x, y) (in some original sequence). Ranges are
    inclusive both ways.
    """
    candidate_subrgs = [[]]
    for t in terms:
        if candidate_subrgs[-1] and t in [',', 'i']:
            candidate_subrgs.append([])
        candidate_subrgs[-1].append(t)
    term_subrgs = [candidate_subrgs[0]]
    for candidate in candidate_subrgs[1:]:
        # Separate the individual persons on having these terms.
        if ('pan' in candidate) or any((e in candidate) for e in first_names):
            term_subrgs.append(candidate)
        else:
            term_subrgs[-1] += candidate
    idx_subrgs = [(rg[0], rg[0]+len(term_subrgs[0])-1)]
    for subrg in term_subrgs[1:]:
        idx_subrgs.append((idx_subrgs[-1][1]+1, idx_subrgs[-1][1]+len(subrg)))
    return idx_subrgs

# pylint: disable=unused-argument
def person_occurrence_func(names_pattern, rev_person_lexemes, additional_base_lexemes,
                           edition_title):
    """
    A function for use with collect_freqtables and a table that will be filled with PersonOccurence
    objects when the function is ran.

    Arguments:
        - names_pattern - the pattern that can be used with find_constrained_subsequences to find
              all name phrases.
        - rev_person_lexemes - a mapping of person terms to their labels (base names and
              observables) that are reported. Can be obtained by performing reverse_index on a dict
              like AFFIX_PERSON_LEXEMES.
        - additional_base_lexemes - more lexemes (besides BASE_PERSON_LEXEMES) that can constitute
              a person mention if they appear.
    """
    occurrences_table = []
    all_base_lexemes = list(BASE_PERSON_LEXEMES.keys()) + additional_base_lexemes
    def ret_fun(sent_forms, sent_terms, sent_interps, sent_offsets, year, pagen, counter):
        phrase_ranges = find_constrained_subsequences(sent_terms, names_pattern)
        used_ranges = []
        for rg in phrase_ranges:
            terms_for_check = [sent_terms[ti] for ti in range(rg[0], rg[1]+1)]
            subranges = person_subranges(terms_for_check, rg)
            local_phrase_objs = []
            for (sub_n, subrange) in enumerate(subranges):
                phrase_terms = [sent_terms[ti] for ti in range(subrange[0], subrange[1]+1)]
                phrase_forms = [sent_forms[ti] for ti in range(subrange[0], subrange[1]+1)]
                phrase_interps = [sent_interps[ti] for ti in range(subrange[0], subrange[1]+1)]
                phrase_offset = sent_offsets[subrange[0]]
                # Require that "pan" or "urodzony" etc. is part of the phrase to consider.
                if not any((base in phrase_terms) for base in all_base_lexemes) or (
                        len(phrase_terms) == 1 and phrase_terms[0] == 'pan'):
                    continue
                used_ranges.append(subrange)
                phrase_obj = PersonOccurence(
                        subrange, phrase_terms, phrase_forms, phrase_interps,
                        list(map(lambda t: rev_person_lexemes[t], phrase_terms)),
                        year, edition_title, phrase_offset, pagen,
                        previous=local_phrase_objs[sub_n-1]
                        if sub_n > 0 and len(local_phrase_objs) == sub_n else False
                        )
                local_phrase_objs.append(phrase_obj)
                occurrences_table.append(phrase_obj)
                counter.update([' + '.join([str(a) for a in phrase_obj.observables])])
    return ret_fun, occurrences_table

class PersonOccurence():
    "Store info about a detected phrase that is about a person."

    def __init__(self, token_range, terms, forms, interps, observables, year, edition,
                 offset_in_par, pagen, previous=False, time_periods=[]):
        """
        Terms are lexemes/bases, observables (as a list for each term) point to roots or features to
        analyze.

        Optionally, another person occurrence immediately preceding this one can be provided to keep
        track of.
        """
        self.token_range = token_range
        self.terms = terms
        self.forms = forms
        self.interps = interps
        self.observables = observables
        self.year = int(year)
        self.edition = edition
        self.offset_in_par = offset_in_par
        self.pagen = pagen
        self.previous = previous
        for period in time_periods:
            if period[0] <= year <= period[1]:
                self.period = period
                break
        else:
            self.period = None

    def pperiod(self):
        "Pretty-printed time period."
        if self.period is None:
            return ''
        return f'{self.period[0]}-{self.period[1]}'

    def preceding_chain(self):
        "The chain of person mentions immediately preceding this one."
        pchain = []
        prev = self.previous
        while prev:
            pchain.insert(0, prev)
            prev = prev.previous
        return pchain

#
# Footnote finding.
#
def clean_marker(marker):
    "Remove extranous bits from the marker to make it easier to compare."
    return re.sub('\\w+', '', marker).strip()

def find_references_in_text(text):
    "Extract potential references to footnotes from the text as (chars, offset)."
    candidates = [(m.group(), m.start()) for m in re.finditer(
            '\\s(\\w*[\\*$#^%&£ÀÉá€)(\\+=»0-9\':;<>\\|]{1,2})|[!?]\\s',
            text)]
    cand_ns_to_remove = []
    for cand_n, candidate in enumerate(candidates):
        if re.match(f'^\\s\\d+\\s{MONTH_PATTERN}', text[candidate[1]:]) is not None:
            cand_ns_to_remove.append(cand_n)
    candidates = [cand for (cand_n, cand) in enumerate(candidates)
                  if cand_n not in cand_ns_to_remove]
    return candidates

def find_notes_in_foot_text(text):
    "Extract potential footnote markers from the text as (chars, offset)."
    note_pattern = '[\\*$#^%&£ÀÉá€)(\\+=»0-9\':;<>\\|!?]{1,2}\\s'
    found = list(re.finditer(f'^{note_pattern}', text)) + list(
            re.finditer(f'\\s{note_pattern}', text))
    return [(m.group(), m.start()) for m in sorted(found, key=lambda m: m.start())]

def extract_main_and_foot_from_page(par_objs):
    "Extract the main and foot(notes) part from the paragraph objs of the page."
    par_objs = sorted(par_objs, key=lambda o: o['rowid'])
    main, foot = [], []
    last_obj_n = 0
    for obj_n, obj in enumerate(reversed(par_objs)):
        last_obj_n = obj_n
        if obj['doc'] is None or len(obj['text']) < 70:
            foot.insert(0, obj)
        else:
            break
    main = par_objs[:len(par_objs)-last_obj_n]
    return main, foot

def extract_refs_from_pars(par_objs):
    "Get (rowid, string, string offset) tuples for references to footnotes in paragraphs."
    refs = []
    for par in par_objs:
        for (occur, offset) in find_references_in_text(par['text']):
            refs.append((par['rowid'], occur, offset))
    return refs

def extract_notes_from_pars (par_objs):
    "Get (rowid, string, string offset) tuples for footnote marks in paragraphs."
    notes = []
    for par in par_objs:
        for (occur, offset) in find_notes_in_foot_text(par['text']):
            notes.append((par['rowid'], occur, offset))
    return notes

def process_page_with_footnotes(par_objs):
    """Extract notes for the set of paragraphs representing a page. The notes are returned as dicts
    with par_row_id, offset, note, confidence_score keys."""
    main, foot = extract_main_and_foot_from_page(par_objs)
    # Extract the references from main and notes from the foot.
    refs = extract_refs_from_pars(main)
    notes = extract_notes_from_pars(foot)
    # Extract note fragments for the note markers before matching them to references.
    note_idx = 0
    note_fragments = []
    par_mapping = {par_obj['rowid']: par_obj for par_obj in par_objs}
    while note_idx < len(notes):
        note = notes[note_idx]
        note_fragments.append(
                par_mapping[note[0]]['text'][note[2]:]
                if (note_idx + 1 == len(notes) or notes[note_idx+1][0] != note[0])
                # get the whole rest of the paragraph unless there's a next note here
                else par_mapping[note[0]]['text'][note[2]:notes[note_idx+1][2]]
                )
        note_idx += 1
    # Pair the references with the notes.
    ref_idx, note_idx = 0, 0
    note_dicts = []
    while ref_idx < len(refs):
        if note_idx >= len(notes):
            break
        main_ref_marker = clean_marker(refs[ref_idx][1])
        main_note_marker = clean_marker(notes[note_idx][1])
        # The ideal case of position + symbol match.
        if main_ref_marker == clean_marker(refs[note_idx][1]):
            note_dicts.append({
                'par_row_id': refs[ref_idx][0],
                'offset': refs[ref_idx][2],
                'ref': refs[ref_idx][1], # useful for debugging
                'note': note_fragments[note_idx],
                'confidence_score': 1.0
                })
            note_idx += 1
        else:
            better_found = False
            # Check if the subsequent reference could be matched to the current footnote.
            check_ref_idx = ref_idx
            while check_ref_idx < min(len(refs), ref_idx + 3):
                ref_marker = clean_marker(refs[check_ref_idx][1])
                if ref_marker == main_note_marker:
                    note_dicts.append({
                        'par_row_id': refs[ref_idx][0],
                        'offset': refs[ref_idx][2],
                        'ref': refs[ref_idx][1],
                        'note': note_fragments[note_idx],
                        # 0.6 from symbol agreement plus decreased proximity value
                        'confidence_score': 0.6 + (0.4 - 0.1 * (check_ref_idx - ref_idx))
                        })
                    note_idx += 1
                    better_found = True
            # Check if the subsequent footnotes could be matched to the current reference.
            check_note_idx = note_idx
            while not better_found and check_note_idx < min(len(notes), note_idx + 3):
                check_marker = clean_marker(refs[check_note_idx][1])
                if main_ref_marker == check_marker:
                    note_dicts.append({
                        'par_row_id': refs[ref_idx][0],
                        'offset': refs[ref_idx][2],
                        'ref': refs[ref_idx][1],
                        'note': note_fragments[note_idx],
                        # 0.6 from symbol agreement plus decreased proximity value
                        'confidence_score': 0.6 + (0.4 - 0.1 * (check_note_idx - note_idx))
                        })
                    note_idx = check_note_idx + 1
                    better_found = True
            if not better_found:
                # Accept the position-only matched if there's no other near option.
                note_dicts.append({
                    'par_row_id': refs[ref_idx][0],
                    'offset': refs[ref_idx][2],
                    'ref': refs[ref_idx][1],
                    'note': note_fragments[note_idx],
                    # only the max proximity value as the confidence
                    'confidence_score': 0.4
                    })
                note_idx += 1
        ref_idx += 1
    return note_dicts

def add_footnotes_rules(sqlite, edition):
    "Use rules to add the text_footnotes JSON column."
    edition_num_row = sqlite.execute('SELECT rowid FROM edition WHERE title = ?',
            (edition,)).fetchone()
    if edition_num_row is None:
        raise RuntimeError(f'cannot find edition "{edition}" in the database')

    current_page_n = 0
    current_page_pars = []

    paragraph_rows = sqlite.execute('SELECT rowid, pagenum, text, doc FROM paragraph'
                                    ' WHERE edition = ? ORDER BY pagenum',
                                    (edition_num_row[0],))
    par_row = paragraph_rows.fetchone()
    while par_row is not None:
        page_n = par_row[1]
        if page_n != current_page_n:
            if current_page_pars:
                ner_info = process_page_with_footnotes(current_page_pars)
                sqlite.execute('UPDATE paragraph SET text_footnotes = ? WHERE rowid = ?',
                    (json.dumps(ner_info), current_page_pars[0]['rowid']))
            current_page_pars = []
            current_page_n = page_n

        current_page_pars.append({
            'rowid': par_row[0],
            'text': par_row[2],
            'doc': par_row[3]
            })
        par_row = paragraph_rows.fetchone()
    sqlite.commit()

#
# LLM NER based annotation.
#
def surname_entity(ent, text) -> Tuple[str, int]:
    """Given an entity dict containing chunks of the phrase/entity, extract the last part which
    could contain the surname and return. The text arg should contain the whole text indexed by
    start, end entries in the dict. The second return value is the character offset of the part
    returned."""
    chunks = ent['chunks']
    prev_start = None
    # Find the beginning of the last word forming the entity.
    for chunk in reversed(chunks):
        if prev_start is None or prev_start == chunk['end']:
            prev_start = chunk['start']
            continue
        break
    space = text[prev_start:].find(' ')
    if space != -1:
        return text[prev_start:prev_start+space], prev_start
    return text[prev_start:], prev_start

def add_potential_surname_entities_ann(sqlite, edition):
    "Use transformers to add the text_potential_surnames JSON column."
    try:
        from transformers import pipeline
    except Exception:
        logging.error(' !NOTE! You may need to install the optional dependencies, see the readme.')
        raise

    pipe = pipeline("token-classification", model="Babelscape/wikineural-multilingual-ner")

    edition_num_row = sqlite.execute('SELECT rowid FROM edition WHERE title = ?',
            (edition,)).fetchone()
    if edition_num_row is None:
        raise RuntimeError(f'cannot find edition "{edition}" in the database')
    paragraph_rows = sqlite.execute('SELECT rowid, text, text_interp FROM paragraph'
                                    ' WHERE edition = ?',
                                    (edition_num_row[0],))
    par_row = paragraph_rows.fetchone()
    while par_row is not None:
        par_txt = par_row[1]
        par_interp = False
        if par_row[2]:
            par_interp = json.loads(par_row[2])
        if len(par_txt.strip()) == 0:
            continue
        result = pipe(par_txt)
        # Here, store dicts containing data (word, kind, start, end) about entities that are found.
        relevant_entities = []
        # (store pointers to find aligned interps more efficiently)
        for ent in result:
            if ent['entity'].endswith('MISC') or ent['entity'].endswith('ORG'):
                continue
            if ent['entity'].startswith('B-') or not relevant_entities:
                ent = copy(ent)
                ent['score'] = float(ent['score']) # replace non-serializable numpy float32 type
                ent['kind'] = ent['entity'][2:]
                ent['chunks'] = [{'chunk': ent['word'],
                                  'start': ent['start'],
                                  'end': ent['end']}]
                del ent['index'], ent['start'], ent['end'], ent['entity']
                relevant_entities.append(ent)
            else:
                relevant_entities[-1]['chunks'].append({'chunk': ent['word'],
                                                        'start': ent['start'],
                                                        'end': ent['end']})
        # Extract the "words" - suspected of being actual surnames, their offsets and try to include
        # the corresponding morpho_interp.
        morpho_sent_idx, morpho_pos_idx = 0, 0
        presentence_offset = 0 # since offset in morpho is counted inside the sentence
        offset_update_idx = -1 # track which sentences updated the above
        for ent in relevant_entities:
            ent['word'], ent['word_offset'] = surname_entity(ent, par_txt)
            new_presentence_offset = presentence_offset # do not update globally if fails to match
            if par_interp:
                break_outer = False
                first_sent = True # only then the morpho_pos_idx is relevant
                for sidx, sent in enumerate(par_interp[morpho_sent_idx:]):
                    for pidx, position in enumerate(sent[morpho_pos_idx if first_sent else 0:]):
                        offset = position_offset(position)
                        if offset + new_presentence_offset >= ent['word_offset']:
                            if offset + new_presentence_offset== ent['word_offset']:
                                form, term, interp = disamb_position(position)
                                ent['morpho_interp'] = {
                                        'form': form.strip(),
                                        'base': term.strip(),
                                        'interp': interp
                                        }
                                presentence_offset = new_presentence_offset
                                morpho_sent_idx, morpho_pos_idx = (morpho_sent_idx + sidx,
                                                                   (pidx + morpho_pos_idx
                                                                   if first_sent else pidx))
                            break_outer = True
                            break
                        if (
                                pidx == len(sent[morpho_pos_idx if first_sent else 0:]) - 1
                                and morpho_sent_idx + sidx > offset_update_idx
                            ):
                            new_presentence_offset += offset + position_char_len(position)
                            offset_update_idx = morpho_sent_idx + sidx
                    if break_outer:
                        break
                    first_sent = False
        sqlite.execute('UPDATE paragraph SET text_potential_surnames = ? WHERE rowid = ?',
                (json.dumps(relevant_entities), par_row[0]))
        par_row = paragraph_rows.fetchone()
    sqlite.commit()
