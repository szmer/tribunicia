"The generators that form an Iterable of pages usable with the `read_edition` function."
from io import StringIO
import os
import re

from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfparser import PDFParser

def pdf_as_pages(pdf_path, number_shift=0, exclude_ranges=[]):
    """
    Yield the pairs: (page number, contents of text files) from the PDF at the `pdf_path`.
    `number_shift` is added to the document number in the PDF to get the page number reported with
    the contents. This number is one-based by default (i.e., what a non-programmer would expect).

    Note that this function will only work on the searchable documents (that have the text embedded
    and selectable in a typical PDF browser). If the PDF is scanned and doesn't have the text
    embedded, you need to extract it into text files with an OCR program.

    Args:
    - `pdf_path`: the path to the PDF file.
    - `number_shift`: will be added to the page number in the file to get the reported page number.
    - `exclude_ranges`: a list of pairs defining the ranges of file numbers to exclude. For example,
        (14, 17) will exclude the pages 14, 15 and 16.
    """
    with open(pdf_path, 'rb') as file_stream:
        parser = PDFParser(file_stream)
        doc = PDFDocument(parser)
        rsrcmgr = PDFResourceManager()
        for page_n, page in enumerate(PDFPage.create_pages(doc)):
            output_string = StringIO()
            device = TextConverter(rsrcmgr, output_string, laparams=LAParams())
            interpreter = PDFPageInterpreter(rsrcmgr, device)
            skip= False
            for rng in exclude_ranges:
                if page_n >= rng[0] and page_n < rng[1]:
                    skip = True
                    break
            if skip:
                continue
            interpreter.process_page(page)
            yield (page_n+1+number_shift, output_string.getvalue())

def txt_dir_as_pages(root_path, pattern=r".*\.txt$", number_pattern=r"(\d+)\.\w+$",
        number_shift=0, exclude_ranges=[]):
    """
    Yield the pairs: (page number, contents of text files) in the `root_path`. We use only the files
    that satisfy the regex `pattern` (all the `.txt` files by default). The files should be
    numbered; the numbers are extracted with `number_pattern` (by default, the number is expected to
    be just before the file extension, such as `.txt`). `number_shift` is added to the file number
    to get the actual page number reported with the contents.

    Args:
    - `root_path`: the path to the root of the edition folder. 
    - `pattern`: a regex pattern that a file must satisfy to be excluded.
    - `number_pattern`: the regex pattern used to extract the number of the file from its name.
    - `number_shift`: will be added to the extracted file number to get the page number.
    - `exclude_ranges`: a list of pairs defining the ranges of file numbers to exclude. For example,
        (14, 17) will exclude the pages 14, 15 and 16.
    """
    # Get just the filenames from the directory, the ones that also satisfy the pattern.
    filenames = [fname  for fname  in os.listdir(root_path)
            if os.path.isfile(os.path.join(root_path, fname))
            and re.search(pattern, str(fname ))]
    # Associate with the page number and apply the excluded ranges.
    page_filenames = []
    for fname in filenames:
        try:
            file_number = re.search(number_pattern, fname).group(1)
            # Remove the prefixing zeroes if needed, and convert to int.
            zeroes = re.search(r'^(0+?)0?', file_number)
            if zeroes is not None:
                file_number = file_number[len(zeroes.group(1)):]
            file_number = int(file_number)
            # Check the excluded ranges.
            skip = False
            for rng in exclude_ranges:
                if file_number >= rng[0] and file_number < rng[1]:
                    skip = True
                    break
            if not skip:
                page_filenames.append((file_number+number_shift, fname))
        except AttributeError:
            raise ValueError(f'{fname} is captured by the pattern, but cannot find the file number')
    page_filenames.sort(key=lambda x: x[0])
    for (page_n, page_fname) in page_filenames:
        with open(os.path.join(root_path, page_fname)) as inp:
            yield (page_n, inp.read())
