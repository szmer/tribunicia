from collections import namedtuple
from datetime import datetime
import json
import os
import sqlite3

import lxml.etree as ET
from unidecode import unidecode

def write_tei_corpus_nkjp(output_path, corp_name, sqlite, edition_title):
    """
    Write a corpus representation in TEI, NKJP (Narodowy Korpus Języka Polskiego)-like format.

    Args:
    - `sqlite` - connection to a database where you have the paragraphs along with an interpretation
        from the `pl_morphoparsing` module.
    - `output_path` - the corpus XML files will be placed there in a directory named from the 
        corp_name`.
    - `corp_name` - the name used for the corpus directory and element IDs.
    - `edition_title` - the title of the edition in the database to write as a TEI corpus.
    """
    corp_name = unidecode(corp_name)
    # Create the output directory.
    output_path = os.path.join(output_path, corp_name.replace(' ', '_'))
    if not os.path.isdir(output_path):
        os.makedirs(output_path)
    # Get the edition row.
    sqlite.row_factory = sqlite3.Row
    edition_row = sqlite.execute('SELECT rowid, * FROM edition WHERE title = ?',
            (edition_title,)).fetchone()
    # Main XMLs of the document and the header XMLs.
    teiw = TeiWriterNkjp()
    doc_rows = sqlite.execute('SELECT rowid, * FROM document WHERE edition = ?',
            (edition_row['rowid'],))
    for doc_n, doc_row in enumerate(doc_rows):
        # (enrich the doc row dict-like object with the list of paragraphs, for tei_raw_with_header)
        pars = sqlite.execute('SELECT * FROM paragraph WHERE doc = ? ORDER BY nextpar',
                (doc_row['rowid'],)).fetchall()
        # (extend the named tuple with the par_rows field)
        Doc = namedtuple('Doc', list(doc_row.keys()) + ['par_rows'])
        doc_row = Doc(par_rows=pars, **{key: doc_row[key] for key in doc_row.keys()})
        (text_xml, header_xml) = teiw.tei_raw_with_header(corp_name, edition_row, doc_n, doc_row)
        # (the id here is corp_name-doc number)
        dir_name = header_xml.attrib['{'+teiw.nsmap['xml']+'}id']
        dir_path = os.path.join(output_path, dir_name)
        if not os.path.isdir(dir_path):
            os.mkdir(dir_path)
        with open(os.path.join(dir_path, 'text.xml'), 'wb+') as text_file:
            tree = ET.ElementTree(text_xml)
            tree.write(text_file, encoding='utf-8', xml_declaration=True, pretty_print=True)
        with open(os.path.join(dir_path, 'header.xml'), 'wb+') as header_file:
            tree = ET.ElementTree(header_xml)
            tree.write(header_file, encoding='utf-8', xml_declaration=True, pretty_print=True)
        segm_xml = teiw.tei_segmentation(corp_name, doc_row)
        morphos_xml = teiw.tei_morphosyntax(corp_name, doc_row)
        with open(os.path.join(dir_path, 'ann_segmentation.xml'), 'wb+') as segm_file:
            tree = ET.ElementTree(segm_xml)
            tree.write(segm_file, encoding='utf-8', xml_declaration=True, pretty_print=True)
        with open(os.path.join(dir_path, 'ann_morphosyntax.xml'), 'wb+') as morphos_file:
            tree = ET.ElementTree(morphos_xml)
            tree.write(morphos_file, encoding='utf-8', xml_declaration=True, pretty_print=True)

class TeiWriterNkjp():
    """
    A class for writing corpora in NKJP (Narodowy Korpus Języka Polskiego)-like format.
    """
    def __init__(self):
        self.nsmap = {
                'xi': 'http://www.w3.org/2001/XInclude',
                'nkjp': 'http://www.nkjp.pl/ns/1.0',
                'xml': 'http://www.w3.org/XML/1998/namespace',
                None: 'http://www.tei-c.org/ns/1.0'
                }

    def segment_id(self, token_counter):
        """
        Take a dictionary with the "corp_name" and "tok_num" (initilized with 0) keys. Return an ID
        for a segment with the `tok_num` token number.
        """
        token_counter['tok_num'] += 1
        return 'segm_1.{}-seg'.format(token_counter['tok_num'])

    def segment_elem(self, id, form, offset, after_pause, raw_parent_id):
        """
        Make a segment element for ann_segmentation.xml.

        Args:
        - `id` for the XML object.
        - `form` - of the word.
        - `offset` - the point in the original text where the form starts.
        - `after pause` - is this word after a space.
        - `raw_parent_id` - the XML ID of the object (the paragraph) where the word can be found in
            text.xml.

        The output "corresp" attribute points to the raw paragraph element id and the substring
        inside it corresponding to the segment.
        """
        segment = ET.Element('seg', {
            '{'+self.nsmap['xml']+'}id': id,
            'corresp': 'text.xml#string-range({},{},{})'.format(raw_parent_id, offset,
                len(form))})
        if not after_pause:
            segment.attrib['{'+self.nsmap['nkjp']+'}nps'] = 'true' # no space before the token
        # Inform of the form for convenience.
        w = ET.SubElement(segment, 'w')
        w.text = form
        return segment

    def morphos_segment_elem(self, token_id_counter, position, after_pause):
        """
        Make a segment element for ann_morphosyntax.xml. The second returned value is whether a
        chosen disambiguated interpretation was found inside.

        Args:
        - `tokens_id_counter` - a dict in the format wanted by the `segment_id` method.
        - `position` - a one-token description of a sentence position, as described by an
            output from `pl_morphoparsing.as_token_positions`.
        - `after pause` - is this part of the text after a space.
        """
        segment = ET.Element('seg', {
            '{'+self.nsmap['xml']+'}id': 'morph_1.{}-seg'.format(token_id_counter['tok_num']),
            'corresp': 'ann_segmentation.xml#segm_1.{}-seg'.format(token_id_counter['tok_num'])})
        fs = ET.SubElement(segment, 'fs', { 'type': 'morph' })
        if not after_pause: # no space before the token
            f = ET.SubElement(fs, 'f', { 'name': 'nps' })
            ET.SubElement(f, 'binary', { 'value': 'true' })
        f = ET.SubElement(fs, 'f', { 'name': 'orth' })
        text = ET.SubElement(f, 'string')
        text.text = position['orth'].strip()
        # Prepare the "interps" (all possible) tag.
        interps = ET.SubElement(fs, 'f', { 'name': 'interps' })
        previous_msd = None # reduce duplication by merging same lemmas and pos
        chosen_interp_idx = None
        chosen_interp_ref = None
        for interp_n, interp in enumerate(position['interps']):
            deduped = False
            if (previous_msd is not None # avoiding duplicate fs differing only in msd
                    and position['interps'][interp_n-1]['base'].strip() == interp['base'].strip()
                    and position['interps'][interp_n-1]['ctag'].strip() == interp['ctag'].strip()):
                # Avoid duplication on only msd differing. The vAlt sub-tag contains <symbol>s with
                # possible interpretations.
                deduped = True
                if previous_msd.tag == 'f':
                    # delete the previous msd as a direct child
                    previous_msd.remove(list(previous_msd.xpath('symbol'))[0])
                    valt = ET.SubElement(previous_msd, 'vAlt')
                    ET.SubElement(valt, 'symbol', { # re-add the previous msd
                        '{'+self.nsmap['xml']+'}id': 'morph_1.{}.{}.1-msd'.format(
                            token_id_counter['tok_num'], interp_n),
                        'value': position['interps'][interp_n-1]['msd']
                        })
                    ET.SubElement(valt, 'symbol', {
                        '{'+self.nsmap['xml']+'}id': 'morph_1.{}.{}.1-msd'.format(
                            token_id_counter['tok_num'], interp_n+1),
                        'value': interp['msd']
                        })
                    previous_msd = valt
                else:
                    ET.SubElement(valt, 'symbol', { # the valt variable was created above
                        '{'+self.nsmap['xml']+'}id': 'morph_1.{}.{}.1-msd'.format(
                            token_id_counter['tok_num'], interp_n+1),
                        'value': interp['msd']
                        })
            msd_id = 'morph_1.{}.{}.1-msd'.format(token_id_counter['tok_num'], interp_n+1)
            if interp['disamb'][2] is not None and interp['disamb'][2] == 'disamb':
                chosen_interp_idx = interp_n
                chosen_interp_ref = '#' + msd_id
            if deduped:
                continue
            interp_f_list = ET.SubElement(interps, 'fs', {
                '{'+self.nsmap['xml']+'}id': 'morph_1.{}.{}-lex'.format(
                    token_id_counter['tok_num'], interp_n+1),
                'type': 'lex',
                })
            f = ET.SubElement(interp_f_list, 'f', { 'name': 'base' })
            lemma = ET.SubElement(f, 'string')
            lemma.text = interp['base'].strip()
            f = ET.SubElement(interp_f_list, 'f', { 'name': 'ctag' })
            ET.SubElement(f, 'symbol', {
                '{'+self.nsmap['xml']+'}id': 'morph_1.{}.{}.ctag'.format(
                    token_id_counter['tok_num'], interp_n+1),
                'value': interp['ctag']
                })
            f = ET.SubElement(interp_f_list, 'f', { 'name': 'msd' })
            ET.SubElement(f, 'symbol', {
                '{'+self.nsmap['xml']+'}id': msd_id,
                'value': interp['msd']
                })
            previous_msd = f
        # Prepare the "disamb" (chosen interp) tag.
        if chosen_interp_idx is not None or chosen_interp_ref is not None:
            disamb = ET.SubElement(fs, 'f', { 'name': 'disamb' })
            tool_report = ET.SubElement(disamb, 'fs', { 'type': 'tool_report' })
            ET.SubElement(tool_report, 'f', { 'name': 'choice', 'fVal': chosen_interp_ref })
            chosen_interp_tag = ET.SubElement(tool_report, 'f', { 'name': 'interpretation' })
            chosen_interp_str = ET.SubElement(chosen_interp_tag, 'string')
            chosen_interp_str.text = ':'.join([
                position['interps'][chosen_interp_idx]['base'].strip(),
                position['interps'][chosen_interp_idx]['ctag']]
                # only take the msd add-list if not empty
                + [i for i in [position['interps'][chosen_interp_idx]['msd']] if i])
        # Finally, bump the token counter.
        token_id_counter['tok_num'] += 1
        return segment, chosen_interp_idx is not None

    def parenthesis_choice_elem(self, position_options, after_pause, tokens_id_counter,
            raw_parent_id):
        """
        Create a XML representing an internally branching part of the sentence graph, where some of
        the paths additionally involve multiple tokens.

        Args:
        - `position_options` - a description of a sentence position, as described by an output from
            `pl_morphoparsing.as_token_positions`.
        - `after pause` - is this part of the text after a space.
        - `tokens_id_counter` - a dict in the format wanted by the `segment_id` method.
        - `raw_parent_id` - the ID of the raw text counterpart of this part of this text (in
            `text.xml`).
        """
        choice = ET.Element('choice', nsmap=self.nsmap)
        for option in position_options:
            if len(option) > 1:
                paren = ET.SubElement(choice, '{'+self.nsmap['nkjp']+'}paren')
                for tok_n, token in enumerate(option):
                    seg = self.segment_elem(self.segment_id(tokens_id_counter),
                            token['orth'].strip(), token['offset'],
                            # is the token after a pause/space?
                            after_pause
                                if tok_n == 0 # (the first token)
                                else (option[tok_n-1]['offset']
                                    + len(option[tok_n-1]['orth'].strip()))
                                != token['offset'],
                            raw_parent_id)
                    paren.append(seg)
                choice.append(paren)
            else:
                seg = self.segment_elem(self.segment_id(tokens_id_counter), token['orth'].strip(),
                        token['offset'], after_pause, raw_parent_id)
                choice.append(seg)
        return choice

    def sentence_elem(self, id, break_page=False):
        """
        A <s> element meant to be added subelements (e.g. for ann_segmentation.xml).
        The `break_page` can be a number of the new page starting with this sentence.
        """
        sentence = ET.Element('s', {'{'+self.nsmap['xml']+'}id': id})
        if break_page:
            ET.SubElement(sentence, 'pb', {'n': str(break_page)})
        return sentence

    def raw_paragraph_elem(self, id: str, text: str, break_page=False, doc_start=False):
        """
        Create a TEI XML object for the raw paragraph with the `text` and `id`. The `break_page`
        can be a number of the new page starting with this paragraph. `Doc_start` means that a
        document starts with the paragraph.
        """
        paragraph = ET.Element('p', {'{'+self.nsmap['xml']+'}id': id})
        page_break = None
        if break_page:
            attrs = {'n': str(break_page)}
            if doc_start:
                attrs['type'] = 'doc_start'
            page_break = ET.SubElement(paragraph, 'pb', attrs)
        if page_break is not None:
            page_break.tail = text # after the pb
        else:
            paragraph.text = text
        return paragraph

    def paragraph_elem(self, id, break_page=False):
        """
        An empty <p> element meant to be added subelements (e.g. for ann_segmentation.xml). The
        `break_page` can be a number of the new page starting with this paragraph.
        """
        paragraph = ET.Element('p', {'{'+self.nsmap['xml']+'}id': id})
        if break_page:
            ET.SubElement(paragraph, 'pb', {'n': str(break_page)})
        return paragraph

    def raw_document_elem(self, id, paragraphs, type=False):
        """
        A XML object for the document, with the `id` and possibly `type` as provided. The
        `paragraphs` should be TEI XML objects already constructed for the raw texts.
        """
        attrs_dict = {'{'+self.nsmap['xml']+'}id': id}
        if type:
            attrs_dict['type'] = type
        document = ET.Element('div', attrs_dict)
        for par in paragraphs:
            document.append(par)
        return document

    def tei_empty_subcorpus(self, name=False, lang=False):
        """
        Create a XML object representing an empty corpus, with the ID = `name`. The second value is
        a handle for the body of the TEI corpus, provided for convenience.
        """
        tei_corp = ET.Element('teiCorpus', nsmap=self.nsmap)
        # TODO include the global header
        tei = ET.SubElement(tei_corp, 'TEI')
        ET.SubElement(tei, '{'+self.nsmap['xi']+'}include', {'href': 'header.xml'})
        attrs = {}
        if name:
            attrs['{'+self.nsmap['xml']+'}id'] = name + '_text'
        if lang:
            attrs['{'+self.nsmap['xml']+'}lang'] = lang
        text = ET.SubElement(tei, 'text', attrs)
        attrs = {}
        if name:
            attrs['{'+self.nsmap['xml']+'}id'] = name + '_body'
        body = ET.SubElement(text, 'body', attrs)
        return tei_corp, body

    def tei_raw_with_header(self, corp_name, edition_row, doc_n, doc_row):
        """
        Get a pair: (main XML object of the document - text.xml, the header XML object-header.xml).

        Args:
        - `corp_name` - the corpus name to be used as part of the header's XML ID.
        - `edition_row` - the edition row from SQLite for the document. At least a "title" element
            is required inside.
        - `doc_row` - a document row from SQLite, with an additional `par_rows` key injected, with
            the sorted paragraph rows for the document.
        """
        previous_page = 0
        par_num = 1
        tei_corp, body = self.tei_empty_subcorpus()
        # the document header
        header = ET.Element('teiHeader', nsmap=self.nsmap)
        title_stmt = ET.SubElement(header, 'titleStmt')
        header_title = ET.SubElement(title_stmt, 'title')
        source_desc = ET.SubElement(header, 'sourceDesc')
        bibl = ET.SubElement(source_desc, 'bibl', {'type': 'original'})
        if doc_row.source_date:
            date = ET.SubElement(bibl, 'date', {'when': doc_row.source_date})
            date.text = doc_row.source_date
        if doc_row.source_region:
            region = ET.SubElement(bibl, 'region')
            region.text = doc_row.source_region
        if doc_row.source_entity:
            author = ET.SubElement(bibl, 'author')
            author.text = doc_row.source_entity
        ET.SubElement(header, 'revisionDesc')
        # information on the whole outer publication
        edition = ET.SubElement(source_desc, 'bibl', {'type': 'edition'})
        series_title = ET.SubElement(edition, 'title', {'level': 's'})
        series_title.text = edition_row['title']
        if edition_row['editor']:
            editor_elem = ET.SubElement(edition, 'editor')
            editor_elem.text = edition_row['editor']
        if edition_row['place']:
            pub_place = ET.SubElement(edition, 'pubPlace', {'role': 'place'})
            pub_place.text = edition_row['place']
        if edition_row['publ_date']:
            pub_year_num = datetime.fromisoformat(edition_row['publ_date']).year
            pub_year = ET.SubElement(edition, 'date', {'when': str(pub_year_num) })
            pub_year.text = str(pub_year_num)
        pages_scope = ET.SubElement(edition, 'biblScope', {'unit': 'page'})
        if doc_row.par_rows[0]['pagenum'] != doc_row.par_rows[-1]['pagenum']:
            pages_scope.text = '{}-{}'.format(doc_row.par_rows[0]['pagenum'],
                    doc_row.par_rows[-1]['pagenum'])
        else:
            pages_scope.text = str(doc_row.par_rows[0]['pagenum'])
        title = doc_row.title
        # the header information
        header.attrib['{'+self.nsmap['xml']+'}id'] = '{}-{}'.format(corp_name, str(doc_n).zfill(5))
        header_title.text = 'TEI P5 encoded version of "{}"'.format(title)
        bibl_title = ET.SubElement(bibl, 'title', {'level': 'a'})
        bibl_title.text = title
        # the local subcorpus information
        tei_pars = []
        doc_start = True
        for par_row in doc_row.par_rows: 
            break_page = False
            if par_row['pagenum'] != previous_page:
                previous_page = break_page = par_row['pagenum']
            elif doc_start:
                break_page = par_row['pagenum']
            # NOTE We hardcode 1 in these ids, since each file contains one text
            tei_par = self.raw_paragraph_elem('txt_1.{}-ab'.format(par_num),
                    par_row['text'], break_page=break_page if break_page else 0,
                    doc_start=doc_start)
            tei_pars.append(tei_par)
            doc_start = False
            par_num += 1
        doc = self.raw_document_elem('txt_{}-div'.format(doc_n), tei_pars, type=doc_row.type)
        body.append(doc)
        return tei_corp, header

    def tei_segmentation(self, corp_name, doc_row):
        """
        Get a XML object for the segments (tokens) of the document.
        
        - `corp_name` - the corpus name to be used as part of tokens' XML ID.
        - `doc_row` - a document row from SQLite, with an additional `par_rows` key injected, with
            the sorted paragraph rows for the document.
        """
        token_id_counter = { 'tok_num': 0, 'corp_name': corp_name }
        tei_corp, body = self.tei_empty_subcorpus(name='segm', lang=doc_row.language)
        sent_num = 0
        for par_num, par in enumerate(doc_row.par_rows):
            tei_par = self.paragraph_elem('segm_{}-p'.format(par_num))
            raw_par_id = 'txt_1.{}-ab'.format(par_num)
            ends = [] # end offsets of each token
            for sent in json.loads(par['text_interp']):
                tei_sent = self.sentence_elem('segm_{}.{}-s'.format(par_num, sent_num))
                tei_par.append(tei_sent)
                for pos_n, position in enumerate(sent):
                    real_token = position # if it's a list, we'll have to dig to the dicts
                    end = 0
                    if type(real_token) == dict:
                        offset = real_token['offset']
                        end = offset + len(real_token['orth'].strip())
                    while type(real_token) != dict:
                        try:
                            offset = real_token[0]['offset']
                            end = offset + (sum([len(segm['orth'].strip() for segm in real_token)])
                                    + len(real_token)-1)
                        except TypeError:
                            pass
                        real_token = real_token[-1]
                    # is there a space before?
                    is_after_pause = True
                    if ends and ends[-1] == offset: # no space between this and previous token
                        is_after_pause = False
                    ends.append(end)
                    if type(position) == dict: # this happens when position is just a dict descr.
                        tei_seg = self.segment_elem(self.segment_id(token_id_counter),
                                position['orth'].strip(), position['offset'], is_after_pause,
                                raw_par_id)
                        tei_sent.append(tei_seg)
                    else: # a list of alternative paths throught the sentence graph positions
                        tei_choice = self.parenthesis_choice_elem(position, is_after_pause,
                                token_id_counter, raw_par_id)
                        tei_sent.append(tei_choice)
                sent_num += 1
            body.append(tei_par)
            par_num += 1
        return tei_corp

    def tei_morphosyntax(self, corp_name, doc_row):
        """
        Get a XML object for the morphosyntactic description of segments (tokens) of the document,
        for ann_morphosyntax.xml.
        
        - `corp_name` - the corpus name to be used as part of tokens' XML ID.
        - `doc_row` - a document row from SQLite, with an additional `par_rows` key injected, with
            the sorted paragraph rows for the document.
        """
        # here start from 1, because we increment after assigning the number
        token_id_counter = { 'tok_num': 1, 'corp_name': corp_name }
        tei_corp, body = self.tei_empty_subcorpus(name='morph', lang=doc_row.language)
        sent_num = 0
        for par_num, par in enumerate(doc_row.par_rows):
            tei_par = self.paragraph_elem('morph_{}-p'.format(par_num))
            ends = [] # end offsets of each token
            for sent in json.loads(par['text_interp']):
                tei_sent = self.sentence_elem('morph_{}.{}-s'.format(par_num, sent_num))
                tei_par.append(tei_sent)
                for pos_n, position in enumerate(sent):
                    real_token = position # if it's a list, we'll have to dig to the dicts
                    end = 0
                    if type(real_token) == dict:
                        offset = real_token['offset']
                        end = offset + len(real_token['orth'].strip())
                    while type(real_token) != dict:
                        try:
                            offset = real_token[0]['offset']
                            end = offset + (sum([len(segm['orth'].strip() for segm in real_token)])
                                    + len(real_token)-1)
                        except TypeError:
                            pass
                        real_token = real_token[-1]
                    # is there a space before?
                    is_after_pause = True
                    if ends and ends[-1] == offset: # no space between this and previous token
                        is_after_pause = False
                    ends.append(end)
                    disamb_found = False
                    if type(position) == dict:
                        tei_seg, disamb_found = self.morphos_segment_elem(token_id_counter, position,
                                is_after_pause)
                        tei_sent.append(tei_seg)
                    else:
                        for path in position:
                            for tok_n, token in enumerate(path):
                                if tok_n == 0:
                                    tei_seg, dsb = self.morphos_segment_elem(token_id_counter, token,
                                            is_after_pause)
                                else:
                                    tei_seg, dsb = self.morphos_segment_elem(token_id_counter, token,
                                            path[tok_n-1]['offset']
                                            + len(path[tok_n-1]['orth'].strip())
                                            != token['offset'])
                                if dsb:
                                    disamb_found = dsb
                                tei_sent.append(tei_seg)
                    if not disamb_found:
                        raise RuntimeError('Did not found a chosen intepretation inside '
                                           + str(position))
                sent_num += 1
            body.append(tei_par)
            par_num += 1
        return tei_corp
