import json
import logging
import re

def disamb_position(position):
    """Given a position in a sentence written to a paragraph's text_interp, return its form, base,
    interp, char offset according to the written disambiguation information (the interpretation with
    the highest disamb likelihood)."""
    form = ''
    # A simple token.
    if isinstance(position, dict):
        form = position['orth']
        offset = position['offset']
        max_disamb = (-1, -1) # the index of the interp, the disamb likelihood
        for i, interp in enumerate(position['interps']):
            if interp['disamb'][0] > max_disamb[1]:
                max_disamb = (i, interp['disamb'][0])
        chosen_interp = position['interps'][max_disamb[0]]
        term = chosen_interp['base']
        # Re-connect the ctag (POS) and the morphosyntactic data (msd).
        interp = chosen_interp['ctag'] + ':' + chosen_interp['msd']
    # A sequence of token sequence options covering the position.
    elif isinstance(position, list):
        option_candidates = [] # the list of candidate_tokens lists, see below
        # First, extract the series of tokens with their likelihoods from each
        # option.
        for option in position:
            # The most likely bases/lemmas for each token in the option, along
            # with their associated triples and disamb strength (triples).
            candidate_tokens = []
            for token in option:
                if not form:
                    form = token['orth']
                    offset = token['offset']
                max_token_disamb = (-1, -1)
                for i, interp in enumerate(token['interps']):
                    if interp['disamb'][0] > max_token_disamb[1]:
                        max_token_disamb = (i, interp['disamb'][0])
                chosen_interp = token['interps'][max_token_disamb[0]]
                candidate_tokens.append((chosen_interp['base'],
                                         chosen_interp['ctag'] + ':'
                                         + chosen_interp['msd'],
                                         max_token_disamb[1]))
                if not form:
                    form = chosen_interp['form']
            option_candidates.append(candidate_tokens)
        # Select the strongest option as the term.
        max_disamb = ('', '', -1) # the term, the interp, the likelihood
        for option in option_candidates:
            likelihood = sum(tok[2] for tok in option) / len(option)
            # NOTE we only take the first lemma in the option, assuming the
            # others to be suffixes.
            if likelihood > 0.0 and len(option) > 1:
                logging.debug(f'Assuming {option[0][0]} base for {option}')
            if likelihood > max_disamb[2]:
                max_disamb = (option[0][0], option[0][1], likelihood)
        term = max_disamb[0]
        interp = max_disamb[1]
    else:
        raise RuntimeError(f'unexpected type of the position {position}')
    return form, term, interp, offset

def sentences_as_terms(par_row, no_merging=False):
    """
    For a paragraph row from the db (columns: text_interp should be the first column in it) get:
    list of forms, list of terms (lemmas), list of interps for the forms, list of indices for the
    forms, as long as there are sentences in the source.

    No_merging governs whether we should merge sentences which should be together but are split
    incorrectly because of abbreviations. Note this messes up the word offset information.
    """
    interp_data = json.loads(par_row[0]) # the interp from the pl_morphoparsing module
    # Collect the sentence parts that should be part of the next sentence because of some known
    # abbreviation errors.
    left_sent_indices = []
    for sent_n, sentence in enumerate(interp_data):
        if not no_merging and (sent_n + 1 < len(interp_data) and len(sentence) >= 2
                and isinstance(sentence[-2], dict) and len(sentence[-2]['orth']) == 1):
            left_sent_indices.append(sent_n)
            continue
        elif left_sent_indices:
            for prev_sent_n in reversed(left_sent_indices):
                sentence = interp_data[prev_sent_n] + sentence
            left_sent_indices = []
        sentence_forms = []
        sentence_terms = []
        sentence_interps = []
        sentence_offsets = []
        for position in sentence:
            form, term, interp, offset = disamb_position(position)
            if re.search('\\w', term):
                sentence_forms.append(form)
                sentence_terms.append(term.strip())
                sentence_interps.append(interp)
                sentence_offsets.append(offset)
        yield sentence_forms, sentence_terms, sentence_interps, sentence_offsets


def position_offset(position):
    """Given a position in a sentence written to a paragraph's text_interp, return char offset in
    the original paragraph text."""
    # A simple token.
    if isinstance(position, dict):
        return position['offset']
    # A sequence of token sequence options covering the position.
    elif isinstance(position, list):
        return position[0][0]['offset']
    else:
        raise RuntimeError(f'unexpected type of the position {position}')

def position_char_len(position):
    """Given a position in a sentence written to a paragraph's text_interp, return full char len
    of the word(s) regardless of interpretation."""
    # A simple token.
    if isinstance(position, dict):
        return len(position['orth'])
    # A sequence of token sequence options covering the position.
    elif isinstance(position, list):
        collected_len = 0
        for subpos in position[0]:
            collected_len += len(subpos['orth'])
        return collected_len
    else:
        raise RuntimeError(f'unexpected type of the position {position}')
