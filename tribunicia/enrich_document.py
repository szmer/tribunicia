"""
Functions that add columns to the document rows in the database, when an edition of sources is
already loaded.
"""

from tribunicia.text_utils import is_sejmik_resolution

def add_is_sejmik_resolution(sqlite, edition):
    """
    For all documents in the `edition` (treated as the edition name) guess the information whether
    it is a sejmik resolution or not and annotate it in the document types.
    """
    edition_num_row = sqlite.execute('SELECT rowid FROM edition WHERE title = ?',
            (edition,)).fetchone()
    if edition_num_row is None:
        raise RuntimeError(f'cannot find edition "{edition}" in the database')
    doc_rows = sqlite.execute('SELECT rowid, title FROM document WHERE edition = ?',
            (edition_num_row[0],))
    doc_row = doc_rows.fetchone()
    while doc_row is not None:
        paragraph_rows = sqlite.execute('SELECT text FROM paragraph WHERE doc = ?',
            (doc_row[0],))
        paragraphs = [row[0] for row in paragraph_rows.fetchall()]
        doc_type = 'other sejmik-related document'
        if is_sejmik_resolution(doc_row[1], paragraphs):
            doc_type = 'sejmik resolution'
        sqlite.execute('UPDATE document SET type = ? WHERE rowid = ?',
                (doc_type, doc_row[0]))
        doc_row = doc_rows.fetchone()
    sqlite.commit()

