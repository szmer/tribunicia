"Collect occurrences of phrases like pan ... and get statistics on them."
import argparse
from collections import defaultdict
import csv
from itertools import chain
import sqlite3

from lib_chrono_explorer import collect_freqtables, docs_query, lexemes_timeframe_region
from tribunicia.text_utils import get_constraint_pattern, find_constrained_subsequences

argparser = argparse.ArgumentParser()
argparser.add_argument('--db_path',
        default='tribunicia.db',
        help='the path to the .db file to examine')
argparser.add_argument('--doc_type',
        default='sejmik resolution',
        help='the type of document to take into account')
argparser.add_argument('--timeframe',
        default='1572,1632',
        help='the range of years to consider')
argparser.add_argument('--offices_path',
        default='offices.csv',
        help='the path to the CSV database with office names')
argparser.add_argument('--surnames_path',
        default='surnames.csv',
        help='the path to the CSV database with surnames')
argparser.add_argument('--html_monitor',
        help='print all the docs with the phrases highlighted for inspection')

args = argparser.parse_args()
sqlite = sqlite3.connect(args.db_path)

_, TIMEFRAME, _ = lexemes_timeframe_region(args.timeframe, spec_chars_ok=True)

REGION_TO_EDITION = {
        'Wielkopolska': 'województw poznańskiego',
        'lubelskie': 'województwa lubelskiego'
        }
TIME_PERIODS = [(1571, 1600), (1601, 1630), (1631, 1660), (1661, 1690)]
PERSONS_END = 1633 # FIXME when to stop looking for individuals, make it configurable?

#
# Indices of lexemes to look for in phrases.
#
BASE_LEXEMES = {'pan': ['pan', 'pp'],
                'jegomość pan': ['jmp'],
                'mościwy': ['mciwych', 'M', 'mci'], 
                'urodzony': ['urodzony', 'urodzić', 'Ura'],
                'jegomość': ['jm', 'JM', 'JMci', 'Jm'],
                'ichm': ['ichm',  'Ichm','ichmciami', 'ichmci', 'ichmciów', 'ichmciom'],
                'wielmożny': ['wielmożny', 'w'],
                'jaśnie wielmożny': ['jw'],
                }
AFFIX_LEXEMES = {
                'JKM': ['JKMć', 'J', 'j', 'K', 'k'],
                'nasz': ['nasz', 'n'], 
                #'X': ['święty'], # X śp = świętej pamięci
                'brat': ['brat', 'bracia'],
                'obywatel': ['obywatel'],
                #'Bóg': ['Bóg'],
                'conj': ['i', 'z', 'ze', 'na', ',', r'\.'],
                'jednostka': ['powiat', 'województwo', 'ziemia'],
                'odmiejscowe': ['[^@]+[cs]ki[^@]{0,4}'],
                'nazwy_własne': ['[A-ZŻŹĆŁÓŚ][^@]{2,}']
               }

def reverse_index(idx):
    "Reverse the original index from key -> [vals] to val -> [keys]."
    rev_idx = defaultdict(lambda: [])
    for key, val_list in idx.items():
        for val in val_list:
            if val not in rev_idx:
                rev_idx[val] = []
            rev_idx[val].append(key)
    return rev_idx
REV_BASE_LEXEMES = reverse_index(BASE_LEXEMES)
REV_AFFIX_LEXEMES = reverse_index(AFFIX_LEXEMES)

#
# Read and prepare the csv databases.
#
OFFICE_NAMES = dict()
with open(args.offices_path, encoding='utf-8') as csv_inp:
    rd = csv.reader(csv_inp)
    next(rd, None) # skip the header
    for row in rd:
        OFFICE_NAMES[row[0]] = [row[0]]
REV_OFFICE_NAMES = reverse_index(OFFICE_NAMES)

# pylint: disable=too-few-public-methods
class NameRecord():
    "A context record for an individual person."

    def __init__(self, no, first_name, surname_versions, region, offices, possessions):
        self.no = int(no) # unique number
        self.first_name = first_name
        self.surname_versions = surname_versions.split('|')
        self.region = region
        self.offices = offices.split('|')
        self.possessions = possessions.split('|')

    def match_occurence(self, occurrence):
        "Check if a person occurrence can be related to this record."
        if self.first_name:
            first_name = self.first_name.split()[0] # only 1st name seems to be used
        else:
            first_name = False # check only surname if it's all there is
        return (((first_name in occurrence.terms) if first_name else True)
                and any(s in occurrence.terms for s in self.surname_versions))

FIRST_NAMES = set()
PERSON_RECORDS = []
PERSON_NAMES = dict()
PERSON_FEATURES = set() # possession-related
with open(args.surnames_path, encoding='utf-8') as csv_inp:
    rd = csv.reader(csv_inp)
    next(rd, None) # skip the header
    for row in rd:
        if not row[-1]: # skip if no features
            continue
        rec = NameRecord(*row)
        PERSON_RECORDS.append(rec)
        first_names = row[1].split()
        FIRST_NAMES.update(first_names)
        pers_surnames = row[2].split('|')
        if pers_surnames[0] not in PERSON_NAMES: # treat the 1st variant as canonical version
            PERSON_NAMES[pers_surnames[0]] = set()
        PERSON_NAMES[pers_surnames[0]].update(set(pers_surnames))
        if first_names:
            PERSON_NAMES[pers_surnames[0]].update(set(first_names))
        PERSON_FEATURES.update(set(row[-1].split('|')))
for k in PERSON_NAMES:
    PERSON_NAMES[k] = list(PERSON_NAMES[k])
REV_PERSON_NAMES = reverse_index(PERSON_NAMES)

#
# Collect the pattern to find sequences forming person entity phrases.
#
BIG_PATTERN = get_constraint_pattern(set(list(chain.from_iterable(BASE_LEXEMES.values()))
                                         + list(chain.from_iterable(AFFIX_LEXEMES.values()))
                                         + list(chain.from_iterable(PERSON_NAMES.values()))
                                         + list(chain.from_iterable(OFFICE_NAMES.values()))))
#
# Look for occurrences of person phrases.
#

class PersonOccurence():
    "Store info about a phrase that is about a person."

    def __init__(self, par_range, terms, forms, interps, observables, year, edition, pagen,
                 previous=False):
        """
        Terms are lexemes/bases, observables (as a list for each term) point to roots or features to
        analyze.
        """
        self.par_range = par_range
        self.terms = terms
        self.forms = forms
        self.interps = interps
        self.observables = observables
        self.year = int(year)
        self.edition = edition
        self.pagen = pagen
        self.previous = previous
        for period in TIME_PERIODS:
            if period[0] <= year <= period[1]:
                self.period = period
                break
        else:
            self.period = None

    def pperiod(self):
        "Pretty-printed time period."
        if self.period is None:
            return ''
        return f'{self.period[0]}-{self.period[1]}'

    def preceding_chain(self):
        "The chain of person mentions immediately preceding this one."
        pchain = []
        prev = self.previous
        while prev:
            pchain.insert(0, prev)
            prev = prev.previous
        return pchain

if args.html_monitor:
    MONITOR_OUT = open(args.html_monitor, 'w+', encoding='utf-8')
def monitor_print(*fun_args):
    "Send text to the HTML monitor file if specified"
    if args.html_monitor:
        print(*fun_args, file=MONITOR_OUT)

def person_subranges(terms, rg):
    """
    Extract the list of subranges related to individual persons from the terms corresponding to rg.
    Ranges are inclusive both ways.
    """
    candidate_subrgs = [[]]
    for t in terms:
        if candidate_subrgs[-1] and t in [',', 'i']:
            candidate_subrgs.append([])
        candidate_subrgs[-1].append(t)
    term_subrgs = [candidate_subrgs[0]]
    for candidate in candidate_subrgs[1:]:
        if ('pan' in candidate) or any((e in candidate) for e in FIRST_NAMES):
            term_subrgs.append(candidate)
        else:
            term_subrgs[-1] += candidate
    idx_subrgs = [(rg[0], rg[0]+len(term_subrgs[0])-1)]
    for subrg in term_subrgs[1:]:
        idx_subrgs.append((idx_subrgs[-1][1]+1, idx_subrgs[-1][1]+len(subrg)))
    return idx_subrgs

assert person_subranges(['pan', 'kasztelan', ',', 'i', 'pan', 'cześnik'],
                        (2, 7)) == [(2, 4), (5, 7)]

OCCURRENCES = dict()
EDITION_SCHEMA_FREQS = dict()

edition_rows = sqlite.execute('SELECT rowid, title, place FROM edition')
monitor_print('<html><head></head><body>')
ed_row = edition_rows.fetchone()
while ed_row is not None:
    edition_title = ed_row[1]
    OCCURRENCES[edition_title] = []
    monitor_print(f'<h1>{edition_title}</h1>')

    # pylint: disable=unused-argument
    def person_occurence_classif(sent_forms, sent_terms, sent_interps, year, pagen, counter):
        "A function for use with collect_freqtables."
        monitor_print('<p>')
        phrase_ranges = find_constrained_subsequences(sent_terms, BIG_PATTERN)
        used_ranges = []
        for rg in phrase_ranges:
            terms_for_check = [sent_terms[ti] for ti in range(rg[0], rg[1]+1)]
            subranges = person_subranges(terms_for_check, rg)
            local_phrase_objs = []
            for (sub_n, subrange) in enumerate(subranges):
                phrase_terms = [sent_terms[ti] for ti in range(subrange[0], subrange[1]+1)]
                phrase_forms = [sent_forms[ti] for ti in range(subrange[0], subrange[1]+1)]
                phrase_interps = [sent_interps[ti] for ti in range(subrange[0], subrange[1]+1)]
                # Require that "pan" or "urodzony" etc. is part of the phrase to consider.
                if not any((base in phrase_terms) for base in BASE_LEXEMES) or (
                        len(phrase_terms) == 1 and phrase_terms[0] == 'pan'):
                    continue
                used_ranges.append(subrange)
                phrase_obj = PersonOccurence(
                        subrange, phrase_terms, phrase_forms, phrase_interps,
                        list(map(lambda t: (REV_BASE_LEXEMES[t]
                                            + REV_AFFIX_LEXEMES[t]
                                            + REV_PERSON_NAMES[t]
                                            + REV_OFFICE_NAMES[t]),
                                 phrase_terms)),
                        year, edition_title, pagen,
                        previous=local_phrase_objs[sub_n-1]
                        if sub_n > 0 and len(local_phrase_objs) == sub_n else False
                        )
                local_phrase_objs.append(phrase_obj)
                OCCURRENCES[edition_title].append(phrase_obj)
                counter.update([' + '.join([str(a) for a in phrase_obj.observables])])

        if args.html_monitor:
            for fi, form in enumerate(sent_forms):
                printed = False
                # sorry but no point in optimizing this
                for rg in used_ranges:
                    if rg[0] <= fi <= rg[1]:
                        monitor_print(f'<span style="background-color: DarkKhaki">{form}</span>')
                        printed = True
                        break
                if not printed:
                    monitor_print(form)
        monitor_print('</p>')

    query, query_args = docs_query(sqlite, edition_title, args.doc_type)
    doc_rows = sqlite.execute(query, tuple(query_args))
    freqtab = collect_freqtables(sqlite, doc_rows, TIMEFRAME, [person_occurence_classif])[0]
    EDITION_SCHEMA_FREQS[edition_title] = freqtab

    ed_row = edition_rows.fetchone()

monitor_print('</body></html>')
if args.html_monitor:
    MONITOR_OUT.close()

#
# Get possessions features and offices statistics.
#
FEATURE_PHRASES = {r: {ft: [] for ft in PERSON_FEATURES} for r in REGION_TO_EDITION}
OFFICE_PHRASES = dict() # region -> period -> office name -> occurrences
for edition_name, occurrences in OCCURRENCES.items():
    ed_region = None
    for reg_name, ed_pattern in REGION_TO_EDITION.items():
        if ed_pattern in edition_name:
            ed_region = reg_name
            if reg_name not in OFFICE_PHRASES:
                OFFICE_PHRASES[reg_name] = dict()
            break
    for occ in occurrences:
        if occ.year < PERSONS_END:
            for pers in PERSON_RECORDS:
                if pers.region != ed_region:
                    continue
                if pers.match_occurence(occ):
                    for ft in pers.possessions:
                        FEATURE_PHRASES[ed_region][ft].append(occ)
        if not occ.pperiod():
            continue
        if occ.pperiod() not in OFFICE_PHRASES[ed_region]:
            OFFICE_PHRASES[ed_region][occ.pperiod()] = dict()
        for office in OFFICE_NAMES:
            if office in occ.terms:
                if office not in OFFICE_PHRASES[ed_region][occ.pperiod()]:
                    OFFICE_PHRASES[ed_region][occ.pperiod()][office] = []
                OFFICE_PHRASES[ed_region][occ.pperiod()][office].append(occ)

def check_keywords(term_seqs, keywords):
    "Check if some sequence contains any of the keywords."
    for s in term_seqs:
        if any((k in s) for k in keywords):
            return True
    return False

def proportion_wielmozny(region, period, office_name):
    "Proportion where the office holder is referred to with wielmożny-type stylings."
    phrases = OFFICE_PHRASES[region][period][office_name]
    if not phrases:
        return 0.0
    strict_count = sum(check_keywords([p.terms], BASE_LEXEMES['wielmożny']
                      + BASE_LEXEMES['jaśnie wielmożny']) for p in phrases)
    inclusive_count = sum(check_keywords(
        [p.terms] + [pv.terms for pv in p.preceding_chain()],
        BASE_LEXEMES['wielmożny'] + BASE_LEXEMES['jaśnie wielmożny'])
                           for p in phrases)
    return strict_count / len(phrases), inclusive_count / len(phrases)

def proportion_urodzony(region, period, office_name):
    "Proportion where the office holder is referred to with the urodzony styling."
    phrases = OFFICE_PHRASES[region][period][office_name]
    if not phrases:
        return 0.0
    strict_count = sum(check_keywords([p.terms], BASE_LEXEMES['urodzony']) for p in phrases)
    inclusive_count = sum(check_keywords(
        [p.terms] + [pv.terms for pv in p.preceding_chain()],
        BASE_LEXEMES['urodzony'])
                           for p in phrases)
    return strict_count / len(phrases), inclusive_count / len(phrases)

# pylint: disable=consider-using-dict-items
for ed_region in OFFICE_PHRASES:
    print(f'=== Region: {ed_region} ===')
    for tm_period in OFFICE_PHRASES[ed_region]:
        print(f'-- {tm_period} --')
        for office in OFFICE_PHRASES[ed_region][tm_period]:
            prop_w = proportion_wielmozny(ed_region, tm_period, office)
            prop_ur = proportion_urodzony(ed_region, tm_period, office)
            print(f'{office}: w. {prop_w}; ur. {prop_ur}', end=' -- ')
        print()
    print()

###-for (edition_title, edition_freq) in EDITION_SCHEMA_FREQS.items():
###-    print('==', edition_title, '==')
###-    print(edition_freq)
###-    print()
