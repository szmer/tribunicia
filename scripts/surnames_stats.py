"Look at NER annotation put in with a huggingface model."
import argparse
import json
import sqlite3

from tribunicia.lib_morphoparsing import disamb_position, position_offset, position_char_len

argparser = argparse.ArgumentParser()
argparser.add_argument('--db_path',
        default='tribunicia.db',
        help='the path to the .db file to examine')

args = argparser.parse_args()

sqlite = sqlite3.connect(args.db_path)

lemma_freqs = {}
paragraph_rows = sqlite.execute('SELECT text_potential_surnames, doc, text, text_interp '
                                'FROM paragraph '
                                'WHERE doc IS NOT NULL')
par_row = paragraph_rows.fetchone()
not_matched = {}
while par_row is not None:
    if par_row[0] is None:
        par_row = paragraph_rows.fetchone()
        continue
    ner_annot = json.loads(par_row[0])
    for entity in ner_annot:
        kind = entity['kind']
        if 'morpho_interp' not in entity:
            key = entity['word']
            # Debug info.
            if False:#key == 'Zurowski' and entity['word_offset'] == 868:
                sents = json.loads(par_row[3])
                sent_offsets = [0]
                for s in sents:
                    nexto = sent_offsets[-1]
                    nexto += position_offset(s[-1]) + position_char_len(s[-1])
                    sent_offsets.append(nexto)
                print(sent_offsets)
                print('-----------------------')
                dbg = [(si, sent_offsets[si] + position_offset(el), el)
                       for (si, sent) in enumerate(sents)
                       for el in sent if 'Bóg' in str(el)]
                for item in dbg:
                    print(item)
            if key not in not_matched:
                not_matched[key] = 0
            not_matched[key] += 1
            continue
        base = entity['morpho_interp']['base']
        key = f'{kind}-{base}'
        if key not in lemma_freqs:
            lemma_freqs[key] = 0
        lemma_freqs[key] += 1
    par_row = paragraph_rows.fetchone()

sorted_freqs = sorted(lemma_freqs.items(), key=lambda x: x[1], reverse=True)
for (key, val) in sorted_freqs[:100]:
    print(key, val)
print('-----------------------')
sorted_freqs = sorted(not_matched.items(), key=lambda x: x[1], reverse=True)
print(sum(lemma_freqs.values()))
print(sum(not_matched.values()))
for (key, val) in sorted_freqs[:100]:
    print(key, val)
