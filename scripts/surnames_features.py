"""Use a csv surnames database to analyze linguistic image of features of persons."""
import argparse
import csv
import re
import sqlite3

from lib_chrono_explorer import (
        lexemes_timeframe_region, ngrams, read_equivs_dict_from_db, sentence_bigrams,
        sentence_co_occurrences
        )

argparser = argparse.ArgumentParser()
argparser.add_argument('--db_path',
        default='tribunicia.db',
        help='the path to the .db file to examine')
argparser.add_argument('--surnames_path',
        default='surnames.csv',
        help='the path to the CSV database with surnames')
argparser.add_argument('--timeframe',
        default='1572,1632',
        help='the range of years to consider')
argparser.add_argument('--doc_type',
        default='sejmik resolution',
        help='the type of document to take into account')

args = argparser.parse_args()

sqlite = sqlite3.connect(args.db_path)

_, TIMEFRAME, _ = lexemes_timeframe_region(args.timeframe, spec_chars_ok=True)
COMPAR_FUNCTION = lambda x, y: re.search(x, y) is not None

equivs_dict = read_equivs_dict_from_db(sqlite)
def equiv(text):
    "If text matches one of the sorted equiv patterns, return the equiv, otherwise the text."
    for (equivalent, pattern) in equivs_dict.items():
        if pattern.match(text):
            return equivalent
    return text

class ContextsRecord:
    "Store associated language context info for a group of words."
    def __init__(self, patterns):
        self.region = ''
        # Patterns-words that alternatively describe the central lexeme.
        self.patterns = patterns
        self.feature = None # feature name of None if NameRecord
        self.bigrams_freq = None
        self.bigram_pairings_freq = None
        self.bigrams_hits_count = None
        self.sent_bigrams_freq = None
        self.sent_bigram_pairings_freq = None
        self.sent_bigrams_hits_count = None
        self.sent_co_ocs_freq = None
        self.sent_co_ocs_hits_count = None

    def patterns_as_regex(self):
        "Get regex catching all the patterns."
        if not self.patterns:
            raise ValueError
        if len(self.patterns) == 1:
            return self.patterns[0]
        patterns = [p for p in self.patterns if p] # remove empty
        return f'({"|".join(patterns)})'

    def add_bigrams(self):
        "Add bigrams data to the object."
        found_ngrams, pairings, _, hits_count = ngrams(
                sqlite,
                # patterns as one "lexeme"
                [self.patterns_as_regex()], self.region, TIMEFRAME,
                compar_function=COMPAR_FUNCTION,
                equiv=equiv, doc_type=args.doc_type)
        self.bigrams_freq = found_ngrams
        self.bigram_pairings_freq = pairings
        self.bigrams_hits_count = hits_count

    def add_sent_bigrams(self):
        "Add sentence bigrams data to the object."
        found_ngrams, pairings, _, hits_count = sentence_bigrams(
                sqlite, [self.patterns_as_regex()], self.region, TIMEFRAME,
                compar_function=COMPAR_FUNCTION,
                equiv=equiv, doc_type=args.doc_type)
        self.sent_bigrams_freq = found_ngrams
        self.sent_bigram_pairings_freq = pairings
        self.sent_bigrams_hits_count = hits_count

    def add_sent_co_occurs(self):
        "Add sentence co-occurrences data to the object."
        found_co_ocs, hits_count = sentence_co_occurrences(
                sqlite, [self.patterns_as_regex()], self.region, TIMEFRAME,
                compar_function=COMPAR_FUNCTION,
                equiv=equiv, doc_type=args.doc_type,
                include_pos=['adj', 'adv'], include_lexemes=['jm', 'jw', 'ur'])
        self.sent_co_ocs_freq = found_co_ocs
        self.sent_co_ocs_hits_count = hits_count

class NameRecord(ContextsRecord):
    "A context record for an individual person."
    def __init__(self, no, first_name, surname, region, offices, possessions):
        self.no = int(no) # unique number
        self.first_name = first_name
        self.surname = surname
        self.region = region
        self.offices = offices
        self.possessions = possessions
        super().__init__([surname])

def expand_office_features(features):
    "Add generalized features related to concrete terrestrial offices."
    additionals = set()
    for ft in features:
        for elem in ['kasztelan', 'wojewoda']:
            if elem in ft:
                additionals.add(elem)
    return features + list(additionals)

#
# Read the names database.
#
records = [] # records of names to consider in the corpus
with open(args.surnames_path, encoding='utf-8') as csv_inp:
    rd = csv.reader(csv_inp)
    next(rd, None) # skip the header
    for row in rd:
        rec = NameRecord(*row)
        ###rec.add_bigrams()
        ###rec.add_sent_bigrams()
        records.append(rec)
for r_n, record in enumerate(records):
    assert r_n + 1 == record.no

#
# Map records features to records.
#
features_map = {}
all_office_features = set()
for record in records:
    office_features = expand_office_features(record.offices.split('|'))
    all_office_features.update(office_features)
    poss_features = record.possessions.split('|')
    for ft_name in office_features + poss_features:
        if not ft_name in features_map:
            features_map[ft_name] = []
        features_map[ft_name].append(record.no)

#
# Get bigrams and sent bigrams for features.
#
feature_records = []
for ft_name, ft_nos in features_map.items():
    lemmas = [records[no-1].surname for no in ft_nos]
    if ft_name in all_office_features:
        lemmas.append(ft_name)
    ft_rec = ContextsRecord(lemmas)
    ft_rec.feature = ft_name
    ###ft_rec.add_bigrams()
    ###ft_rec.add_sent_bigrams()
    ft_rec.add_sent_co_occurs()
    feature_records.append(ft_rec)
