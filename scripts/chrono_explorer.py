import argparse
from cmd import Cmd
from collections import Counter
import logging
from pathlib import Path
# For command autocompletion.
import readline # pylint: disable=unused-import
import re
import sqlite3
import sys

from lib_chrono_explorer import *
from tribunicia.lib_morphoparsing import sentence_as_terms
from tribunicia.utils import exhaust_paragraph_rows

try:
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mtick
except Exception:
    logging.error(' !NOTE! You may need to install the optional dependencies, see the readme.')
    raise

argparser = argparse.ArgumentParser()
argparser.add_argument('--db_path',
        default='tribunicia.db',
        help='the path to the .db file to examine')
argparser.add_argument('--edition',
        help='the edition from which to read documents')
argparser.add_argument('--doc_type',
        default='sejmik resolution',
        help='the type of document to take into account')
argparser.add_argument('--file_to_freq',
        help='a file to read arguments to years command line by line and print frequencies')
argparser.add_argument('--regex',
        action='store_true',
        default=False,
        help='if set, the word specifications will be treated as regexes; note this can break if '
               'you effectively allow more than one letter as the first character of a word')
argparser.add_argument('--drop_intro',
                       type=int,
                       help='if set to a number of words, this number will be dropped from the '
                       'beginning of each document being examined by the script')
argparser.add_argument('--monochrome',
        action='store_true',
        default=False,
        help='only draw the relative frequency on plots, so they\'re monochromatic.')

args = argparser.parse_args()

sqlite = sqlite3.connect(args.db_path)

VERB_POS = [ 'fin', 'bedzie', 'praet', 'impt', 'imps', 'inf', 'ger', 'pcon', 'pant', 'pact',
            'pactb', 'ppas', 'ppasb', 'ppraet', 'fut', 'plusq', 'aglt', 'agltaor', 'winien', 'pred']


if args.regex:
    # Treat x as a regex pattern for y (assumed to be the term from the corpus).
    compar_function = lambda x, y: re.search(x, y) is not None
else:
    compar_function = lambda x, y: x == y

class SearchShell(Cmd):
    prompt = '(search) '
    intro = 'Welcome to tribunicia search.'


    def __init__(self):
        super().__init__()
        self.destination = sys.stdout
        self.reset_flags()
        self.equivs = []
        self.do_list_equivs('')

    def reset_flags(self):
        self.listmax = 20
        self.draw_plots = True
        self.short_years_cmd = False

    #
    # "Equiv" functions used to map some regexes to uniform terms for n-grams.
    #
    def equiv(self, text):
        "If text matches one of the sorted equiv patterns, return the equiv, otherwise the text."
        for (equiv, pattern) in self.equivs:
            if pattern.match(text):
                return equiv
        return text

    def do_list_equivs(self, _):
        """
        Load equivs from the database (creating the tribunicia table if necessary) and list them.
        This is also guaranteed to run at program initialization.
        """
        equivs_dict = read_equivs_dict_from_db(sqlite)
        self.equivs = list(equivs_dict.items())
        if len(self.equivs) == 0:
            print('No equivs for n-grams known from the database.')
        else:
            for (pattern, equiv) in self.equivs:
                print(f'{pattern} -> {equiv}')

    def do_set_equiv(self, equiv_pair):
        """
        Set an equivalence for computing n-grams: the argument should have the form "equiv;pattern".
        Equiv is the term form that will appear in the n-gram reports; pattern is a regular
        expression. Equivs are stored in the database. Set a blank pattern to remove the equiv.
        """
        equivs_dict = dict(self.equivs)
        equiv, _, pattern = equiv_pair.partition(';')
        if not pattern.strip():
            del equivs_dict[equiv]
        else:
            pattern = re.compile(pattern)
            equivs_dict[equiv] = pattern
        sqlite.execute('UPDATE tribunicia_settings SET setting_value = ?'
                       ' WHERE setting_key = "equivs"',
                       # deal with re pattern JSON serialization
                       (json.dumps(equivs_dict, default=lambda patt: patt.pattern),))
        sqlite.commit()
        self.equivs = list(equivs_dict.items())

    #
    # The output functions, capable of writing to a file or to console.
    #
    def clear_output(self, sender):
        """
        Make sure, if the output is to be sent to a file, that the file will not exist before
        writing the new data.
        """
        if self.destination != sys.stdout:
            Path(self.destination, Path(sender+'.txt')).unlink(missing_ok=True)


    def write_output(self, message, sender):
        """
        Use this instead of print() to send the output to a file instead of stdout if a destination
        folder is set.
        """
        if self.destination == sys.stdout:
            print(message)
        elif isinstance(self.destination, Path):
            self.destination.mkdir(exist_ok=True)
            with open(Path(self.destination, Path(sender+'.txt')), 'a+') as out:
                print(message, file=out)
        else:
            raise ValueError(f'Unrecognized destination {self.destination}')


    def do_set_folder(self, path_spec):
        """
        Set folder to which the outputs should be written, instead of the console. Use set_folder
        none to set it back to console.
        """
        if path_spec.strip() == "none":
            self.destination = sys.stdout
        self.destination = Path(path_spec)


    def do_set_listmax(self, maxval):
        """
        Change the maximum value printed in top X lists (default: 20). Pass "none" to print all
        items.
        """
        if maxval.lower() == "none":
            self.listmax = None
        else:
            self.listmax = int(maxval)


    def do_toggle_plots(self, _):
        """
        Set the plots drawing on (the default) or off.
        """
        if self.draw_plots:
            self.draw_plots = False
            print("From now one the plots will not be drawn.")
        else:
            self.draw_plots = True
            print("From now one the plots will be drawn.")


    def do_contexts(self, cmd_args):
        """
        Look for a word, print contexts where it's found along with dates, regions and page numbers.
        Multiple words (lexemes) can be searched if separated by semicolon (;), the order will not
        matter. Optionally you can set <start year>,<end year> (only one is necessary) at the end.
        Region can be also specified as one of the last arguments with /region's_name_with_underscores
        instead of spaces.
        """
        try:
            lexemes, timeframe, region = lexemes_timeframe_region(cmd_args)
        except SpecCharsException as e:
            resp = input(str(e) + ' - do you wish to continue? ("y"+ENTER, or anything else'
                         ' cancels) ')
            if resp == 'y' or resp == 'yes':
                lexemes, timeframe, region = lexemes_timeframe_region(cmd_args, spec_chars_ok=True)
            else:
                return 
        print(f'Looking for {lexemes}.')
        self.clear_output(f'contexts_{cmd_args}')

        query, query_args = docs_query(sqlite, args.edition, args.doc_type, region=region)
        doc_rows = sqlite.execute(query, tuple(query_args))
        doc_row = doc_rows.fetchone()
        drop_counter = 0 # count words to drop if drop_intro
        while doc_row is not None:
            year = int(doc_row[1].partition('-')[0])
            source_region = doc_row[2]
            if not year_ok(year, timeframe):
                doc_row = doc_rows.fetchone()
                continue
            paragraph_rows = sqlite.execute(
                    'SELECT text_interp, pagenum FROM paragraph WHERE doc = ?',
                    (doc_row[0],))
            for par_row in exhaust_paragraph_rows(paragraph_rows):
                page = par_row[1]
                sentence_forms, sentence_terms, _, _ in sentence_as_terms(par_row)
                if args.drop_intro and drop_counter < args.drop_intro:
                    if drop_counter + len(sentence_forms) <= args.drop_intro:
                        drop_counter += len(sentence_forms)
                        continue
                    else:
                        diff = args.drop_intro - drop_counter
                        sentence_forms = sentence_forms[diff:]
                        sentence_terms = sentence_terms[diff:]
                        drop_counter += diff
                for ti, _  in enumerate(sentence_terms):
                    if (ti + len(lexemes) - 1 < len(sentence_terms)
                        and all(compar_function(l, t) for (l, t)
                                 in zip(lexemes, sorted(sentence_terms[ti:ti+len(lexemes)],
                                                        key=str.lower)))):
                        # Print as forms for clarity.
                        self.write_output(f'<{year}:{page}/{source_region}>\n'
                              + ''.join([ # note the spaces are preserved in forms
                                       f'***{f.strip()}*** '
                                       if fi in range(ti, ti+len(lexemes))
                                       else f
                                       for fi, f in enumerate(sentence_forms)
                                       ]),
                              f'contexts_{cmd_args}')
            doc_row = doc_rows.fetchone()


    def do_docs(self, cmd_args):
        """
        List documents (possibly of a given time range or region): their titles, initial fragments,
        lengths, regions and dates. The first argument sets the length of the initial fragment
        in words.
        """
        # The "lexeme" here becomes the number of initial words to display.
        specs, timeframe, region = lexemes_timeframe_region(cmd_args)
        if not len(specs) == 1 or not re.match('^\d+$', specs[0]):
            print(f'{specs} does not set one number of words to display.')
            return
        fragm_length = int(specs[0])
        self.clear_output(f'docs_{cmd_args}')
        query, query_args = docs_query(sqlite, args.edition, args.doc_type, region=region)
        doc_rows = sqlite.execute(query, tuple(query_args))
        doc_row = doc_rows.fetchone()
        counter = 1
        while doc_row is not None:
            year = int(doc_row[1].partition('-')[0])
            if not year_ok(year, timeframe):
                doc_row = doc_rows.fetchone()
                continue
            date = doc_row[1]
            region = doc_row[2]
            title = doc_row[3]
            length = 0
            fragment = ''
            word_counter = 0 # for collecting the initial fragment
            drop_counter = 0 # for dropping words if drop_intro
            paragraph_rows = sqlite.execute(
                    'SELECT text_interp, pagenum FROM paragraph WHERE doc = ?',
                    (doc_row[0],))
            for par_row in exhaust_paragraph_rows(paragraph_rows):
                sentence_forms, _, _, _ in sentence_as_terms(par_row)
                if args.drop_intro and drop_counter < args.drop_intro:
                    if drop_counter + len(sentence_forms) <= args.drop_intro:
                        drop_counter += len(sentence_forms)
                        continue
                    else:
                        diff = args.drop_intro - drop_counter
                        sentence_forms = sentence_forms[diff:]
                        drop_counter += diff
                if word_counter < fragm_length:
                    fragment += ' '.join(sentence_forms[:(fragm_length-word_counter)])
                    word_counter += min(fragm_length-word_counter, len(sentence_forms))
                length += len(sentence_forms)
            self.write_output(f'{counter}. {region}:{date} ({length} ws.) ""{title}"": {fragment}',
                              f'docs_{cmd_args}')
            doc_row = doc_rows.fetchone()
            counter += 1


    def do_forms(self, cmd_args):
        """
        Find typical forms (morphonsyntactic descriptions) appearing in the corpus for the lexeme
        sequence. You can also specify the region and timeframe similarly as with the 'contexts'
        command.
        """
        try:
            specs, timeframe, region = lexemes_timeframe_region(cmd_args)
        except SpecCharsException as e:
            resp = input(str(e) + ' - do you wish to continue? ("y"+ENTER, or anything else'
                         ' cancels) ')
            if resp == 'y' or resp == 'yes':
                lexemes, timeframe, region = lexemes_timeframe_region(cmd_args, spec_chars_ok=True)
            else:
                return
        print(f'Looking for {specs}.')
        self.clear_output(f'forms_{cmd_args}')

        query, query_args = docs_query(sqlite, args.edition, args.doc_type, region=region)
        doc_rows = sqlite.execute(query, tuple(query_args))
        doc_row = doc_rows.fetchone()

        forms_freqs = Counter()
        while doc_row is not None:
            year = int(doc_row[1].partition('-')[0])
            if not year_ok(year, timeframe):
                doc_row = doc_rows.fetchone()
                continue
            paragraph_rows = sqlite.execute('SELECT text_interp, pagenum FROM paragraph WHERE doc = ?',
                    (doc_row[0],))
            drop_counter = 0 # for words if drop_intro
            for par_row in exhaust_paragraph_rows(paragraph_rows):
                sentence_forms, sentence_terms, sentence_interps, _ in sentence_as_terms(par_row)
                if args.drop_intro and drop_counter < args.drop_intro:
                    if drop_counter + len(sentence_forms) <= args.drop_intro:
                        drop_counter += len(sentence_forms)
                        continue
                    else:
                        diff = args.drop_intro - drop_counter
                        sentence_forms = sentence_forms[diff:]
                        sentence_terms = sentence_terms[diff:]
                        sentence_interps = sentence_interps[diff:]
                        drop_counter += diff
                for ti, interp in enumerate(sentence_terms):
                    if (ti + len(specs) - 1 < len(sentence_terms)
                        and all([compar_function(l, t)
                                 for (l, t) in
                                 zip(specs, sorted(sentence_terms[ti:ti+len(specs)],
                                                   key=str.lower))])):
                         forms_freqs.update([' '.join(
                             f'{f}:{t}:{i}' for (f, t, i)
                             in zip(sentence_forms[ti:ti+len(specs)],
                                    sentence_terms[ti:ti+len(specs)],
                                    sentence_interps[ti:ti+len(specs)]))])
            doc_row = doc_rows.fetchone()
        self.write_output(f'Found {forms_freqs.total()} ocurrences in total.',
                              f'forms_{cmd_args}')
        self.write_output('=== FORMS FOUND ===', f'forms_{cmd_args}')
        counter = 1
        for (item, freq) in forms_freqs.most_common(self.listmax):
            self.write_output(f'{counter}. {item} - {freq}', f'forms_{cmd_args}')
            counter += 1


    def do_freq_deviations(self, cmd_args):
        """
        Look for a word or a phrase and find the words that have the highest relative frequency
        compared to the whole implied corpus (can be narrowed down by year, region). Multiple words
        (lexemes) can be searched if separated by semicolon (;), the order will not matter.
        Optionally you can set <start year>,<end year> (only one is necessary) at the end. The
        region can be also specified as one of the last arguments with /region's_name_with_underscores
        instead of spaces.
        """
        window_size = 7
        try:
            lexemes, timeframe, region = lexemes_timeframe_region(cmd_args)
        except SpecCharsException as e:
            resp = input(str(e) + ' - do you wish to continue? ("y"+ENTER, or anything else'
                         ' cancels) ')
            if resp == 'y' or resp == 'yes':
                lexemes, timeframe, region = lexemes_timeframe_region(cmd_args, spec_chars_ok=True)
            else:
                return
        print(f'Analyzing for {lexemes}.')
        self.clear_output(f'freq_deviations_{cmd_args}')

        query, query_args = docs_query(sqlite, args.edition, args.doc_type, region=region)
        doc_rows = sqlite.execute(query, tuple(query_args))
        doc_row = doc_rows.fetchone()
        # The freqwuency counters for both the windows around the target phrase and the whole corpus 
        # being compared to it.
        window_freqs = Counter()
        corpus_freqs = Counter()
        while doc_row is not None:
            year = int(doc_row[1].partition('-')[0])
            if not year_ok(year, timeframe):
                doc_row = doc_rows.fetchone()
                continue
            paragraph_rows = sqlite.execute('SELECT text_interp, pagenum FROM paragraph WHERE doc = ?',
                    (doc_row[0],))
            drop_counter = 0 # for words if drop_intro
            for par_row in exhaust_paragraph_rows(paragraph_rows)
                _, sentence_terms, _, _ in sentence_as_terms(par_row)
                if args.drop_intro and drop_counter < args.drop_intro:
                    if drop_counter + len(sentence_terms) <= args.drop_intro:
                        drop_counter += len(sentence_terms)
                        continue
                    else:
                        diff = args.drop_intro - drop_counter
                        sentence_terms = sentence_terms[diff:]
                        drop_counter += diff
                corpus_freqs.update(sentence_terms)
                # List of pairs delimiting the windows around occurences of the phrase of interest.
                windows = []
                for ti, term in enumerate(sentence_terms):
                    if (ti + len(lexemes) - 1 < len(sentence_terms)
                        and all([compar_function(l, t)
                                 for (l, t) in
                                 zip(lexemes, sorted(sentence_terms[ti:ti+len(lexemes)],
                                                     key=str.lower))])):
                         windows.append((max(0, ti-window_size),
                                         min(len(sentence_terms), ti+len(lexemes)+window_size)))
                window_pnt = 0
                for ti, term in enumerate(sentence_terms):
                    if window_pnt >= len(windows):
                        break
                    if ti >= windows[window_pnt][0] and ti < windows[window_pnt][1]:
                        window_freqs.update([term])
                    elif ti == windows[window_pnt][1]:
                        window_pnt += 1
            doc_row = doc_rows.fetchone()
        # Compute the relative gains of the terms from the windows compared to the general corpus.
        terms_with_relative_freqs = []
        for term, window_freq in window_freqs.most_common():
            if corpus_freqs[term] < 5:
                continue
            terms_with_relative_freqs.append((term,
                                              (window_freq / window_freqs.total()
                                               # mult. the number by a constant to avoid underflow
                                               * (window_freqs.total() % 10))
                                              / corpus_freqs[term]))
        self.write_output('=== LARGEST FREQ GAINS ===', f'freq_deviations_{cmd_args}')
        counter = 1
        for term, relative_freq in sorted(terms_with_relative_freqs,
                                          key=lambda x: x[1], reverse=True)[:self.listmax]:
            counter += 1
            self.write_output(f'{counter}. {term} - {relative_freq}', f'freq_deviations_{cmd_args}')

    def do_regions(self, cmd_args):
        """
        Look for a word, print its observed distribution across source regions. Multiple words
        (lexemes) can be searched if separated by semicolon (;), the order will not matter.
        Optionally you can set <start year>,<end year> (only one is necessary) at the end.

        Do "regions ." with --regex enabled to get the general statistics of word count for each
        region in the database.
        """
        try:
            lexemes, timeframe, region = lexemes_timeframe_region(cmd_args)
        except SpecCharsException as e:
            resp = input(str(e) + ' - do you wish to continue? ("y"+ENTER, or anything else'
                         ' cancels) ')
            if resp == 'y' or resp == 'yes':
                lexemes, timeframe, region = lexemes_timeframe_region(cmd_args, spec_chars_ok=True)
            else:
                return
        if region:
            print('Please do not specify a region for the regions command. Exiting.')
            return
        print(f'Looking for {lexemes}.')
        self.clear_output(f'regions{cmd_args}')

        query, query_args = docs_query(sqlite, args.edition, args.doc_type)
        doc_rows = sqlite.execute(query, tuple(query_args))
        doc_row = doc_rows.fetchone()
        total_regions_freqs = Counter() # for the whole corpus, for reference
        regions_freqs = Counter() # for the places where we found the lexemes
        actual_lexemes = set() # collect the matched lexemes to display if regex is enables
        while doc_row is not None:
            year = int(doc_row[1].partition('-')[0])
            if not year_ok(year, timeframe):
                doc_row = doc_rows.fetchone()
                continue
            region = doc_row[2]
            paragraph_rows = sqlite.execute('SELECT text_interp, pagenum FROM paragraph WHERE doc = ?',
                    (doc_row[0],))
            drop_counter = 0 # for words if drop_intro
            for par_row in exhaust_paragraph_rows(paragraph_rows):
                _, sentence_terms, _, _ in sentence_as_terms(par_row)
                if args.drop_intro and drop_counter < args.drop_intro:
                    if drop_counter + len(sentence_terms) <= args.drop_intro:
                        drop_counter += len(sentence_terms)
                        continue
                    else:
                        diff = args.drop_intro - drop_counter
                        sentence_terms = sentence_terms[diff:]
                        drop_counter += diff
                total_regions_freqs.update({ region: len(' '.join(sentence_terms)) })
                for ti, term in enumerate(sentence_terms):
                    if (ti + len(lexemes) - 1 < len(sentence_terms)
                        and all([compar_function(l, t)
                                 for (l, t) in
                                 zip(lexemes, sorted(sentence_terms[ti:ti+len(lexemes)],
                                                     key=str.lower))])):
                        if args.regex:
                            actual_lexemes.add(' '.join(sentence_terms[ti:ti+len(lexemes)]))
                        regions_freqs.update([region])
            doc_row = doc_rows.fetchone()
        if regions_freqs.total() > 0:
            if args.regex:
                self.write_output('Lexeme forms found: ' + '; '.join(actual_lexemes),
                                  f'regions_{cmd_args}')
            self.write_output('Region, expected ratio, observed ratio, difference, frequency',
                  f'regions_{cmd_args}')
            for (region, total) in total_regions_freqs.most_common(self.listmax):
                self.write_output(region + '\n'
                                  + '{0:.0%} {1:.0%} {2:.0%}'.format(
                                      total / total_regions_freqs.total(),
                                      regions_freqs[region] / regions_freqs.total(),
                                      (regions_freqs[region] / regions_freqs.total()
                                       - total / total_regions_freqs.total()))
                                  + ' ' + str(regions_freqs[region]),
                                  f'regions_{cmd_args}')
        else:
            print('None found.')


    def do_top(self, cmd_years):
        """
        Find and print the most frequent terms for the given year, or a range separated by a comma.
        """
        if ',' in cmd_years:
            parts = cmd_years.split(',')
            timeframe = [int(parts[0]), int(parts[1])]
        else:
            timeframe = [int(cmd_years), int(cmd_years)]
        self.clear_output(f'top_{cmd_years}')
        query, query_args = docs_query(sqlite, args.edition, args.doc_type)

        # pylint: disable=unused-argument
        def count_top(sent_forms, sent_terms, sent_interps, year, pagen, counter):
            counter.update([t for t in sent_terms])

        query, query_args = docs_query(sqlite, args.edition, args.doc_type)
        doc_rows = sqlite.execute(query, tuple(query_args))
        terms_freqs = collect_freqtables(
            sqlite,
            doc_rows,
            timeframe,
            [count_top],
            drop_intro=args.drop_intro
            )[0]

        self.write_output(f'Found {terms_freqs.total()} tokens in total.',
                              f'top_{cmd_years}')
        self.write_output('=== TERMS FOUND ===', f'top_{cmd_years}')
        counter = 1
        for (item, freq) in terms_freqs.most_common(self.listmax):
            self.write_output(f'{counter}. {item} - {freq} - {freq / terms_freqs.total()}',
                              f'top_{cmd_years}')
            counter += 1


    def do_verbs(self, cmd_args):
        """
        Print the verb forms (observed more than one time) appearing near the defined lexeme
        sequence. The window used is four words wide on each side. You can also specify the region
        and timeframe similarly as with the 'contexts' command.
        """
        try:
            lexemes, timeframe, region = lexemes_timeframe_region(cmd_args)
        except SpecCharsException as e:
            resp = input(str(e) + ' - do you wish to continue? ("y"+ENTER, or anything else'
                         ' cancels) ')
            if resp in ['y', 'yes']:
                lexemes, timeframe, region = lexemes_timeframe_region(cmd_args, spec_chars_ok=True)
            else:
                return
        print(f'Looking for {lexemes}.')
        self.clear_output(f'verbs_{cmd_args}')

        query, query_args = docs_query(sqlite, args.edition, args.doc_type, region=region)
        doc_rows = sqlite.execute(query, tuple(query_args))
        # Counting pairs: the interp of the original word, and the interps and lexemes of the
        # context verbs. pylint: disable=unused-argument
        def pair_freqs_fun(sent_forms, sent_terms, sent_interps, year, pagen, counter):
            for ti, term in enumerate(sent_terms):
                if (ti + len(lexemes) - 1 < len(sent_terms)
                    and all(compar_function(l, t)
                             for (l, t) in
                             zip(lexemes, sorted(sent_terms[ti:ti+len(lexemes)],
                                                 key=str.lower)))):
                    interest_area_idxs = (list(range(max(0, ti-4), ti))
                                          + list(range(ti+1,
                                                       min(len(sent_terms), ti+len(lexemes)+4))))
                    for idx in interest_area_idxs:
                        if any(re.search(f'^{pos}:', sent_interps[idx]) for pos in VERB_POS):
                            counter.update([sent_terms[idx] + ':' + sent_interps[idx]])

        pair_freqs = collect_freqtables(
            sqlite,
            doc_rows,
            timeframe,
            [pair_freqs_fun],
            drop_intro=args.drop_intro
            )[0]
        self.write_output('=== VERB ASSOCIATIONS ===', f'verbs_{cmd_args}')
        counter = 1
        for (item, freq) in pair_freqs.most_common(self.listmax):
            self.write_output(f'{counter}. {item} - {freq}', f'verbs_{cmd_args}')
            counter += 1


    def do_ngrams(self, cmd_args):
        """
        Look for a term or sequence of terms, give its most common n-grams and pairings (which
        are n-grams counted with no regard for whether the additional term is before or after).
        Multiple words (lexemes) can be searched if separated by semicolon (;), the order will not
        matter. Optionally you can set <start year>,<end year> (only one is necessary) at the end.
        The region can be also specified as one of the last arguments with
        /region's_name_with_underscores instead of spaces.

        The n-grams are given with their absolute frequency  and the relative ratio of absolute
        n-gram frequency divided by the frequency of the base term (or term sequence).
        """
        try:
            lexemes, timeframe, region = lexemes_timeframe_region(cmd_args)
        except SpecCharsException as e:
            resp = input(str(e) + ' - do you wish to continue? ("y"+ENTER, or anything else'
                         ' cancels) ')
            if resp in ['y', 'yes']:
                lexemes, timeframe, region = lexemes_timeframe_region(cmd_args, spec_chars_ok=True)
            else:
                return
        print(f'Looking for {lexemes}.')
        self.clear_output(f'ngrams_{cmd_args}')

        found_ngrams, pairings, actual_lexemes, hits_count = ngrams(
                sqlite, lexemes, region, timeframe, compar_function,
                equiv=self.equiv, drop_intro=args.drop_intro,
                edition=args.edition, doc_type=args.doc_type)
        self.write_output(f'Found {hits_count} ocurrences in total.', f'ngrams_{cmd_args}')
        if args.regex:
            self.write_output('Lexeme forms found: ' + '; '.join(actual_lexemes),
                                  f'ngrams_{cmd_args}')
        if len(found_ngrams):
            self.write_output('=== Top ngrams ===', f'ngrams_{cmd_args}')
            counter = 1
            for (ngr, freq) in found_ngrams.most_common(self.listmax):
                self.write_output(f'{counter}. {ngr} - {freq} - {freq / hits_count}',
                                  f'ngrams_{cmd_args}')
                counter += 1
        if len(pairings):
            self.write_output('', f'ngrams_{cmd_args}')
            self.write_output('=== Top pairings ===', f'ngrams_{cmd_args}')
            counter = 1
            for (prg, freq) in pairings.most_common(self.listmax):
                self.write_output(f'{counter}. {prg} - {freq} - {freq / hits_count}',
                                  f'ngrams_{cmd_args}')
                counter += 1


    def do_sentence_bigrams(self, cmd_args):
        """
        Look for sentences with a term or sequence of terms, give its most common bigrams and
        pairings (which are bigrams counted with no regard for whether the additional term is
        before or after). The original terms are excluded from the bigrams.  Multiple words (lexemes)
        can be searched if separated by semicolon (;), the order will not matter. Optionally you can
        set <start year>,<end year> (only one is necessary) at the end. The region can be also
        specified as one of the last arguments with /region's_name_with_underscores instead of
        spaces.

        The bigrams are given with their number of sentences and the relative ratio of absolute
        bigram frequency divided by the number of sentences that were matched.
        """
        try:
            lexemes, timeframe, region = lexemes_timeframe_region(cmd_args)
        except SpecCharsException as e:
            resp = input(str(e) + ' - do you wish to continue? ("y"+ENTER, or anything else'
                         ' cancels) ')
            if resp in ['y', 'yes']:
                lexemes, timeframe, region = lexemes_timeframe_region(cmd_args, spec_chars_ok=True)
            else:
                return
        print(f'Looking for {lexemes}.')
        self.clear_output(f'sentence_bigrams_{cmd_args}')
        actual_lexemes = set() # collect the matched lexemes to display if regex is enables
        hits_count = 0

        # pylint: disable=unused-argument
        def ngrams_fun(sent_forms, sent_terms, sent_interps, year, pagen, counter):
            nonlocal hits_count
            sent_selected = False
            for ti, _ in enumerate(sent_terms):
                if (ti + len(lexemes) - 1 < len(sent_terms)
                    and all(compar_function(l, t)
                             for (l, t) in
                             zip(lexemes, sorted(sent_terms[ti:ti+len(lexemes)],
                                                 key=str.lower)))):
                    actual_lexemes.add(' '.join(sent_terms[ti:ti+len(lexemes)]))
                    hits_count += 1
                    sent_selected = True
                    break
            if sent_selected:
                for ti, _ in enumerate(sent_terms[:-1]):
                    if all(not compar_function(lexeme, sent_terms[ti]) and
                           not compar_function(lexeme, sent_terms[ti+1])
                           for lexeme in lexemes):
                        counter.update([' '.join(sent_terms[ti:ti+2])])

        def pairings_fun(sent_forms, sent_terms, sent_interps, year, pagen, counter):
            sent_selected = False
            for ti, _ in enumerate(sent_terms):
                if (ti + len(lexemes) - 1 < len(sent_terms)
                    and all(compar_function(l, t)
                             for (l, t) in
                             zip(lexemes, sorted(sent_terms[ti:ti+len(lexemes)],
                                                 key=str.lower)))):
                    actual_lexemes.add(' '.join(sent_terms[ti:ti+len(lexemes)]))
                    sent_selected = True
                    break
            if sent_selected:
                for ti, _ in enumerate(sent_terms[:-1]):
                    if all(not compar_function(lexeme, sent_terms[ti]) and
                           not compar_function(lexeme, sent_terms[ti+1])
                           for lexeme in lexemes):
                        counter.update([' '.join(sorted(sent_terms[ti:ti+2]))])

        query, query_args = docs_query(sqlite, args.edition, args.doc_type, region=region)
        doc_rows = sqlite.execute(query, tuple(query_args))
        found_ngrams, pairings = tuple(collect_freqtables(
            sqlite,
            doc_rows,
            timeframe,
            [ngrams_fun, pairings_fun],
            drop_intro=args.drop_intro
            ))
        self.write_output(f'Hit {hits_count} sentences in total.', f'sentence_bigrams_{cmd_args}')
        if len(found_ngrams):
            self.write_output('=== Top ngrams ===', f'sentence_bigrams_{cmd_args}')
            counter = 1
            for (ngr, freq) in found_ngrams.most_common(self.listmax):
                self.write_output(f'{counter}. {ngr} - {freq} - {freq / hits_count}',
                                  f'sentence_bigrams_{cmd_args}')
                counter += 1
        if len(pairings):
            self.write_output('', f'sentence_bigrams_{cmd_args}')
            self.write_output('=== Top pairings ===', f'sentence_bigrams_{cmd_args}')
            counter = 1
            for (prg, freq) in pairings.most_common(self.listmax):
                self.write_output(f'{counter}. {prg} - {freq} - {freq / hits_count}',
                                  f'sentence_bigrams_{cmd_args}')
                counter += 1


    def do_years(self, cmd_args):
        """
        Look for a word or a phrase, give its yearly frequencies and relative proportions of the word
        count in the given year (note that for phrases each word is counted). Multiple words
        (lexemes) can be searched if separated by semicolon (;), the order will not matter.
        Optionally you can set <start year>,<end year> (only one is necessary) at the end. The
        region can be also specified as one of the last arguments with
        /region's_name_with_underscores instead of spaces.
        """
        try:
            lexemes, timeframe, region = lexemes_timeframe_region(
                    cmd_args,
                    spec_chars_ok=self.short_years_cmd)
        except SpecCharsException as e:
            resp = input(str(e) + ' - do you wish to continue? ("y"+ENTER, or anything else'
                         ' cancels) ')
            if resp in ['y', 'yes']:
                lexemes, timeframe, region = lexemes_timeframe_region(cmd_args, spec_chars_ok=True)
            else:
                return
        print(f'Looking for {lexemes}.')
        self.clear_output(f'years_{cmd_args}')
        actual_lexemes = set() # collect the matched lexemes to display if regex is enables

        # pylint: disable=unused-argument
        def year_freqs_fun(sent_forms, sent_terms, sent_interps, year, pagen, counter):
            for ti, _ in enumerate(sent_terms):
                # Check if an occurence of the target set of lexemes starts here. The comparison
                # is on a sorted sequence.
                if (ti + len(lexemes) - 1 < len(sent_terms)
                    and all(compar_function(l, t)
                             for (l, t) in
                             zip(lexemes, sorted(sent_terms[ti:ti+len(lexemes)],
                                                 key=str.lower)))):
                    counter.update([year])
                    actual_lexemes.add(' '.join(sent_terms[ti:ti+len(lexemes)]))


        def year_total_freqs_fun(sent_forms, sent_terms, sent_interps, year, pagen, counter):
            counter.update([year] * len(sent_forms))

        query, query_args = docs_query(sqlite, args.edition, args.doc_type, region=region)
        doc_rows = sqlite.execute(query, tuple(query_args))
        year_freqs, year_total_freqs = tuple(collect_freqtables(
            sqlite,
            doc_rows,
            timeframe,
            [year_freqs_fun, year_total_freqs_fun],
            drop_intro=args.drop_intro
            ))
        self.write_output(f'Found {year_freqs.total()} ocurrences in total.',
                              f'years_{cmd_args}')
        if args.regex:
            self.write_output('Lexeme forms found: ' + '; '.join(actual_lexemes),
                                  f'years_{cmd_args}')
        if len(timeframe) > 2:
            for n, point in enumerate(timeframe):
                if n+1 < len(timeframe):
                    period = (point, timeframe[n+1])
                    if n != 1:
                        period = (period[0]+1, period[1])
                    period_len = (period[1] - period[0] + 1)
                    period_avg = (sum(freq  / year_total_freqs[year] for (year, freq)
                                                 in year_freqs.most_common()
                                                 if year_ok(year, period))
                                  / period_len)
                    self.write_output(f'Average freq in {period[0]}-{period[1]}:',
                              f' {period_avg} {period_len} years_{cmd_args}')
        if not self.short_years_cmd:
            for (year, freq) in year_freqs.most_common(self.listmax):
                self.write_output(f'{year} - {freq} / {freq / year_total_freqs[year]}',
                                  f'years_{cmd_args}')
        if self.draw_plots:
            years = sorted(list(year_freqs.keys()))
            rel_freqs = [year_freqs[year] * len(lexemes) / year_total_freqs[year] for year in years]
            freqs = [year_freqs[year] for year in years]
            fig, ax1 = plt.subplots(1, 1)
            fig.suptitle(' '.join(lexemes))
            if args.monochrome:
                ax1.yaxis.set_major_formatter(mtick.PercentFormatter(1.0, decimals=2))
                l1, = ax1.plot(years, rel_freqs, color='tab:grey', label='relative')
            else:
                l1, = ax1.plot(years, freqs, color='tab:grey', label='absolute')
                ax2 = ax1.twinx()
                ax2.yaxis.set_major_formatter(mtick.PercentFormatter(1.0, decimals=2))
                l2, = ax2.plot(years, rel_freqs, color='tab:red', label='relative')
                plt.legend([l1, l2], ['absolute', 'relative'], loc='upper left')
            plt.show()

    def do_exit(self, _):
        return True

if __name__ == '__main__':
    shell = SearchShell()
    if args.file_to_freq:
        shell.draw_plots = False
        shell.short_years_cmd = True
        with open(args.file_to_freq, encoding='utf-8') as inp:
            for line in inp:
                shell.do_years(line)
                print()
        shell.reset_flags()
    shell.cmdloop()
