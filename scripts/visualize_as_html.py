import argparse
import sqlite3

argparser = argparse.ArgumentParser()
argparser.add_argument('edition_title',
        help='the title of the edition to pull from the database (or really a unique part of that'
        ' title)')
argparser.add_argument('output_file', help='the path to the output HTML file')
argparser.add_argument('--spec_type',
        help='for this type of document, it will be colored differently')
argparser.add_argument('--db_path',
        default='tribunicia.db',
        help='the path to the .db file to find the edition in')

args = argparser.parse_args()

# Get the edition paragraphs.
sqlite = sqlite3.connect(args.db_path)
query = 'SELECT text, doc FROM paragraph WHERE edition = ? ORDER BY nextpar'
edition_num_row = sqlite.execute('SELECT rowid FROM edition WHERE title LIKE ?',
        (f'%{args.edition_title}%',)).fetchone()
if edition_num_row is None:
    raise RuntimeError(f'cannot find edition like {args.edition_title} in the database')
paragraph_rows = sqlite.execute(query, (edition_num_row[0],))

# Print the HTML.
doc_rows_by_id = dict()
with open(args.output_file, 'w+') as out:
    print('<html><head></head><body>', file=out)
    row = paragraph_rows.fetchone()
    while row is not None:
        content = row[0]
        if not row[1]: # no doc, meta paragraph
            print(f'<p style="background-color: moccasin; font-family: monospace">{content}</p>',
                    file=out)
        else: # a document paragraph row
            if not row[1] in doc_rows_by_id:
                query = 'SELECT title, type FROM document WHERE rowid = ?'
                doc_row = sqlite.execute(query, (row[1],)).fetchone()
                if doc_row is None:
                    raise RuntimeError(f'cannot find the document for paragraph {row}')
                doc_rows_by_id[row[1]] = doc_row
                print('<hr>', file=out)
            doc_row = doc_rows_by_id[row[1]]
            if doc_row[0] == row[0]: # matching texts
                if args.spec_type and doc_row[1] == args.spec_type:
                    print(f'<h4 style="background-color: DarkKhaki">{content}</h4>', file=out)
                else:
                    print(f'<h4>{content}</h4>', file=out)
            else:
                if args.spec_type and doc_row[1] == args.spec_type:
                    print(f'<p style="background-color: DarkKhaki">{content}</p>', file=out)
                else:
                    print(f'<p>{content}</p>', file=out)
        # Get the next row.
        row = paragraph_rows.fetchone()
    print('</body></html>', file=out)
