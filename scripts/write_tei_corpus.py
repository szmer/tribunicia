import argparse
import os
import sqlite3

from tribunicia.write_tei import write_tei_corpus_nkjp

argparser = argparse.ArgumentParser()
argparser.add_argument('write_path', help='the path (to possibly create) to write the editions\''
        ' TEI subfolders')
argparser.add_argument('--db_path',
        default='tribunicia.db',
        help='the path to the .db file that should be written')
argparser.add_argument('--edition',
        help='the unique part of the edition to pull from the database if you want only one'
        ' edition')

args = argparser.parse_args()
sqlite = sqlite3.connect(args.db_path)

query = 'SELECT rowid, title FROM edition'
if args.edition:
    query += ' WHERE title LIKE ?'
    editions = sqlite.execute(query, (f'%{args.edition}%',))
else:
    editions = sqlite.execute(query)

os.makedirs(args.write_path, exist_ok=True)

region_counts = dict() # source_region -> the next number for an edition from there
for ed in editions:
    ed_title = ed[1]
    ed_region = sqlite.execute('SELECT source_region FROM document WHERE edition = ?'
            , (ed[0],)).fetchone()[0]
    if ed_region not in region_counts:
        region_counts[ed_region] = 1
    write_tei_corpus_nkjp(args.write_path,
            ed_region.replace(' ', '_')+f'-{region_counts[ed_region]}',
            sqlite, ed_title)
    region_counts[ed_region] += 1
