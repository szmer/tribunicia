import argparse
from copy import copy
from collections import Counter
import csv
import json
import logging
import math
import os
import sqlite3

try:
    from cycler import cycler
    import matplotlib.pyplot as plt
    import numpy as np
    from sklearn.linear_model import LinearRegression
except Exception:
    logging.error(' !NOTE! You may need to install the optional dependencies, see the readme.')
    raise

# This is from the user gboffi's answer in Stack Overflow: https://stackoverflow.com/a/38120100
monochrome = (cycler('color', ['k']) * cycler('marker', ['', '.']) *
              cycler('linestyle', ['-', '--', ':']))
plt.rc('axes', prop_cycle=monochrome)

argparser = argparse.ArgumentParser(description='Collect year-by year stats on some Polish lemmas'
                                    'and present them as csv files and temporal plots.')
argparser.add_argument('terms_file',
        help='the file where each line has, space-separated, terms for which a model should be'
        ' computed together')
argparser.add_argument('output_path',
        help='the folder that will be created for the model plots (they will be put there)')
argparser.add_argument('--edition',
        help='the unique part of the edition to pull from the database for plotting the'
        ' regressions.')
argparser.add_argument('--doc_type',
        help='the type of document to plot the regressions for.')
argparser.add_argument('--db_path',
        default='tribunicia.db',
        help='the path to the .db file to find the edition in')
argparser.add_argument('--min_year_freq',
        type=int,
        default=5000,
        help='the minimal total frequency for a year for it to be considered in regressions')
argparser.add_argument('--timeframe',
        help='the comma-separated min and max year to take into account; one of them can be'
        ' omitted')
argparser.add_argument('--group_charts',
        help='create charts for terms groups indicated (one per line) in this file, instead of '
        'charts for every term group')

args = argparser.parse_args()

timeframe = (False, False)
if args.timeframe:
    parts = args.timeframe.split(',')
    if parts[0]:
        timeframe = (int(parts[0]), timeframe[1])
    if parts[1]:
        timeframe = (timeframe[0], int(parts[1]))

# Map the terms into model groups.
terms_to_model = dict()
chart_group_count = 0
with open(args.terms_file, encoding='utf-8') as inp:
    for num, line in enumerate(inp):
        if line[0] == '#':
            continue
        for term in line.split():
            if term:
                if term in terms_to_model:
                    terms_to_model[term].append(num)
                else:
                    terms_to_model[term] = [num]

# Prepare the group charts info, if requested.
terms_chart_groups = dict()
# (this and below tracks how many terms are for each group and how many have already the plot drawn)
group_sizes = dict()
group_counters = dict()
if args.group_charts:
    with open(args.group_charts, encoding='utf-8') as inp:
        for num, line in enumerate(inp):
            chart_group_count += 1
            if line[0] == '#':
                continue
            for term in line.split():
                if term:
                    if num in group_sizes:
                        group_sizes[num] += 1
                    else:
                        group_sizes[num] = 1
                        group_counters[num] = 0
                    if term in terms_chart_groups:
                        terms_chart_groups[term].append(num)
                    else:
                        terms_chart_groups[term] = [num]

# Get the edition paragraphs.
sqlite = sqlite3.connect(args.db_path)
query = 'SELECT rowid, source_date FROM document WHERE source_date IS NOT NULL AND source_date <> ""'
query_args = []
if args.edition:
    edition_num_row = sqlite.execute('SELECT rowid FROM edition WHERE title LIKE ?',
            (f'%{args.edition_title}%',)).fetchone()
    if edition_num_row is None:
        raise RuntimeError(f'cannot find edition like {args.edition_title} in the database')
    query += 'AND edition = ? '
    query_args.append(edition_num_row[0])
if args.doc_type:
    query += 'AND type = ? '
    query_args.append(args.doc_type)
doc_rows = sqlite.execute(query, tuple(query_args))

# Get the term frequencies throught the years.
doc_row = doc_rows.fetchone()
term_freqs = dict() # dict of counters by year
year_total_freqs = dict() # year -> its frequency
while doc_row is not None:
    year = int(doc_row[1].partition('-')[0])
    if timeframe[0] and year < timeframe[0]:
        doc_row = doc_rows.fetchone()
        continue
    if timeframe[1] and year > timeframe[1]:
        doc_row = doc_rows.fetchone()
        continue
    if not year in year_total_freqs:
        year_total_freqs[year] = 0
    paragraph_rows = sqlite.execute('SELECT text_interp FROM paragraph WHERE doc = ?',
            (doc_row[0],))
    par_row = paragraph_rows.fetchone()
    while par_row is not None:
        interp_data = json.loads(par_row[0]) # the interpretation from the pl_morphoparsing module
        for sentence in interp_data:
            for position in sentence:
                #
                # Select the intepretation with the highest disamb likelihood.
                #
                # A simple token.
                if isinstance(position, dict):
                    max_disamb = (-1, -1) # the index of the interpretation, the disamb likelihood
                    for i, interp in enumerate(position['interps']):
                        if interp['disamb'][0] > max_disamb[1]:
                            max_disamb = (i, interp['disamb'][0])
                    chosen_interp = position['interps'][max_disamb[0]]
                    term = chosen_interp['base']
                # A sequence of tokens covering the position.
                elif isinstance(position, list):
                    option_candidates = [] # the list of candidate_tokens lists, see below
                    # First, extract the series of tokens with their likelihoods from each option.
                    for option in position:
                        # The most likely bases/lemmas for each token in the option, along with
                        # their disamb strength (pairs).
                        candidate_tokens = []
                        for token in option:
                            max_token_disamb = (-1, -1)
                            for i, interp in enumerate(token['interps']):
                                if interp['disamb'][0] > max_token_disamb[1]:
                                    max_token_disamb = (i, interp['disamb'][0])
                            chosen_interp = token['interps'][max_token_disamb[0]]
                            candidate_tokens.append((chosen_interp['base'], max_token_disamb[1]))
                        option_candidates.append(candidate_tokens)
                    # Select the strongest option as the term.
                    max_disamb = ('', -1) # the term, the likelihood
                    for option in option_candidates:
                        likelihood = sum([tok[1] for tok in option]) / len(option)
                        # NOTE we only take the first lemma in the option, assuming the others to be 
                        # suffixes.
                        if likelihood > 0.0 and len(option) > 1:
                            logging.info(f'Assuming {option[0][0]} base for {option}')
                        if likelihood > max_disamb[1]:
                            max_disamb = (option[0][0], likelihood)
                    term = max_disamb[0]
                else:
                    raise RuntimeError(f'unexpected type of the position {position}')
                # Finally, add the selected term to the frequency list.
                if term in terms_to_model:
                    if not term in term_freqs:
                        term_freqs[term] = Counter()
                    term_freqs[term].update([year])
                year_total_freqs[year] += 1
        par_row = paragraph_rows.fetchone()
    doc_row = doc_rows.fetchone()

# Create the linear models for the terms against the years. 
model_to_terms = dict()
for term in terms_to_model:
    for model in terms_to_model[term]:
        if not model in model_to_terms:
            model_to_terms[model] = [term]
        else:
            model_to_terms[model].append(term)
os.makedirs(args.output_path)
with open(os.path.join(args.output_path, 'summary.csv'), 'w+') as out:
    w = csv.writer(out)
    w.writerow(('modelled_terms', 'regression coefficient', 'max_spike_year', 'max_spike',
        'min_spike_year', 'min_spike'))
    for model in model_to_terms:
        # Write the csv.
        model_year_freqs = Counter()
        for term in model_to_terms[model]:
            if term in term_freqs:
                model_year_freqs.update(term_freqs[term])
        if len(model_year_freqs) == 0: # model with no terms actually appearing
            continue
        year_freqs = []
        with open(os.path.join(args.output_path, f'{model}_{model_to_terms[model][0]}.csv'),
                'w+') as out2:
            w2 = csv.writer(out2)
            w2.writerow(('year', 'relative freq', 'freq', 'year_total'))
            for year, freq in sorted(model_year_freqs.items(), key=lambda item: item[0]):
                if year_total_freqs[year] > args.min_year_freq:
                    row = [year, freq / year_total_freqs[year], freq, year_total_freqs[year]]
                    csv_row = copy(row)
                    csv_row[1] = '{:.6f}'.format(csv_row[1])
                    w2.writerow(csv_row)
                    year_freqs.append(row)
        # Prepare the predictions for the plot.
        year_freqs = sorted(year_freqs, key=lambda item: item[0])
        datapoints = np.array(year_freqs)
        reg = LinearRegression().fit(
                # convert to a matrix with one feature column
                datapoints[:, 0].reshape(-1, 1),
                datapoints[:, 1] # the relative frequency
                )
        years = [x[0] for x in year_freqs]
        preds = [] # the numbers predicted from the regression model
        max_spike = (-1, -100)
        min_spike = (-1, 100)
        for y in years:
            pred = reg.predict([[y]]).item()
            if model_year_freqs[y] / year_total_freqs[y] - pred > max_spike[1]:
                max_spike = (y, model_year_freqs[y] / year_total_freqs[y] - pred)
            if model_year_freqs[y] / year_total_freqs[y] - pred < min_spike[1]:
                min_spike = (y, model_year_freqs[y] / year_total_freqs[y] - pred)
            preds.append(pred)
        w.writerow(('-'.join(model_to_terms[model]), '{:.7f}'.format(reg.coef_[0]),
            max_spike[0], '{:.7f}'.format(max_spike[1]),
            min_spike[0], '{:.7f}'.format(min_spike[1])))
        # Draw the plot (the normal case) or add it to the proper group plot.
        if not terms_chart_groups:
            plt.figure(model) # (the model identifier is an int)
            plt.plot(years, [x[1] for x in year_freqs])
            plt.plot(years, preds)
            plt.savefig(os.path.join(args.output_path, f'{model}_{model_to_terms[model][0]}.png'))
            plt.close(model)
        else:
            chart_groups = dict() # group number -> the label term
            for term in model_to_terms[model]:
                if term in terms_chart_groups:
                    for group in terms_chart_groups[term]:
                        chart_groups[group] = term
            for group in chart_groups:
                plt.figure(group)
                plt.subplot(100 * math.floor(math.sqrt(group_sizes[group])) +
                            10 * math.ceil(math.sqrt(group_sizes[group])) +
                            group_counters[group] + 1)
                group_counters[group] += 1
                plt.plot(years, [x[1] for x in year_freqs], label=chart_groups[group])
                plt.legend(loc='best')
if terms_chart_groups:
    for i in range(chart_group_count):
        plt.figure(i)
        plt.subplots_adjust(hspace=0.3, wspace=0.8)
        plt.savefig(os.path.join(args.output_path, f'group_{i}.png'))
        plt.close(i)

# Write some csv stats.
with open(os.path.join(args.output_path, 'yearstat.csv'), 'w+', encoding='utf-8') as out:
    w = csv.writer(out)
    w.writerow(('year', 'freq'))
    for year, freq in year_total_freqs.items():
        w.writerow((year, freq))
