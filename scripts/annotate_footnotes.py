"Find footnote references and collect a semi-automatic annotation for them."
import argparse
from collections import Counter
import csv
from itertools import chain
from math import ceil
import pathlib
import sqlite3

import urwid
from tribunicia.enrich_with_names import (
        BASE_PERSON_LEXEMES,
        AFFIX_PERSON_LEXEMES,
        extract_refs_from_pars,
        person_occurrence_func,
        person_phrase_pattern,
        reverse_index,
)
from tribunicia.lib_morphoparsing import sentences_as_terms

argparser = argparse.ArgumentParser()
argparser.add_argument('edition_title',
        help='the title of the edition to pull from the database (or really a unique part of that'
        ' title)')
argparser.add_argument('--document_type',
        help='types of documents to search in for notes')
argparser.add_argument('--db_path',
        default='tribunicia.db',
        help='the path to the .db file to find the edition in')
argparser.add_argument('--offices_path',
        default='offices.csv',
        help='the path to the CSV database with office names')
argparser.add_argument('--annot_path',
        default='reference_notes.csv',
        help='the path to use for loading and saving the reference notes annotations')

args = argparser.parse_args()
REF_INPUT_INSTRUCTION = ('(press enter to input note, n to change page, '
                         'r to go between references, s to save, q to quit)')
TXT_WIDTH = 115

# Get the edition paragraphs.
sqlite = sqlite3.connect(args.db_path)
query = ('SELECT rowid, pagenum, text, doc, text_interp FROM paragraph'
         ' WHERE edition = ? ORDER BY rowid')
edition_row = sqlite.execute('SELECT rowid, title FROM edition WHERE title LIKE ?',
        (f'%{args.edition_title}%',)).fetchone()
if edition_row is None:
    raise RuntimeError(f'cannot find edition like {args.edition_title} in the database')
paragraph_rows = sqlite.execute(query, (edition_row[0],))
edition_name = edition_row[1]

# Setup the person phrase detection.
OFFICE_NAMES = dict()
with open(args.offices_path, encoding='utf-8') as csv_inp:
    rd = csv.reader(csv_inp)
    next(rd, None) # skip the header
    for row in rd:
        OFFICE_NAMES[row[0]] = [row[0]]
REV_OFFICE_NAMES = reverse_index(OFFICE_NAMES)
unified_persons_pattern = person_phrase_pattern(chain.from_iterable(OFFICE_NAMES.values()))
unified_persons_lexemes = dict(BASE_PERSON_LEXEMES, **AFFIX_PERSON_LEXEMES)
rev_person_lexemes = reverse_index(unified_persons_lexemes)
PERSON_PHRASE_FUNC, PERSON_OCCURRENCES = person_occurrence_func(
        unified_persons_pattern, rev_person_lexemes, list(REV_OFFICE_NAMES.keys()), edition_name)
PERSONS_COUNTER = Counter()

# Load saved reference annots if present.
SAVED_ANNOTS = {}
if pathlib.Path(args.annot_path).is_file():
    with open(args.annot_path, encoding='utf-8') as f:
        rd = csv.reader(f)
        header_line = True
        for line in rd:
            if header_line:
                header_line = False
                continue
            ref_rowid, ref_offset, ref_fragment, ref_note = line
            ref_rowid, ref_offset = int(ref_rowid), int(ref_offset)
            if ref_rowid not in SAVED_ANNOTS:
                SAVED_ANNOTS[ref_rowid] = {}
            SAVED_ANNOTS[ref_rowid][ref_offset] = (ref_note, ref_fragment)

class NoteEditBox(urwid.Filler):
    "The widget for holding the note edit field."

    def keypress(self, size, key: str) -> str | None:
        "Handle keypresses - captures enter and saves the current note."
        global ui
        if not ui.accept_note_text: # pass it to global
            return key
        if key != "enter":
            # (entering the input text)
            return super().keypress(size, key)
        ui.save_note(self.body.edit_text)
        ui.accept_note_text = False
        self.body.set_caption(REF_INPUT_INSTRUCTION)
        self.body.set_edit_text('')
        return None

class UILayout():
    "The Urwid UI layout."

    def __init__(self, pars_query):
        self.page_txt = urwid.Pile([urwid.Text('')])
        self.page_num = urwid.Text('?')
        page_num_padded = urwid.Padding(self.page_num, align='center', width=10)
        page_txt_filler = urwid.Filler(self.page_txt)
        page_txt_padded = urwid.Padding(page_txt_filler, align='center', width=TXT_WIDTH)
        top_div = urwid.Divider('+', top=1, bottom=4)
        middle_div = urwid.Divider('+')
        self.current_note_disp = urwid.Text('Current note: -')
        self.note_edit = NoteEditBox(urwid.Edit(REF_INPUT_INSTRUCTION))
        pile_contents = [top_div, page_num_padded, page_txt_padded, middle_div,
                                self.current_note_disp, self.note_edit]
        self.pile = urwid.Pile(pile_contents)
        self.top = urwid.ListBox([self.pile])
        # Put self.note_edit in focus.
        self.top.set_focus_path([0, len(pile_contents)-1])

        self.pars_query = pars_query
        self.current_page_n = 0
        self.current_page_pars = []
        self.current_page_struct = []
        self.current_page_refs = []
        self.current_page_ref_n = 0
        self.awaiting_paragraph = False

        self.ref_notes = SAVED_ANNOTS or {} # row_id -> offset -> manually entered text note
        self.accept_note_text = False

    def display_page_and_hilite_ref(self, ref_n):
        "Display the current page, highlight the indicated reference if possible."
        wgs = []
        for par in self.current_page_pars:
            if (ref_n < len(self.current_page_refs)
                and self.current_page_refs[ref_n][0] == par['rowid']):
                ref_marker = self.current_page_refs[ref_n][1]
                length = len(ref_marker)
                offset = self.current_page_refs[ref_n][2]
                wgs += [urwid.Text([par['text'][:offset],
                        ('hilite', ' >>' + ref_marker + '<< '),
                        par['text'][offset+length:]])]
            else:
                wgs.append(urwid.Text(par['text']))
            wgs.append(urwid.Text('\n'))
        self.page_txt.widget_list = wgs
        self.update_current_note_display()

    def next_page(self):
        "Collect and display the next page."
        par_row = paragraph_rows.fetchone()
        self.current_page_n = par_row[1]
        if self.awaiting_paragraph:
            self.current_page_pars = [self.awaiting_paragraph]
        else:
            self.current_page_pars = []
        while par_row is not None:
            page_n = par_row[1]
            if page_n != self.current_page_n:
                if self.current_page_pars:
                    # References.
                    extracted_refs = extract_refs_from_pars(self.current_page_pars)
                    # Person occurrences are also treated as references.
                    person_occurrences_as_refs = []
                    for par_info in self.current_page_pars:
                        if par_info['interp'] is None:
                            continue
                        par_rowid = par_info['rowid']
                        sents_accum_offset = 0
                        for sent_forms, sent_terms, sent_interps, sent_offsets in sentences_as_terms(
                                [par_info['interp']], no_merging=True):
                            del PERSON_OCCURRENCES[:] # clear them
                            PERSON_PHRASE_FUNC(sent_forms, sent_terms, sent_interps, sent_offsets,
                                               -1, page_n, PERSONS_COUNTER)
                            for occ in PERSON_OCCURRENCES:
                                person_occurrences_as_refs.append(
                                        (par_rowid,
                                         ' '.join(occ.forms),
                                         occ.offset_in_par + sents_accum_offset))
                            if sent_offsets:
                                sents_accum_offset += sent_offsets[-1] + len(sent_forms[-1]) + 2
                    # Use them both, sorting by offset in the rowid.
                    self.current_page_refs = sorted(extracted_refs + person_occurrences_as_refs,
                                                    key=lambda r: r[0] * 1e6 + r[2])
                    self.page_num.set_text(str(self.current_page_n)+'\n')
                    self.current_page_ref_n = 0
                    self.display_page_and_hilite_ref(0)
                    self.update_current_note_display()
                self.awaiting_paragraph = {
                    'rowid': par_row[0],
                    'text': par_row[2],
                    'doc': par_row[3],
                    'interp': par_row[4]
                }
                return

            self.current_page_pars.append({
                'rowid': par_row[0],
                'text': par_row[2],
                'doc': par_row[3],
                'interp': par_row[4]
            })
            par_row = paragraph_rows.fetchone()

    def save_note(self, note):
        """Save the note that was put in, or remove when note is empty."""
        note = note.strip()
        ref = self.current_page_refs[self.current_page_ref_n]
        rowid = ref[0]
        offset = ref[2]
        # The fragment is for control later if the paragraph is the same as originally intended.
        fragment = [par for par in self.current_page_pars
                    if par['rowid'] == rowid][0]['text'][offset:offset+15]
        if rowid not in self.ref_notes:
            self.ref_notes[rowid] = {}
        if note:
            self.ref_notes[rowid][offset] = (note, fragment)
        elif offset in self.ref_notes[rowid]:
            del self.ref_notes[rowid][offset]
        self.update_current_note_display()

    def save_annotations_to_file(self):
        "Save collected reference note annotations to the defined file path."
        if self.ref_notes: # don't overwrite if an empty annotation
            out_dicts = []
            for rowid, notes in self.ref_notes.items():
                for offset, (note, fragment) in notes.items():
                    out_dicts.append({'rowid': rowid, 'offset': offset,
                                      'fragment': fragment, 'note': note})
            with open(args.annot_path, 'w+', encoding='utf-8') as out:
                columns = ['rowid', 'offset', 'fragment', 'note']
                wt = csv.DictWriter(out, columns)
                wt.writeheader()
                for elem in out_dicts:
                    wt.writerow(elem)

    def update_current_note_display(self):
        "Update the display of the current note for the current ref in the UI."
        if self.current_page_ref_n >= len(self.current_page_refs):
            self.current_note_disp.set_text('')
            return
        ref = self.current_page_refs[self.current_page_ref_n]
        rowid = ref[0]
        offset = ref[2]
        ident = f'Current note ({self.current_page_ref_n+1} out of {len(self.current_page_refs)})'
        if rowid not in self.ref_notes:
            self.current_note_disp.set_text(f'{ident}: -')
            return
        if offset not in self.ref_notes[rowid]:
            self.current_note_disp.set_text(f'{ident}: -')
            return
        # The tuple is (note, fragment).
        self.current_note_disp.set_text(f'{ident}: {self.ref_notes[rowid][offset][0]}')

ui = UILayout(paragraph_rows)
ui.next_page()

def key_commands(key: str) -> None:
    "Handle key commands."
    global ui
    if key in {'q', 'Q'}:
        raise urwid.ExitMainLoop()
    if key in {'n', 'N'}:
        ui.next_page()
    if key in {'s', 'S'}:
        ui.save_annotations_to_file()
    elif key in {'r', 'R'}:
        if ui.current_page_ref_n + 1 < len(ui.current_page_refs):
            ui.current_page_ref_n += 1
        else:
            ui.current_page_ref_n = 0
        ui.display_page_and_hilite_ref(ui.current_page_ref_n)
    elif key == 'enter':
        if ui.current_page_ref_n < len(ui.current_page_refs):
            ui.note_edit.body.set_caption('Enter reference text: ')
            ui.accept_note_text = True

pallette = [('hilite', 'yellow', 'dark cyan')]
loop = urwid.MainLoop(ui.top, pallette, unhandled_input=key_commands)
loop.run()
