"Draw total year plots and print some numbers describing the whole corpus."
import argparse
from collections import Counter
import json
import logging
import re
import sqlite3

from tribunicia.lib_morphoparsing import disamb_position
from lib_chrono_explorer import year_ok

try:
    import matplotlib.pyplot as plt
except Exception:
    logging.error(' !NOTE! You may need to install the optional dependencies, see the readme.')
    raise

argparser = argparse.ArgumentParser()
argparser.add_argument('--db_path',
        default='tribunicia.db',
        help='the path to the .db file to find the edition in')
argparser.add_argument('--edition',
        help='the edition from which to read documents')
argparser.add_argument('--region',
        help='the region for documents')
argparser.add_argument('--doc_type',
        default='sejmik resolution',
        help='the type of document to take into account')
argparser.add_argument('--timeframe',
        help='the first and/or last year to consider, with a comma in between')
argparser.add_argument('--monochrome',
        action='store_true',
        default=False,
        help='only draw the relative frequency on plots, so they\'re monochromatic.')
args = argparser.parse_args()

timeframe = (False, False)
if args.timeframe:
    timeframe = args.timeframe.split(',')
    if timeframe[0]:
        timeframe[0] = int(timeframe[0])
    else:
        timeframe[0] = False
    if timeframe[1]:
        timeframe[1] = int(timeframe[1])
    else:
        timeframe[1] = False

sqlite = sqlite3.connect(args.db_path)

query = ('SELECT rowid, source_date, source_region FROM document WHERE source_date IS NOT NULL '
         ' AND source_date <> "" ')
query_args = []
if args.edition:
    edition_num_row = sqlite.execute('SELECT rowid FROM edition WHERE title LIKE ?',
            (f'%{args.edition_title}%',)).fetchone()
    if edition_num_row is None:
        raise RuntimeError(f'cannot find edition like {args.edition_title} in the database')
    query += 'AND edition = ? '
    query_args.append(edition_num_row[0])
if args.doc_type:
    query += 'AND type = ? '
    query_args.append(args.doc_type)
if args.region:
    query += 'AND source_region LIKE ? '
    query_args.append(f'%{args.region}%')
query += 'ORDER BY source_date '

doc_rows = sqlite.execute(query, tuple(query_args))
doc_row = doc_rows.fetchone()
year_doc_counts = Counter()
year_word_counts = dict()
region_word_counts = dict()
region_doc_counts = dict()
while doc_row is not None:
    year = int(doc_row[1].partition('-')[0])
    region = doc_row[-1]
    if not year_ok(year, timeframe):
        doc_row = doc_rows.fetchone()
        continue
    if year not in year_word_counts:
        year_word_counts[year] = 0
    year_doc_counts.update([year])

    if region not in region_word_counts:
        region_word_counts[region] = 0
        region_doc_counts[region] = 0
    region_doc_counts[region] += 1

    paragraph_rows = sqlite.execute('SELECT text_interp, pagenum FROM paragraph WHERE doc = ?',
            (doc_row[0],))
    par_row = paragraph_rows.fetchone()
    while par_row is not None:
        interp_data = json.loads(par_row[0]) # the interp from the pl_morphoparsing module
        for sentence in interp_data:
            interpreted_sent = [disamb_position(pos) for pos in sentence]
            interpreted_sent = [t for (f, t, i) in interpreted_sent if re.search('\\w+', t)]
            year_word_counts[year] += len(interpreted_sent)
            region_word_counts[region] += len(interpreted_sent)
        par_row = paragraph_rows.fetchone()
    doc_row = doc_rows.fetchone()

print(f'Total token count: {sum(year_word_counts.values())}')
print(f'Total document count: {year_doc_counts.total()}')
print('== REGIONS (words, docs) ==')
for region, reg_words in region_word_counts.items():
    print(f'{region} -- {reg_words} - {region_doc_counts[region]}')

fig, ax1 = plt.subplots(1, 1)
fig.suptitle('Word counts by year')
words_data_points = sorted(list(year_word_counts.items()),
                     key=lambda x: x[0])
plt.plot([item[0] for item in words_data_points],
         [item[1] for item in words_data_points],
         color='tab:grey' if args.monochrome else 'tab:red',
         label='words')
#plt.vlines([1610, 1650], 0, 70000, colors=['grey', 'grey'])
plt.legend(loc='upper center')
plt.show()

fig = plt.figure(2)
fig.suptitle('Document counts by year')
docs_data_points = sorted(list(year_doc_counts.most_common()),
                     key=lambda x: x[0])
plt.plot([item[0] for item in docs_data_points],
         [item[1] for item in docs_data_points],
         color='tab:grey' if args.monochrome else 'tab:blue',
         label='documents')
plt.show()

fig = plt.figure(3)
fig.suptitle('Average document length by year')
plt.plot([item[0] for item in docs_data_points],
         [words_data_points[i][1] / item[1] for (i, item) in enumerate(docs_data_points)],
         color='tab:grey' if args.monochrome else 'tab:red', label='length')
plt.legend(loc='upper right')
plt.show()
