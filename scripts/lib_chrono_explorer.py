"Things used by chrono explorer, also exposed to other script modules."
from collections import Counter
import json
import re

from tribunicia.lib_morphoparsing import sentence_as_terms
from tribunicia.utils import exhaust_paragraph_rows

class SpecCharsException(ValueError):
    "Exception when CLI command args contain unexpected chars."

def docs_query(sqlite, edition=False, doc_type=False, region=False):
    """
    Prepare a query for getting document objects from the database, optionally for a specific
    region.
    """
    query = ('SELECT rowid, source_date, source_region, title FROM document'
             ' WHERE source_date IS NOT NULL AND source_date <> "" ')
    query_args = []
    if edition:
        edition_num_row = sqlite.execute('SELECT rowid FROM edition WHERE title LIKE ?',
                (f'%{edition}%',)).fetchone()
        if edition_num_row is None:
            raise RuntimeError(f'cannot find edition like {edition} in the database')
        query += 'AND edition = ? '
        query_args.append(edition_num_row[0])
    if doc_type:
        query += 'AND type = ? '
        query_args.append(doc_type)
    if region:
        query += 'AND source_region = ? '
        query_args.append(region)
    query += 'ORDER BY source_date '
    return query, query_args

def read_equivs_dict_from_db(sqlite):
    "Return a dict of equivalences (equivalent word -> pattern to recognize its forms) saved in db."
    sqlite.execute('CREATE TABLE IF NOT EXISTS tribunicia_settings ('
                   'setting_key TEXT UNIQUE,'
                   'setting_value TEXT)')
    equivs_dict_txt = sqlite.execute('SELECT setting_value FROM tribunicia_settings '
                                 'WHERE setting_key = "equivs"').fetchone()
    if equivs_dict_txt is None:
        equivs_dict = dict()
        sqlite.execute('INSERT INTO tribunicia_settings (setting_key, setting_value) '
                       'VALUES ("equivs", "{}")')
        sqlite.commit()
    else:
        equivs_dict = json.loads(equivs_dict_txt[0])
        for equiv in equivs_dict.keys():
            equivs_dict[equiv] = re.compile(equivs_dict[equiv])
    return equivs_dict

def lexemes_timeframe_region(cmd_args, spec_chars_ok=False):
    """
    This extracts the user-requested lexeme, timeframe and region information from the cmd_args
    string to Pythonic objects, as the return values. Unless spec_chars_ok, it wil raise a
    SpecCharsException if there are commas or semicolons in extracted lexemes.
    """
    lexemes = cmd_args.strip()
    region = False
    timeframe = (False, False)
    if len(lexemes.split()) > 1:
        cmd_args = lexemes.split()
        for idx in [-1] if len(cmd_args) == 2 else [-1, -2]:
            scope_arg_maybe = cmd_args[idx]
            if (re.match('^([0-9]{4})?,([0-9]{4},?)*$', scope_arg_maybe)
                    and len(scope_arg_maybe) >= 5):
                lexemes = ' '.join(cmd_args[:idx])
                parts = scope_arg_maybe.split(',')
                if parts:
                    timeframe = (int(parts[0]), timeframe[1])
                if len(parts) > 1:
                    timeframe = (timeframe[0], int(parts[-1]))
                if len(parts) > 2:
                    timeframe = [int(p) for p in parts]
                print(f'The extracted timeframe is {timeframe}.')
            elif scope_arg_maybe[0] == '/':
                lexemes = ' '.join(cmd_args[:idx])
                region = scope_arg_maybe[1:].replace('_', ' ')
            else:
                break
    lexemes = sorted([l.strip() for l in lexemes.split(';')],
                     # sort by the letter characters inside the specification
                     key=lambda l: re.sub('[^\\w]+', '', l).lower())
    if not spec_chars_ok:
        for lex in lexemes:
            if ',' in lex or ';' in lex:
                raise SpecCharsException(f'A special character encountered in {lex}')
    return lexemes, timeframe, region

def year_ok(year, timeframe):
    "Is the year in the timeframe?"
    if timeframe[0] and year < timeframe[0]:
        return False
    if timeframe[-1] and year > timeframe[-1]:
        return False
    return True

def collect_freqtables(sqlite, doc_rows, timeframe, funcs_to_collect,
                       drop_intro=False):
    """
    Go through docs from doc_rows in the timeframe. Funcs_to_collect should be functions that get
    (forms, paragraph, interps, year, page number, counter) and update the counter if needed. The
    counters for all functions are returned in a list.
    """
    counters = []
    for _ in funcs_to_collect:
        counters.append(Counter())
    doc_row = doc_rows.fetchone()
    while doc_row is not None:
        year = int(doc_row[1].partition('-')[0])
        if not year_ok(year, timeframe):
            doc_row = doc_rows.fetchone()
            continue
        paragraph_rows = sqlite.execute('SELECT text_interp, pagenum FROM paragraph WHERE doc = ?',
                (doc_row[0],))
        drop_counter = 0 # count words if drop_intro
        for par_row in exhaust_paragraph_rows(paragraph_rows):
            pagen = par_row[1]
            sent_forms, sent_terms, sent_interps, sent_offsets in sentence_as_terms(par_row)
            if drop_intro and drop_counter < drop_intro:
                if drop_counter + len(sent_terms) <= drop_intro:
                    drop_counter += len(sent_terms)
                    continue
                else:
                    diff = drop_intro - drop_counter
                    sent_terms = sent_terms[diff:]
                    drop_counter += diff
            for fi, fun in enumerate(funcs_to_collect):
                fun(sent_forms, sent_terms, sent_interps, sent_offsets, year, pagen, counters[fi])
        doc_row = doc_rows.fetchone()
    return counters

def ngrams(sqlite, lexemes, region, timeframe, compar_function,
           equiv=lambda x: x, drop_intro=False, edition='', doc_type=''):
    "Calculate ngrams and other data, surrounding the lexemes one word in each direction."
    actual_lexemes = set() # collect the matched lexemes to display if regex is enables
    hits_count = 0

    # pylint: disable=unused-argument
    def ngrams_fun(sent_forms, sent_terms, sent_interps, sent_offsets, year, pagen, counter):
        nonlocal hits_count
        for ti, _ in enumerate(sent_terms):
            if (ti + len(lexemes) - 1 < len(sent_terms)
                and all(compar_function(l, t)
                         for (l, t) in
                         zip(lexemes, sorted(sent_terms[ti:ti+len(lexemes)],
                                             key=str.lower)))):
                hits_count += 1
                actual_lexemes.add(' '.join(sent_terms[ti:ti+len(lexemes)]))
                if ti > 0:
                    # The n-gram with the previous term.
                    counter.update([' '.join(map(equiv,
                                                 sent_terms[ti-1:ti+len(lexemes)]))])
                if ti + len(lexemes) < len(sent_terms):
                    counter.update([' '.join(map(equiv,
                                                 sent_terms[ti:ti+len(lexemes)+1]))])

    def pairings_fun(sent_forms, sent_terms, sent_interps, sent_offsets, year, pagen, counter):
        for ti, _ in enumerate(sent_terms):
            if (ti + len(lexemes) - 1 < len(sent_terms)
                and all(compar_function(l, t)
                         for (l, t) in
                         zip(lexemes, sorted(sent_terms[ti:ti+len(lexemes)],
                                             key=str.lower)))):
                if ti > 0:
                    # The pairing with the previous term, which should be excluded from the
                    # sorting.
                    counter.update([' '.join([equiv(sent_terms[ti-1])]
                                             + list(
                                                 sorted(
                                                     map(equiv,
                                                         sent_terms[ti:ti+len(lexemes)]))))])
                if ti + len(lexemes) < len(sent_terms):
                    counter.update([' '.join([equiv(sent_terms[ti+len(lexemes)])]
                                             + list(
                                                 sorted(
                                                     map(equiv,
                                                         sent_terms[ti:ti+len(lexemes)]))))])

    query, query_args = docs_query(sqlite, edition, doc_type, region=region)
    doc_rows = sqlite.execute(query, tuple(query_args))
    found_ngrams, pairings = tuple(collect_freqtables( # pylint: disable=unbalanced-tuple-unpacking
        sqlite,
        doc_rows,
        timeframe,
        [ngrams_fun, pairings_fun],
        drop_intro=drop_intro))
    return found_ngrams, pairings, actual_lexemes, hits_count

def sentence_bigrams(sqlite, lexemes, region, timeframe, compar_function,
                     equiv=lambda x: x, drop_intro=False, edition='', doc_type=''):
    "Calculate ngrams in sentence and other data, for sentences containing the lexemes."
    actual_lexemes = set() # collect the matched lexemes to display if regex is enables
    hits_count = 0

    # pylint: disable=unused-argument
    def ngrams_fun(sent_forms, sent_terms, sent_interps, sent_offsets, year, pagen, counter):
        nonlocal hits_count
        sent_selected = False
        for ti, _ in enumerate(sent_terms):
            if (ti + len(lexemes) - 1 < len(sent_terms)
                and all(compar_function(l, t)
                         for (l, t) in
                         zip(lexemes, sorted(sent_terms[ti:ti+len(lexemes)],
                                             key=str.lower)))):
                actual_lexemes.add(' '.join(sent_terms[ti:ti+len(lexemes)]))
                hits_count += 1
                sent_selected = True
                break
        if sent_selected:
            sent_ngrams = set()
            for ti, _ in enumerate(sent_terms[:-1]):
                if all(not compar_function(lexeme, sent_terms[ti]) and
                       not compar_function(lexeme, sent_terms[ti+1])
                       for lexeme in lexemes):
                    sent_ngrams.add(' '.join(map(equiv, sent_terms[ti:ti+2])))
            counter.update(list(sent_ngrams))

    def pairings_fun(sent_forms, sent_terms, sent_interps, sent_offsets, year, pagen, counter):
        sent_selected = False
        for ti, _ in enumerate(sent_terms):
            if (ti + len(lexemes) < len(sent_terms)
                and all(compar_function(l, t)
                         for (l, t) in
                         zip(lexemes, sorted(sent_terms[ti:ti+len(lexemes)],
                                             key=str.lower)))):
                actual_lexemes.add(' '.join(sent_terms[ti:ti+len(lexemes)]))
                sent_selected = True
                break
        if sent_selected:
            sent_ngrams = set()
            for ti, _ in enumerate(sent_terms[:-1]):
                if all(not compar_function(lexeme, sent_terms[ti]) and
                       not compar_function(lexeme, sent_terms[ti+1])
                       for lexeme in lexemes):
                    sent_ngrams.add(' '.join(sorted(map(equiv, sent_terms[ti:ti+2]))))
            counter.update(list(sent_ngrams))

    query, query_args = docs_query(sqlite, edition, doc_type, region=region)
    doc_rows = sqlite.execute(query, tuple(query_args))
    found_ngrams, pairings = tuple(collect_freqtables( # pylint: disable=unbalanced-tuple-unpacking
        sqlite,
        doc_rows,
        timeframe,
        [ngrams_fun, pairings_fun],
        drop_intro=drop_intro,
        ))
    return found_ngrams, pairings, actual_lexemes, hits_count

def sentence_co_occurrences(sqlite, lexemes, region, timeframe, compar_function,
                     equiv=lambda x: x, edition='', doc_type='',
                     include_pos=None, include_lexemes=None):
    "Get co-occurrences of specified POS and lexemes in sentences where the pattern is found."
    hits_count = 0
    if include_pos is None:
        include_pos = set()
    if include_lexemes is None:
        include_lexemes = set()

    # pylint: disable=unused-argument
    def co_ocs_fun(sent_forms, sent_terms, sent_interps, sent_offsets, year, pagen, counter):
        nonlocal hits_count
        sent_selected = False
        for ti, _ in enumerate(sent_terms):
            if (ti + len(lexemes) - 1 < len(sent_terms)
                and all(compar_function(l, t)
                         for (l, t) in
                         zip(lexemes, sorted(sent_terms[ti:ti+len(lexemes)],
                                             key=str.lower)))):
                hits_count += 1
                sent_selected = True
                break
        if sent_selected:
            sent_co_ocs = set()
            for ti, _ in enumerate(sent_terms[:-1]):
                if (all(not compar_function(lexeme, sent_terms[ti])
                       for lexeme in lexemes)
                    and (sent_terms[ti] in include_lexemes
                         # ctag (POS) starts the morphosyntactic descriptions (interps)
                         or any(sent_interps[ti].startswith(pos+':')
                                                            for pos in include_pos))):
                    sent_co_ocs.add(equiv(sent_terms[ti]))
            counter.update(list(sent_co_ocs))

    query, query_args = docs_query(sqlite, edition, doc_type, region=region)
    doc_rows = sqlite.execute(query, tuple(query_args))
    found_co_ocs = tuple(collect_freqtables( # pylint: disable=unbalanced-tuple-unpacking
        sqlite,
        doc_rows,
        timeframe,
        [co_ocs_fun],
        ))[0]
    return found_co_ocs, hits_count
