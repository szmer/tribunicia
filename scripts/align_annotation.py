import argparse
import csv
import logging
import re
import sqlite3
import yaml

from tribunicia.structure_finder import StructureFinder
from tribunicia.text_utils import join_linebreaks

logging.getLogger().setLevel(logging.WARNING)

argparser = argparse.ArgumentParser(description='Use an older annotation file with titles and'
        ' authors to fill `source_entity` fields for documents from an edition.')
argparser.add_argument('annotation_file', help='A file with document titles in sequence mapped to'
        ' true document authors.')
argparser.add_argument('edition_file', help='The YAML file containing the OCR correction rules'
        ' and edition title.')
argparser.add_argument('--db_path',
        default='tribunicia.db',
        help='the path to the .db file to find the edition in')
argparser.add_argument('--expected', help='The YAML file containing the OCR correction rules.')
argparser.add_argument('--author_assertion', help=
        'x,y: the y author expected if doc of x type; deviations are shown and counted.'
        ' Violating annotations will not be written.')
argparser.add_argument('-y', action='store_true',
        help='Whether to write the authors from the annotation without asking.')

args = argparser.parse_args()

with open(args.edition_file) as f:
    desc_yaml = yaml.safe_load(f)
sf = StructureFinder(desc_yaml['text_structure'])

doc_titles = [] # title, author pairs
with open(args.annotation_file) as f:
    r = csv.reader(f)
    doc_titles = []
    for row in r:
        row[0] = re.sub('[\\s\n]+', ' ', join_linebreaks(sf.ocr_corrected(row[0])))
        doc_titles.append(row)
head = 0 # the next title index to match
final_title_place = -1
final_title_ratio = -1 # where the last title to match fell (in proportion to whole edition)
new_doc_count = 0

author_assertion = False
if args.author_assertion:
    author_assertion = args.author_assertion.split(',')
    assert len(author_assertion) == 2

sqlite = sqlite3.connect(args.db_path)
query = 'SELECT rowid, title, type, source_entity FROM document WHERE edition = ? ORDER BY rowid'
edition_num_row = sqlite.execute('SELECT rowid FROM edition WHERE title = ?',
        (desc_yaml['edition_metadata']['edition_title'],)).fetchone()
if edition_num_row is None:
    raise RuntimeError(f"cannot find edition like {desc_yaml['edition_metadata']['edition_title']}"
                       " in the database")
doc_rows = sqlite.execute(query, (edition_num_row[0],))
docid_author = dict() # doc id -> its author
par_num = 0
current_author = False
assertion_broken_count = 0
for doc_n, doc_row in enumerate(doc_rows):
    new_doc_count += 1
    assertion_broken = False
    pars = sqlite.execute('SELECT text FROM paragraph WHERE doc = ? ORDER BY nextpar',
            (doc_row[0],)).fetchall()
    author_fragm_lengths = dict() # possible author -> length of paragraphs supporting
    for par in pars:
        par_num += 1
        if head < len(doc_titles) and doc_titles[head][0] in par[0]:
            current_author = doc_titles[head][1]
            head += 1
        if not current_author in author_fragm_lengths:
            author_fragm_lengths[current_author] = 0
        author_fragm_lengths[current_author] += len(par[0])
        if head == len(doc_titles) and final_title_place == -1:
            final_title_place = par_num
        if final_title_place != -1:
            final_title_ratio = final_title_place / par_num
    max_author, max_len = False, -1
    for author in author_fragm_lengths:
        if author_fragm_lengths[author] > max_len:
            max_len = author_fragm_lengths[author]
            max_author = author
    if author_assertion and author_assertion[1] != max_author and doc_row[2] == author_assertion[0]:
        logging.info(f'Assertion broken on doc {doc_row[0]} ({max_author}, {doc_row[2]})')
        assertion_broken = True
        assertion_broken_count += 1
    if max_author and not assertion_broken:
        docid_author[doc_row[0]] = max_author
    logging.debug(doc_row[1], '--', doc_row[2], '!!' if assertion_broken else '--', max_author)

print('Final title matched', final_title_place, f'({final_title_ratio} into the edition)')
print('Docs in file', len(doc_titles), 'in database', new_doc_count)
print(f'Author assertion broken {assertion_broken_count} times')

should_write = -1
if args.y:
    should_write = True
if final_title_place == -1:
    print('Not writing due to non-matched titles.')
    should_write = False
while should_write == -1:
    ans = input('Write the authors from the annotation? (y/n): ')
    if ans == 'y':
        should_write = True
    if ans == 'n':
        should_write = False

if should_write:
    for docid, author in docid_author.items():
        sqlite.execute('UPDATE document  SET source_entity = ? WHERE rowid = ?', (author, docid))
        logging.debug(f'Writing {author} to document {docid}...')
    sqlite.commit()
