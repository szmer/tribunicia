# Tribunicia

Tribunicia is a Python project for generating and processing text corpora from multiple OCR'ed books.
Its primary purpose is to use for the Polish [sejmik documents project](http://sejmiki.nlp.ipipan.waw.pl/),
but it should be adaptable for other corpora.

You can configure how document headings and metatext (that you presumably want to ignore - footnotes
etc.) should be detected with regular expressions. Tribunicia will read text from PDF files, or a
folder of .txt files, split it into documents and store them in a SQLite database. Optional labeling
of the text with Polish morphosyntactic tags (you have to have Morfeusz and Concraft installed) and
writing out a XML TEI-format corpus is also implemented.

This project supersedes [populobot](https://github.com/szmer/populobot). The analysis scripts from
there should be gradually refactored into tribunicia. The name is a similar reference to
*tribunicia potestas*, the power held by Ancient Roman *tribunes plebis*.

## Installation

It is recommended to create a [virtual environment](https://docs.python.org/3/tutorial/venv.html)
in this directory unless you know what you're doing and how to deal with potential conflicts.

Run `pip install .` and proceed to run the scripts that you want to use.

Run `pip install -r optional.requirements` if you encounter a script that wants additional
dependencies (you will get an error message with `!NOTE!`).

## Project structure

Run any of the scripts with `--help` to get info about it. For example, `python read_pdf.py --help`.
The main ones are `read_$FORMAT.py` in the main directory, see also the `scripts/` directory.

All the data is stored in a SQLite database. The default is `tribunicia.db`. You can also point to
another one with `--db_path` for each script.

## Testing

Run `pytest tests` in the project's root dir.

## Setting up pl_morphoparsing

The `tribunicia.pl_morphoparsing` module is responsible for auto-annotating Polish texts using the
standard tools, Morfeusz parser and Concraft disambiguator.

The Morfeusz package should be installed from `http://morfeusz.sgjp.pl/download/`. Also download a
Morfeusz model and put into some folder, preferably in the working directory that you're using.

Concraft should be obtained from `https://github.com/kawu/concraft-pl`. Here you will also need:
1. a model .gz file (you can place it in the same folder as the Morfeusz model)
2. to copy the `bindings/python/concraft_pl2.py` file to the `tribunicia` folder in this directory.

### For tests

The test suite can be ran for the pl_morphoparsing module, running `pytest` with `--pl_morpho` flag.
First you have to create the `tests/resources/pl_test_models.yaml`, looking like this:
```
morfeusz_dict_dir: "..."
morfeusz_dict_name: "..."
concraft_model_path: "..."
```
These are passed to the `pl_morphoparsing.MorfeuszAnalyzer` constructor (see its docstring).

To also include the `enrich_with_names` module, pass the `--ner` flag additionally.
