"""
Load an edition of texts from .txt files.
"""
import argparse
from datetime import datetime
import logging
import sqlite3
import yaml

from tribunicia.dbsetup import setup_db
from tribunicia.doc_metadata import DocMetadata
from tribunicia.enrich_document import add_is_sejmik_resolution
from tribunicia.pagegen import pdf_as_pages
from tribunicia.pl_morphoparsing import add_paragraphs_interps
from tribunicia.read_edition import read_edition
from tribunicia.structure_finder import StructureFinder

# TODO deal with overwrites!
argparser = argparse.ArgumentParser(
        description='Load and index an edition of documents from scanned pages.')
argparser.add_argument('desc_file_path',
        help='the YAML file describing what to do with the edition')
argparser.add_argument('--db_path',
        default='tribunicia.db',
        help='the path to the .db file that will be used or created for the docs')
argparser.add_argument('--enrichments',
        default='',
        help='comma-separated functions from tribunicia.enrich_document to run on the edition')
argparser.add_argument('--print_info', action='store_true',
        default=False,
        help='print info-level logging, such as settings being taken from the yaml file')
argparser.add_argument('--morpho_models',
        help='the path to the YAML file with paths and settings for the models of the'
        ' pl_morphoparsing module')
args = argparser.parse_args()

if args.print_info:
    logging.getLogger().setLevel(logging.INFO)

with open(args.desc_file_path) as f:
    desc_yaml = yaml.safe_load(f)

# Create the pages generator from the folder of txt files.
pagegen_kwargs = dict()
for key, mapped_key in [('page_number_shift', 'number_shift'),
        ('excluded_file_number_ranges', 'exclude_ranges')]:
    if key in desc_yaml['file_structure']:
        logging.info(f'Setting {key} to {desc_yaml["file_structure"][key]} for loading')
        pagegen_kwargs[mapped_key] = desc_yaml['file_structure'][key]
pages = pdf_as_pages(desc_yaml['edition_path'], **pagegen_kwargs)

# Create the object for the edition metadata.
metadata_kwargs = dict()
for key in ['source_entity', 'editor', 'publisher', 'edition_title',
            'source_place', 'publ_place', 'source_region', 'publ_date']:
    if key in desc_yaml['edition_metadata']:
        logging.info(f'Setting edition metadata {key} to {desc_yaml["edition_metadata"][key]}')
        if key.endswith('_date'):
            # The date components y, m, d are good for creating the datetime object.
            metadata_kwargs[key] = datetime(*[int(e) for e
                in desc_yaml['edition_metadata'][key].split('-')])
        else:
            metadata_kwargs[key] = desc_yaml['edition_metadata'][key]
edition_metadata = DocMetadata(**metadata_kwargs)

sqlite = sqlite3.connect(args.db_path)
setup_db(sqlite)
sf = StructureFinder(desc_yaml['text_structure'])
read_edition_kwargs = dict()
for key, mapped_key in [('strip_chapter_titles', 'strip_chapter_titles'),
                        ('book_title_to_strip', 'book_title'),
                        ('merge_short_docs', 'merge_short_docs'),
                        ('double_newlines', 'double_newlines')]:
    if key in desc_yaml['file_structure']:
        logging.info(f'Setting {key} to {desc_yaml["file_structure"][key]} for reading edition')
        read_edition_kwargs[mapped_key] = desc_yaml['file_structure'][key]
read_edition(pages, edition_metadata, sqlite, sf, **read_edition_kwargs)

enrichments = args.enrichments.split(',')
for enr in enrichments:
    if not enr:
        continue
    elif enr == 'add_is_sejmik_resolution':
        add_is_sejmik_resolution(sqlite, desc_yaml['edition_metadata']['edition_title'])
    elif enr == 'add_paragraphs_interps':
        with open(args.morpho_models) as f:
            morpho_yaml = yaml.safe_load(f)
        add_paragraphs_interps(sqlite, edition=desc_yaml['edition_metadata']['edition_title'],
                models_settings=morpho_yaml)
    elif enr == 'add_potential_surnames':
        from tribunicia.enrich_with_names import add_potential_surname_entities_ann
        add_potential_surname_entities_ann(sqlite,
                                       edition=desc_yaml['edition_metadata']['edition_title'])
    elif enr == 'add_footnotes':
        from tribunicia.enrich_with_names import add_footnotes_rules
        add_footnotes_rules(sqlite,
                            edition=desc_yaml['edition_metadata']['edition_title'])
    else:
        raise Warning('Enrichment {enr} is unknown')
